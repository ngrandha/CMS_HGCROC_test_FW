Instructions for writing a .bin file to the on-board configuration flash:

Open Vivado_Lab (or regular vivado), select "open target" and "autoconnect".

Right-click on the FPGA (xcku040_1) and select "Add Configuration memory device" 
and select "mt25qu256-spi-x1_x2_x4".

Right click on the memory and select "Program Configuration Memory Device"
and select the .bin file to program.

Note (20190109):
At the moment it's necessary to push the PROG button after switching the power ON 
as the current firmware for some reason doesn't start correctly at power-up.


It is possible to generate the bin file from the bit file without compiling the
entire Vivado project.
This is done with the Tcl command:
write_cfgmem -force -format BIN -interface SPIx4 -loadbit "up 0x00000000 20180309.bit" 20180309.bin
