For now I'm using Xilinx Vivado Lab Solutions to load the bit-file into the
KCU105. This can be downloaded from 
https://www.xilinx.com/support/download.html

You may also need to install digilent Adept 2 runtime from
https://reference.digilentinc.com/reference/software/adept/start
(but it doesn't hurt to try without first)

Set dip-switches SW15 to off
Set dip-switches SW12 to off (configures IP address)
Connect the HGROC board to the LPC FMC connector 
  (furthest from the PCI bracket)
Connect an USB cable to the JTAG port and power up the board.

In lab tools, simply open the "hardware manager" and click 
"open target" -> "autoconnect". 
It should find a xkcu040_0.
Right click on this and select "program device" and find the bit-file you want to load.

Connect a cable (via an RJ45 SFP) to SFP0. The board should reply to ping at
192.168.200.16 

Proceed to ../sw/README.txt
