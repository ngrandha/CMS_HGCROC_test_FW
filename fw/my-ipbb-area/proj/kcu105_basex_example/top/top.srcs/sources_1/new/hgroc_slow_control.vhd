library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;


--library UNISIM;
--use UNISIM.VComponents.all;

entity hgroc_slow_control is
    Port ( 
           sc_clk    : in STD_LOGIC;
           ipbus_clk : in STD_LOGIC;           
           reset : in STD_LOGIC;
           ipbus_in: in ipb_wbus;
           ipbus_out: out ipb_rbus;
             

           
           dac_mosi : out STD_LOGIC;
           dac_sclk : out STD_LOGIC;
           dac_sync1 : out STD_LOGIC;
           dac_sync2 : out STD_LOGIC
                      
           );
end hgroc_slow_control;

architecture Behavioral of hgroc_slow_control is
 signal ipbus_ack : std_logic;
 signal ipbus_wdata : std_logic_vector(31 downto 0);
 type fsm_type is ( idle, wrb1, wrb2, wrb3, rst1, wr1, wr2, load1, load2 );
 
 signal sc_addr_reg : std_logic_vector(11 downto 0);
 signal sc_cmd_reg  : std_logic_vector(2 downto 0);

 signal sc_count_reg, sc_count : std_logic_vector(15 downto 0);
 signal sc_strobe,sc_strobe_i,sc_strobe_j,sc_strobe_k : std_logic;
 signal sc_ack,sc_ack_i : std_logic;
 signal sc_running,sc_running_i,sc_err : std_logic;
 signal sc_pre_reg : std_logic_vector(7 downto 0);
 signal sc_pre_c   : std_logic_vector(8 downto 0);
 signal sc_pre     : std_logic;
 
 signal dac_spi_run,dac_mosi_i,dac_sclk_i : std_logic;
 signal dac_spi_c : std_logic_vector(7 downto 0);
 signal dac_spi_dout : std_logic_vector(22 downto 0);
 signal dac_spi_sync_i : std_logic_vector(1 downto 0);
 
 signal ipb_ram_we     : std_logic;
 signal ipb_ram2_we    : std_logic;
 signal ipb_ram_addr   : std_logic_vector(11 downto 0);
 signal ipb_ram_din    : std_logic_vector(31 downto 0);
 signal ipb_ram_dout   : std_logic_vector(31 downto 0);
 signal ipb_ram2_dout  : std_logic_vector(31 downto 0);
 signal ipb_read_ram   : std_logic;
 signal ipb_read_ram2  : std_logic;
 
 signal sc_ram2_we     : std_logic;
 signal sc_ram_addr    : std_logic_vector(13 downto 0); 
 signal sc_ram2_addr  : std_logic_vector(13 downto 0);
 signal sc_ram2_din    : std_logic_vector(7 downto 0);
 signal sc_ram_dout    : std_logic_vector(7 downto 0);
 
 attribute keep : string;  
 attribute keep of sc_strobe_j: signal is "true";  
 attribute keep of sc_running_i: signal is "true";
 attribute keep of sc_ack_i: signal is "true";
 
 
begin



 sc_ram2_we <= '0';
 sc_ram_addr <= (others => '0'); 
 sc_ram2_addr <= (others => '0');
 sc_ram2_din <= (others => '0');
 



sc_ram_inst: entity work.sc_bram
		port map(
		  clka => ipbus_clk,
		  ena => '1',
		  addra => ipb_ram_addr,
		  wea(0) => ipb_ram_we,
		  dina => ipb_ram_din,
		  douta => ipb_ram_dout,
		  
		  clkb => sc_clk,
		  enb => '1',
		  addrb => sc_ram_addr,
		  web(0) => '0',
		  dinb => X"00",
		  doutb => sc_ram_dout
		  );
		  
		  ipb_ram_addr<=ipbus_in.ipb_addr(11 downto 0);


sc_ram2_inst: entity work.sc_bram
		port map(
		  clka => ipbus_clk,
		  ena => '1',
		  addra => ipb_ram_addr,
		  wea(0) => ipb_ram2_we,
		  dina => ipb_ram_din,
		  douta => ipb_ram2_dout,
		  
		  clkb => sc_clk,
		  enb => '1',
		  addrb => sc_ram2_addr,
		  web(0) => sc_ram2_we,
		  dinb => sc_ram2_din,
		  doutb => open
		  );


		  		  
ipb_ram_din<=ipbus_in.ipb_wdata;
ipbus_out.ipb_rdata <= ipb_ram_dout when ipb_read_ram='1' else  ipb_ram2_dout when ipb_read_ram2='1' else ipbus_wdata ;

ipb_ram_we <='1' when ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='1' and ipbus_in.ipb_addr(13 downto 12)="01" else '0';
ipb_ram2_we <='1' when ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='1' and ipbus_in.ipb_addr(13 downto 12)="10" else '0';






process(reset, ipbus_clk)
begin
  if reset='1' then
    ipbus_ack <= '0';
    sc_running_i<='0';
    sc_strobe<='0';
    sc_strobe_i<='0';
    sc_err<='0';    
    sc_pre_reg<=X"0e";
    sc_count_reg<=X"0F4E";
    
    sc_ack_i<='0';
    ipb_read_ram<='0';
    ipb_read_ram2<='0';
    
    dac_sync1<='1';
    dac_sync2<='1';    

    dac_spi_sync_i<="11";
    dac_spi_run<='0';

    
  elsif rising_edge(ipbus_clk) then

    ipb_read_ram<='0';
    ipb_read_ram2<='0';
   
    sc_strobe<=sc_strobe_i;
    
    sc_ack_i<=sc_ack;
    
    if sc_ack_i='1' then
      sc_strobe_i<='0';
    end if;  
    
    ipbus_ack <='0';
    sc_running_i<=sc_running;
    
    if ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='1' then
      if ipbus_in.ipb_addr(13 downto 12)= "00" then   
        case ipbus_in.ipb_addr(3 downto 0) is
          when "0000" =>
            if sc_running_i='0' then
              sc_strobe_i<='1';    
              sc_addr_reg<=ipbus_in.ipb_wdata(11 downto 0);             
              sc_cmd_reg<=ipbus_in.ipb_wdata(18 downto 16);
            else
              sc_err<='1';
            end if;     
            ipbus_ack<='1';
             
          when "0001" =>
            sc_count_reg<=ipbus_in.ipb_wdata(15 downto 0);
            ipbus_ack<='1';

          when "0010" =>
            sc_pre_reg<=ipbus_in.ipb_wdata(7 downto 0);
            ipbus_ack<='1';
            
          
          when "0100" => --write dac at sync1
            if dac_spi_run='0' then
              dac_spi_dout<="0"&ipbus_in.ipb_wdata(15 downto 0)&"000000";
              dac_spi_c<=X"2f";
              dac_spi_sync_i(0)<='0';
              dac_sclk_i<='1';
              dac_mosi_i<='0';
              dac_spi_run<='1';
              ipbus_ack<='1';
            end if;

          when "0101" => --write dac at sync2
            if dac_spi_run='0' then
              dac_spi_dout<="0"&ipbus_in.ipb_wdata(15 downto 0)&"000000";
              dac_spi_c<=X"2f";
              dac_spi_sync_i(1)<='0';
              dac_sclk_i<='1';
              dac_mosi_i<='0';
              dac_spi_run<='1';
              ipbus_ack<='1';
            end if;
            
          
          when others =>
              ipbus_ack<='1';
              
        end case;  
      else
        --write enable to the ram is decoded separately, just need to acknowledge that we have grabbed the data
        ipbus_ack<='1';
      end if;
    end if;  
                         
            
    if ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='0' then           
      if ipbus_in.ipb_addr(13 downto 12)= "00" then
  --      ipb_read_ram<='0';
        ipbus_wdata<=(others =>'0');          
        case ipbus_in.ipb_addr(3 downto 0) is
          when "0000" => 
            ipbus_wdata(31)<=sc_running_i;
            ipbus_wdata(30)<=sc_err;
            
            ipbus_wdata(18 downto 16)<=sc_cmd_reg;                
           
            ipbus_wdata(11 downto 0)<=sc_addr_reg;
            ipbus_ack<='1';
            
          when "0001" =>
            ipbus_wdata(15 downto 0)<=sc_count_reg;
            ipbus_ack<='1';
  
          when "0010" =>
            ipbus_wdata(7 downto 0)<=sc_pre_reg;
            ipbus_ack<='1';            
            
          when others =>
            ipbus_wdata<=X"ABCDEF12";
            ipbus_ack<='1';                        
            
        end case;
      else        
        --bram sees read request at the same cycle we see this, and the response will be available one cycle later
        if ipbus_in.ipb_addr(13 downto 12)= "01" then
          ipb_read_ram<='1';
        end if;
        
        if ipbus_in.ipb_addr(13 downto 12)= "10" then
          ipb_read_ram2<='1';
        end if;  

          
        ipbus_ack<='1';             
      end if;       
               
    end if;
         
    if dac_spi_run='1' then
      dac_sclk_i<=not dac_sclk_i;
      if dac_sclk_i='0' then 
        dac_mosi_i<=dac_spi_dout(22);
        dac_spi_dout<=dac_spi_dout(21 downto 0)&"0";
      end if;
      if unsigned(dac_spi_c)=0 then
        dac_spi_run<='0';
        dac_spi_sync_i<="11";
      end if;
      dac_spi_c<=std_logic_vector(unsigned(dac_spi_c)-1);
      
    end if;       
         
    dac_sync1<=dac_spi_sync_i(0);
    dac_sync2<=dac_spi_sync_i(1);
    dac_mosi<=dac_mosi_i;
    dac_sclk<=dac_sclk_i;     
                  
  end if;
end process;               
                      
                      

ipbus_out.ipb_err <= '0';
ipbus_out.ipb_ack <=ipbus_ack;       

end Behavioral;
