library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity hgroc_daq is
    generic(
      N_LINKS: natural := 6      
    );
    Port (                       
           ipbus_clk       : in STD_LOGIC;           
           reset           : in STD_LOGIC;
           ipbus_in        : in ipb_wbus;
           ipbus_out       : out ipb_rbus;
           clk_320         : in std_logic;
           clk_640         : in std_logic;    
           rst_daq         : in std_logic;
           FMC_ck_40p      : out STD_LOGIC;
           FMC_ck_40n      : out STD_LOGIC;                  
           FMC_ck_320p     : out STD_LOGIC;
           FMC_ck_320n     : out STD_LOGIC;
           FMC_fcmd_p      : out std_logic;
           FMC_fcmd_n      : out std_logic;
           FMC_data_p      : in STD_LOGIC_VECTOR (1 downto 0);
           FMC_data_n      : in STD_LOGIC_VECTOR (1 downto 0);
           FMC_trigger_P   : in STD_LOGIC_VECTOR (3 downto 0);
           FMC_trigger_N   : in STD_LOGIC_VECTOR (3 downto 0);
           FMC_ReSyncLoad  : out STD_LOGIC;
           FMC_ReSync_ext  : out STD_LOGIC;
           FMC_L1_ext      : out STD_LOGIC;
           FMC_trig1       : out STD_LOGIC;
           FMC_trig2       : out STD_LOGIC;
           FMC_rstb        : out STD_LOGIC;
           FMC_strobe_ext  : out STD_LOGIC;
		   user_sma_gpio_p : out std_logic;
           user_sma_gpio_n : in std_logic;
           FMC_probe_ToT   : in std_logic;
           FMC_probe_ToA_N : in std_logic
           );
end hgroc_daq;

architecture Behavioral of hgroc_daq is
signal ipbus_ack : std_logic;
signal ipbus_wdata : std_logic_vector(31 downto 0);

signal rst_daq_320: std_logic;
signal rst_daq_320_i : std_logic;

signal clk_40_f,clk_40_i : std_logic :='0';
signal clk_40_c : std_logic_vector(2 downto 0) :="000";

constant swap_inputs : std_logic_vector(5 downto 0):=  "110000";


signal idelay_casc1,idelay_casc2,idelay_casc3,idelay_casc4 : std_logic_vector(7 downto 0);
signal tap_load,tap_load_j,tap_load_k : std_logic_vector(31 downto 0);
signal tap_wdata,tap_wdata_i,tap_wdata_j : std_logic_vector(10 downto 0);
signal tap_addr : std_logic_vector(4 downto 0);
signal tap_w,tap_w_i ,tap_w_j: std_logic :='0';

type tap_rdata_t is array (0 to 31) of std_logic_vector(10 downto 0);
signal tap_rdata : tap_rdata_t;
signal en_vtc : std_logic;

signal fmc_hs_data_p,fmc_hs_data_n : std_logic_vector(N_LINKS-1 downto 0);

type iserdes_t is array (0 to N_LINKS-1) of std_logic_vector(3 downto 0);
signal iserdes_q,hs_data : iserdes_t;

type data_capture_fifo_d_t is array (0 to N_LINKS-1) of std_logic_vector(31 downto 0);
signal data_capture_fifo_d : data_capture_fifo_d_t;
signal data_capture_fifo_rd,data_capture_fifo_full,data_capture_fifo_empty : std_logic_vector(N_LINKS-1 downto 0);
signal data_capture_fifo_da : std_logic_vector(31 downto 0);


signal data_capture_wr_en : std_logic_vector(N_LINKS -1downto 0) := (others => '0');
signal idelay_data_bitpos : std_logic_vector(95 downto 0);

type idelay_data_t is array (0 to N_LINKS-1) of std_logic_vector(31 downto 0);
signal idelay_data_i,idelay_data_j : idelay_data_t;

type idelay_data_wr_c_t is array (0 to N_LINKS-1) of std_logic_vector(4 downto 0);
signal idelay_data_wr_c : idelay_data_wr_c_t;


signal daq_run : std_logic :='0';
signal daq_stop : std_logic :='0';
signal daq_trig : std_logic;

signal  daq_trig_i: std_logic :='0';
signal  daq_trig_j: std_logic :='0';
signal  daq_trig_k: std_logic :='0';


signal data_capture_en : std_logic :='0';
signal data_capture_sync : std_logic;
signal data_capture_synced : std_logic_vector(N_LINKS-1 downto 0);

signal data_capture_sync_pos_clr,data_capture_sync_pos_clr_i : std_logic;
type data_capture_sync_pos_t is array (0 to N_LINKS-1) of std_logic_vector(19 downto 0);
signal data_capture_sync_pos : data_capture_sync_pos_t;

signal fifo_capture_en : std_logic_vector(5 downto 0) := "000000";

signal daq_run_c : std_logic_vector(19 downto 0);
type daq_event_t is array(0 to 43) of std_logic_vector(19 downto 0);
signal daq_event : daq_event_t;
signal daq_event_flag : std_logic_vector(43 downto 0) :=(others => '0');
signal daq_event_state : std_logic_vector(21 downto 0) :=(others => '0');

signal fifo_idle_pattern : std_logic_vector(31 downto 0);

signal ReSyncLoad      : std_logic :='0';
signal rstb            : std_logic :='1';


signal L1_ext          : std_logic :='0';
signal ReSync_ext      : std_logic :='0';
signal Strobe_ext      : std_logic :='0';
signal trig1_ext       : std_logic :='0';
signal trig2_ext       : std_logic :='0';

signal fcmd_stream     : std_logic_vector(7 downto 0);


signal user_sma_gpio_p_i : std_logic :='0';
signal user_sma_gpio_n_i : std_logic :='0';

type delay_init_t is ARRAY(0 to N_LINKS-1) of integer;
constant delay_init : delay_init_t := (0,0,0,0,0,0);

signal rst_fifo_i, rst_fifo_j, rst_fifo_k : std_logic;
signal rst_fifo : std_logic;

signal clk320_rate : std_logic := '0';

signal start_daq_i, start_daq_j : std_logic;
signal daq_start : std_logic;
signal fsm_run : std_logic;

signal daq_start_source : std_logic_vector(4 downto 0);
signal tot_daq_i, tot_daq_j : std_logic;
signal tot_daq_start : std_logic;
signal tot_fsm_run : std_logic;
signal toa_daq_i, toa_daq_j : std_logic;
signal toa_daq_start : std_logic;
signal toa_fsm_run : std_logic;

signal auto_trig_i, auto_trig_j : std_logic;
signal auto_trig : std_logic;
signal auto_fsm_run : std_logic;

begin



clk320_out: entity work.diff_out_clk320 
        generic map ( is_diff => 1 )
        port map(
          clk_320 => clk_320,
          clk_640 => clk_640,
          rst => rst_daq_320,
          delay_in => "00000000000",
          rate      => clk320_rate,
          delay_out => open,
          delay_wr  => '0',
          vtc       => en_vtc,
          OutP      => FMC_ck_320p,
          OutN      => FMC_ck_320n
          );
          


clk40_out: entity work.diff_out_320 
        generic map ( is_diff => 1 )
        port map(
          clk_320 => clk_320,
          clk_640 => clk_640,
          rst => rst_daq_320,
          d => clk_40_i,
          delay_in => "00000000000",
          delay_out => open,
          delay_wr  => '0',
          vtc       => en_vtc,
          OutP      => FMC_ck_40p,
          OutN      => FMC_ck_40n
          );
          
clk_40_i<=not clk_40_c(2);          

fcmd_out: entity work.diff_out_320 
        generic map ( is_diff => 1, swap => 1)
        port map(
          clk_320   => clk_320,
          clk_640   => clk_640,
          rst       => rst_daq_320,
          d         => fcmd_stream(7),
          delay_in  => tap_wdata,
          delay_out => tap_rdata(22),
          delay_wr  => tap_load(22),
          vtc       => en_vtc,
          OutP      => FMC_fcmd_p,
          OutN      => FMC_fcmd_n
          );          
          
          
          
rst_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => rstb,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(31),
                    delay_wr  => tap_load(31),
                    vtc       => en_vtc,
                    OutP      => FMC_rstb,
                    OutN      => open
                  );




L1_ext_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => L1_ext,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(30),
                    delay_wr  => tap_load(30),
                    vtc       => en_vtc,
                    OutP      => FMC_L1_ext,
                    OutN      => open
                  );



ReSyncLoad_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => ReSyncLoad,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(29),
                    delay_wr  => tap_load(29),
                    vtc       => en_vtc,
                    OutP      => FMC_ReSyncLoad,
                    OutN      => open
                  );
                  
ReSync_ext_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => ReSync_ext,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(28),
                    delay_wr  => tap_load(28),
                    vtc       => en_vtc,
                    OutP      => FMC_ReSync_ext,
                    OutN      => open
                  );


Strobe_ext_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => Strobe_ext,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(27),
                    delay_wr  => tap_load(27),
                    vtc       => en_vtc,
                    OutP      => FMC_Strobe_ext,
                    OutN      => open
                  );
                  
                  
trig1_ext_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => trig1_ext,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(26),
                    delay_wr  => tap_load(26),
                    vtc       => en_vtc,
                    OutP      => FMC_trig1,
                    OutN      => open
                  );                  

trig2_ext_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => trig2_ext,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(25),
                    delay_wr  => tap_load(25),
                    vtc       => en_vtc,
                    OutP      => FMC_trig2,
                    OutN      => open
                  );                  

gpio_sma_p_out: entity work.diff_out_320 
                  generic map ( is_diff => 0 )
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    d => user_sma_gpio_p_i,
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(24),
                    delay_wr  => tap_load(24),
                    vtc       => en_vtc,
                    OutP      => user_sma_gpio_p,
                    OutN      => open
                  );

--gpio_sma_n_out: entity work.diff_out_320 
--                  generic map ( is_diff => 0 )
--                  port map(
--                    clk_320 => clk_320,
--                    clk_640 => clk_640,
--                    rst => rst_daq_320,
--                    d => user_sma_gpio_n_i,
--                    delay_in => tap_wdata,
--                    delay_out => tap_rdata(23),
--                    delay_wr  => tap_load(23),
--                    vtc       => en_vtc,
--                    OutP      => user_sma_gpio_n,
--                    OutN      => open
--                  );



--IDELAYCTRL_inst : IDELAYCTRL
--generic map( SIM_DEVICE => "ULTRASCALE" )
--  port map (
--  RDY => open,
--  REFCLK => clk_320,
--  RST  => rst_daq_320
--);


--synchronize reset for idelayctrl, use for everythingelse as well atm
process(clk_320,rst_daq)
begin
  if rst_daq='1' then
    rst_daq_320_i<='1';    
    rst_daq_320<='1';
  elsif rising_edge(clk_320) then
    rst_daq_320_i<=rst_daq;    
    rst_daq_320<=rst_daq_320_i;
  end if;
end process;


data_capture_gen: for k in 0 to N_LINKS-1 generate

process (clk_320) 
begin
  if rising_edge(clk_320) then
    data_capture_wr_en(k)<='0';
    
    data_capture_sync_pos_clr_i<=data_capture_sync_pos_clr;


    idelay_data_i(k)<=idelay_data_i(k)(27 downto 0) & hs_data(k);
    idelay_data_j(k)<=idelay_data_i(k);

--  counter of received nibbles for each data stream      
    idelay_data_wr_c(k)<=std_logic_vector(unsigned(idelay_data_wr_c(k))-1);
    
--synchronization test, only used if data_capture_sync=1, otherwise always capturing while data_capture_en=1  
    if fifo_capture_en(k)='1' then  -- each FIFO has its own 'capture' signal
      if data_capture_sync='1' and data_capture_synced(k)='0' then    
        if idelay_data_i(k)=X"ACCCCCCC" then    -- 
          data_capture_wr_en(k)<='1';
          idelay_data_wr_c(k)<="11111";
          data_capture_synced(k)<='1';
          data_capture_sync_pos(k)<=daq_run_c;
        end if;
      else 
        data_capture_synced(k)<='1';
      end if;  
      if data_capture_synced(k)='1' and idelay_data_wr_c(k)(2 downto 0)="000"  then 
        data_capture_wr_en(k)<='1';
      end if;  
              
    else
      idelay_data_wr_c(k)<=idelay_data_bitpos(8*k+4 downto 8*k);
      data_capture_synced(k)<='0';

      if data_capture_sync_pos_clr_i='1' then
        data_capture_sync_pos(k)<=(others=>'1');
      end if;
      
    end if;
  end if;
end process;

data_capture_fifo : entity work.fifo_data_capture
  PORT MAP (
    --rst => rst_daq_320,
    rst => rst_fifo,
    wr_clk => clk_320,
    rd_clk => ipbus_clk,
    din => idelay_data_j(k),
    wr_en => data_capture_wr_en(k),
    rd_en => data_capture_fifo_rd(k),
    dout => data_capture_fifo_d(k),
    --dout => data_capture_fifo_da,
    full => data_capture_fifo_full(k),
    empty => data_capture_fifo_empty(k),
    wr_rst_busy => open,
    rd_rst_busy => open
  );
  end generate;


fmc_hs_data_p(3 downto 0)<=fmc_trigger_p;
fmc_hs_data_n(3 downto 0)<=fmc_trigger_n;

fmc_hs_data_p(5 downto 4)<=FMC_data_p;
fmc_hs_data_n(5 downto 4)<=FMC_data_n;

idata_hs_delay_gen: for k in 0 to 5 generate

hs_data_in: entity work.diff_in 
                  generic map ( delay_init => 0, swap => swap_inputs(k)) 
                  port map(
                    clk_320 => clk_320,
                    clk_640 => clk_640,
                    rst => rst_daq_320,
                    q => hs_data(k),
                    delay_in => tap_wdata,
                    delay_out => tap_rdata(k),
                    delay_wr  => tap_load(k),
                    vtc       => en_vtc,
                    InP      => FMC_hs_data_p(k),
                    InN      => FMC_hs_data_n(k)
                  );

end generate;


--statemachine to write delays since they need to run at clkdiv
process(clk_320)
begin
  if rising_edge(clk_320) then
    tap_w_i<=tap_w;
    tap_w_j<=tap_w_i;
    
    tap_load<=tap_load_j;
    tap_load_j<=tap_load_k;
    
--    tap_wdata<=tap_wdata_j;
    
    tap_load_k<=(others=>'0');    
    if tap_w_j='0' and tap_w_i='1' then
      tap_wdata_j<=tap_wdata_i;
      tap_load_k(to_integer(unsigned(tap_addr)))<='1';
    end if;    
  end if;
end process;

tap_wdata<=tap_wdata_i; --just assume the data is stable by the time tap_wdata is asserted, and that we aren't doing these back-to-back....



process(rst_daq_320,clk_320)
begin
  if rising_edge(clk_320) then
  
    if clk_40_c="111" then
      case daq_event_state(11 downto 7) is
        when "00001" => --Orbit       
          fcmd_stream<="11000011";
        when "00010" => --Trig       
          fcmd_stream<="11001001";
        when "00100" => --Calib       
          fcmd_stream<="11010001";
        when "01000" => --Sync       
          fcmd_stream<="11011111";
        when "10000" => --Dump       
          fcmd_stream<="11010111";
        when others => --Idle       
          fcmd_stream<="11000001";
      end case;
    else 
      fcmd_stream<=fcmd_stream(6 downto 0) & "0";
    end if;
  end if;
end process;  

process(rst_daq_320,clk_320)
begin
  if rst_daq_320='1' then
    start_daq_i <= '0';
    start_daq_j <= '0';

    daq_start   <= '0';
    fsm_run     <= '0';
  elsif rising_edge(clk_320) then
    start_daq_i <= user_sma_gpio_n;
    start_daq_j <= start_daq_i;

    if (start_daq_j = '1' and fsm_run = '0') then
      daq_start <= '1';
    else
      daq_start <= '0';
    end if;

    if (start_daq_j = '1' and fsm_run = '0') then
      fsm_run <= '1';
    elsif (daq_stop = '1') then
      fsm_run <= '0';
    else
      fsm_run <= fsm_run;
    end if;
  end if;
end process;

process(rst_daq_320,clk_320)
begin
  if rst_daq_320='1' then
    tot_daq_i <= '0';
    tot_daq_j <= '0';

    tot_daq_start   <= '0';
    tot_fsm_run     <= '0';
  elsif rising_edge(clk_320) then
    tot_daq_i <= FMC_probe_ToT;
    tot_daq_j <= tot_daq_i;

    if (tot_daq_j = '1' and tot_fsm_run = '0') then
      tot_daq_start <= '1';
    else
      tot_daq_start <= '0';
    end if;

    if (tot_daq_j = '1' and tot_fsm_run = '0') then
      tot_fsm_run <= '1';
    elsif (daq_stop = '1') then
      tot_fsm_run <= '0';
    else
      tot_fsm_run <= tot_fsm_run;
    end if;
  end if;
end process;

process(rst_daq_320,clk_320)
begin
  if rst_daq_320='1' then
    toa_daq_i <= '0';
    toa_daq_j <= '0';

    toa_daq_start   <= '0';
    toa_fsm_run     <= '0';
  elsif rising_edge(clk_320) then
    toa_daq_i <= not(FMC_probe_ToA_N);
    toa_daq_j <= toa_daq_i;

    if (toa_daq_j = '1' and toa_fsm_run = '0') then
      toa_daq_start <= '1';
    else
      toa_daq_start <= '0';
    end if;

    if (toa_daq_j = '1' and toa_fsm_run = '0') then
      toa_fsm_run <= '1';
    elsif (daq_stop = '1') then
      toa_fsm_run <= '0';
    else
      toa_fsm_run <= toa_fsm_run;
    end if;
  end if;
end process;

process(rst_daq_320,clk_320)
begin
  if rst_daq_320='1' then
    auto_trig_i  <= '1';
    auto_trig_j  <= '0';

    auto_trig    <= '0';
    auto_fsm_run <= '0';
  elsif rising_edge(clk_320) then
    auto_trig_j  <= auto_trig_i;

    if (auto_trig_j = '1'  and auto_fsm_run = '0') then
      auto_trig <= '1';
    else
      auto_trig <= '0';
    end if;

    if (auto_trig_j = '1' and auto_fsm_run = '0') then
      auto_trig_i  <= '0';
      auto_fsm_run <= '1';
    elsif (daq_stop = '1') then
      auto_trig_i  <= '1';
      auto_fsm_run <= '0';
    else
      auto_trig_i  <= auto_trig_i;
      auto_fsm_run <= auto_fsm_run;
    end if;

  end if;
end process;

--daq  statemachine
process(rst_daq_320,clk_320)
begin
  if rising_edge(clk_320) then

    case daq_start_source is
      when "00001" => -- signal from the script to start the FSM
        daq_trig_i <= daq_trig;
      when "00010" => -- external signal (GPIO_N) to start the FSM
        daq_trig_i <= daq_start;
      when "00100" => -- start the FSM with the external 'FMC_probe_ToT' signal
        daq_trig_i <= tot_daq_start;
      when "01000" => -- start the FSM with the external 'FMC_probe_ToA_N' signal
        daq_trig_i <= toa_daq_start;
      when "10000" => -- automatic start for the FSM
        daq_trig_i <= auto_trig;
      when others => null;
    end case;

    daq_trig_j<=daq_trig_i;    
    
    
--    data_capture_en<=daq_event_state(14);
    for k in 0 to 5 loop
      fifo_capture_en(k) <= daq_event_state(16+k);
    end loop;
    
    rstb<=not daq_event_state(0);
    L1_ext<=daq_event_state(1);       
    ReSyncLoad<=daq_event_state(2);
    ReSync_ext<=daq_event_state(3);    
    Strobe_ext<=daq_event_state(4);
    trig1_ext<=daq_event_state(5);
    trig2_ext<=daq_event_state(6);
    
    user_sma_gpio_p_i<=daq_event_state(12);
    user_sma_gpio_n_i<=daq_event_state(13);

    clk_40_c<=std_logic_vector(unsigned(clk_40_c)+1);     
    
    
    if clk_40_c="111" then
      clk_40_f<='1';
    else
      clk_40_f<='0';  
    end if;
    
    
    if daq_stop='1' then
      daq_run<='0';
      daq_event_state<=(others=>'0');
    end if;

    for k in 0 to 21 loop
      if daq_event_flag(k*2)='1' then
        daq_event_state(k)<='1';
      end if;
      if daq_event_flag(k*2+1)='1' then
        daq_event_state(k)<='0';
      end if;
    end loop;

           
    daq_event_flag<=(others=>'0');       
    for k in 0 to 43 loop
      if daq_run_c=daq_event(k) then
        daq_event_flag(k)<='1';
      end if;
    end loop;   
       
    if daq_event_flag(31)='1' then
      daq_stop <= '1';
    end if;
 
    if daq_run='1' then
      daq_run_c<=std_logic_vector(unsigned(daq_run_c)+1);
    end if;  

    if daq_trig_i='1' and daq_trig_j='0' then
      daq_trig_k<='1';
    end if;
    
    if daq_trig_k='1' and clk_40_f='1' then
      daq_run<='1';
      daq_stop<='0';
      daq_run_c<=X"00000";
      daq_trig_k<='0'; 
    end if;    
  end if;
end process;

rst_fifo_k <= rst_fifo_i;
rst_fifo <= rst_daq_320 or rst_fifo_k;



process(reset, ipbus_clk)
begin
  if reset='1' then   
    data_capture_fifo_rd<=(others=>'0');
    daq_trig<='0'; 
    
    idelay_data_bitpos<=(others => '1');
    
    data_capture_sync_pos_clr<='0';
    
    fifo_idle_pattern<=x"01234567";
    
    rst_fifo_i <= '1';
    
    clk320_rate  <= '0';

    data_capture_sync <= '0'; -- synchronization

    
  elsif rising_edge(ipbus_clk) then
    ipbus_ack <='0';
    
    data_capture_sync_pos_clr<='0';
    
    data_capture_fifo_rd<=(others=>'0');
    
    daq_trig<='0';
    tap_w<='0';
  
    rst_fifo_i <= '0';

    if ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='1' then
      if ipbus_in.ipb_addr(11 downto 10)= "00" then   
        case ipbus_in.ipb_addr(3 downto 0) is
          when "0000" =>  --status and vtc enable
            en_vtc       <= ipbus_in.ipb_wdata(16);
            clk320_rate  <= ipbus_in.ipb_wdata(17);

          when "0001"  => 
            daq_trig<='1';  
            data_capture_sync_pos_clr<='1';

          when "0010"  => -- enable the synchronization
            data_capture_sync <= ipbus_in.ipb_wdata(0);

          when "0011"  => -- select the source to start the FSM of the DAQ controller
            daq_start_source <= ipbus_in.ipb_wdata(4 downto 0);

          when "0100" => 
            idelay_data_bitpos(31 downto 0)<=ipbus_in.ipb_wdata;  

          when "0101" => 
            idelay_data_bitpos(63 downto 32)<=ipbus_in.ipb_wdata;  

          when "0110" => 
            idelay_data_bitpos(95 downto 64)<=ipbus_in.ipb_wdata;  

          when "0111" => 
            fifo_idle_pattern<=ipbus_in.ipb_wdata;

          when "1000" => --reset all FIFOs
            rst_fifo_i <= '1';

          when others => null;      
        end case;   
        ipbus_ack <='1';
      elsif  ipbus_in.ipb_addr(11 downto 10)= "01" then -- write delay registers
        if ipbus_in.ipb_addr(9 downto 8)= "00" then
          tap_addr  <= ipbus_in.ipb_addr(4 downto 0);
          tap_w     <= '1';
          tap_wdata_i <= ipbus_in.ipb_wdata(10 downto 0);
          ipbus_ack <='1';
        end if;  
      
        if ipbus_in.ipb_addr(9 downto 8)= "01" then  -- write daq event registers
          daq_event(to_integer(unsigned(ipbus_in.ipb_addr(5 downto 0))))<=ipbus_in.ipb_wdata(19 downto 0);
          ipbus_ack <='1';
        end if;  
      elsif  ipbus_in.ipb_addr(11 downto 10)= "10" then -- write otest ram
        ipbus_ack <='1';         
      end if;  
    end if;
      
    
    if ipbus_in.ipb_strobe='1' and ipbus_ack='0' and ipbus_in.ipb_write='0' then
      ipbus_wdata<=(others=>'0');           
      if ipbus_in.ipb_addr(11 downto 10)= "00" then
        
        case ipbus_in.ipb_addr(3 downto 0) is
          when "0000" => 
            ipbus_wdata(N_LINKS-1 downto 0)<=data_capture_fifo_empty;
            ipbus_wdata(16)<= en_vtc;
            ipbus_wdata(17)<= clk320_rate;

          when "0011" =>
            ipbus_wdata(4 downto 0)<= daq_start_source;
        
          when "0100" => 
            ipbus_wdata<=idelay_data_bitpos(31 downto 0);

          when "0101" => 
            ipbus_wdata<=idelay_data_bitpos(63 downto 32);
            
          when "0110" => 
            ipbus_wdata<=idelay_data_bitpos(95 downto 64);
            
          when "0111" => 
            ipbus_wdata<=fifo_idle_pattern;                                          
                        
          when others => null;
        end case; 
        ipbus_ack<='1';
      elsif  ipbus_in.ipb_addr(11 downto 10)= "01" then 
        
        if ipbus_in.ipb_addr(9 downto 8)= "00" then     -- read delay  registers     
--          ipbus_wdata(10 downto 0)<=tap_rdata(to_integer(unsigned(ipbus_in.ipb_addr(4 downto 0))))(10 downto 0);          
          ipbus_ack <='1';
        end if;  
        if ipbus_in.ipb_addr(9 downto 8)= "01" then -- read event  registers          
          ipbus_wdata(19 downto 0)<=daq_event(to_integer(unsigned(ipbus_in.ipb_addr(5 downto 0))));
          ipbus_ack <='1';
         end if;

        if ipbus_in.ipb_addr(9 downto 8)= "10" then -- read sync stamps          
          ipbus_wdata(19 downto 0)<=data_capture_sync_pos(to_integer(unsigned(ipbus_in.ipb_addr(4 downto 0))));
          ipbus_ack <='1';
         end if;
                
      elsif  ipbus_in.ipb_addr(11 downto 10)= "11" then -- read fifos
        if data_capture_fifo_empty(to_integer(unsigned(ipbus_in.ipb_addr(3 downto 0))))='0' then
          
          data_capture_fifo_rd(to_integer(unsigned(ipbus_in.ipb_addr(3 downto 0))))<='1';                    
          
          ipbus_wdata<=data_capture_fifo_d(to_integer(unsigned(ipbus_in.ipb_addr(3 downto 0))));
          ipbus_ack<='1';
   
        else   
          ipbus_wdata<=fifo_idle_pattern;
          ipbus_ack <='1';
        end if;          
      end if;    
    end if;
  end if;
end process;  

ipbus_out.ipb_err <= '0';
ipbus_out.ipb_ack <= ipbus_ack;  
ipbus_out.ipb_rdata <= ipbus_wdata;

end behavioral;
