library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;

library UNISIM;
use UNISIM.VComponents.all;

entity diff_in is
    Generic (delay_init : integer; swap : std_logic := '0') ;
    Port ( clk_320 : in STD_LOGIC;
           clk_640 : in STD_LOGIC;
           rst : in STD_LOGIC;
           q : out STD_LOGIC_VECTOR(3 downto 0);
           InP : in STD_LOGIC;
           InN : in STD_LOGIC;
           delay_in : in STD_LOGIC_VECTOR (10 downto 0);
           delay_wr : in STD_LOGIC;
           delay_out : out STD_LOGIC_VECTOR (10 downto 0);
           vtc : in STD_LOGIC);
end diff_in;

architecture Behavioral of diff_in is
 signal d,idelay_data : std_logic;
 signal iserdes_q,q_i,q_j: std_logic_vector(3 downto 0);
 signal delay_reg : std_logic_vector(1 downto 0) :="00";

begin


o_inst_diff_nswap : if swap='0' generate       
  --data_ids: ibufds generic map( DIFF_TERM => TRUE ) port map (i=>InP, ib=>InN, o=> d );
    data_ids: ibufds  port map (i=>InP, ib=>InN, o=> d );
end generate;

o_inst_diff_swap : if swap='1' generate       
  --data_ids: ibufds generic map( DIFF_TERM => TRUE ) port map (i=>InN, ib=>InP, o=> d );
  data_ids: ibufds  port map (i=>InN, ib=>InP, o=> d );
end generate;



  IDELAYE3_inst_m : IDELAYE3
    generic map (
      CASCADE => "NONE",
      DELAY_FORMAT => "COUNT",
      DELAY_SRC => "IDATAIN",
      DELAY_TYPE => "VAR_LOAD",
      DELAY_VALUE => delay_init,
      IS_CLK_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REFCLK_FREQUENCY => 320.0,
      UPDATE_MODE => "ASYNC"
    )
    port map (
     CASC_OUT => open,
     CNTVALUEOUT => delay_out(8 downto 0),
     DATAOUT => idelay_data,
     CASC_IN => '0',
     CASC_RETURN => '0',
     CE => '0',
     CLK => clk_320,
     CNTVALUEIN => delay_in(8 downto 0),
     DATAIN => '0',
     EN_VTC => vtc,
     IDATAIN => d,
     INC => '0',
     LOAD => delay_wr,
     RST => rst
    );
    --idelay_hs_data(k)<=fmc_hs_data(k);
    
    iserdes_inst : ISERDESE3 
    generic map (
      DATA_WIDTH => 4,
      FIFO_ENABLE => "FALSE",
      FIFO_SYNC_MODE => "FALSE",
      IS_CLK_B_INVERTED => '1',
      IS_CLK_INVERTED  => '0',
      IS_RST_INVERTED => '0' 
    )
    port map (
      FIFO_EMPTY => open,
      INTERNAL_DIVCLK => open,
      Q(7 downto 4) => open,
      Q(3 downto 0) => iserdes_q,
      CLK => clk_640,
      CLK_B => clk_640,
      CLKDIV => clk_320,
      D => idelay_data,
      FIFO_RD_CLK => '0',
      FIFO_RD_EN => '0',
      RST => rst
    );
    
delay_out(10 downto 9)<=delay_reg;    
    
process (clk_320)
begin
  if rising_edge(clk_320) then
    if swap='0' then  
      q_i(3)<=iserdes_q(0);
      q_i(2)<=iserdes_q(1);
      q_i(1)<=iserdes_q(2);
      q_i(0)<=iserdes_q(3);
    else
      q_i(3)<=not iserdes_q(0);
      q_i(2)<=not iserdes_q(1);
      q_i(1)<=not iserdes_q(2);
      q_i(0)<=not iserdes_q(3);    
    end if;
      
    q_j<=q_i;
    
    if delay_wr='1' then
      delay_reg<=delay_in(10 downto 9);
    end if;
    
    case delay_reg is 
      when "00" => 
        q<=q_i;
      when "01" => 
        q<=q_j(0 downto 0)&q_i(3 downto 1);
      when "10" => 
        q<=q_j(1 downto 0)&q_i(3 downto 2);
      when "11" => 
        q<=q_j(2 downto 0)&q_i(3 downto 3);
    end case;  
    
  end if;
end process;  

end Behavioral;
