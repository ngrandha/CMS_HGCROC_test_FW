---------------------------------------------------------------------------------
--
--   Copyright 2017 - Rutherford Appleton Laboratory and University of Bristol
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
--                                     - - -
--
--   Additional information about ipbus-firmare and the list of ipbus-firmware
--   contacts are available at
--
--       https://ipbus.web.cern.ch/ipbus
--
---------------------------------------------------------------------------------


-- ipbus_example
--
-- selection of different IPBus slaves without actual function,
-- just for performance evaluation of the IPbus/uhal system
--
-- Kristian Harder, March 2014
-- based on code by Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ipbus_example.all;

entity ipbus_example is
	port(
		ipb_clk: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		nuke: out std_logic;
		soft_rst: out std_logic;
		userled: out std_logic;
		clk_40 : in std_logic;
		clk_320 : in std_logic;
		clk_640 : in std_logic;
		rst_daq : in std_logic;
		
		
	    fmc_lpc_p : inout std_logic_vector(33 downto 0);
        fmc_lpc_n : inout std_logic_vector(33 downto 0);
        
        user_sma_gpio_p : out std_logic;
        user_sma_gpio_n : out std_logic;
        
        i2c_scl         : inout std_logic;
        i2c_sda         : inout std_logic
	);

end ipbus_example;

architecture rtl of ipbus_example is

 
	signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);
	signal ctrl, stat: ipb_reg_v(0 downto 0);
	signal FMC_data_P,FMC_data_N : std_logic_vector(7 downto 0);
	
	signal usrreg_i2c_settings2           : std_logic_vector(31 downto 0); -- 13
    signal usrreg_i2c_settings           : std_logic_vector(31 downto 0); -- 13 
    signal usrreg_i2c_command            : std_logic_vector(31 downto 0); -- 14 
    signal usrreg_i2c_reply              : std_logic_vector(31 downto 0); -- 15
	
	signal i2c_ctrl : ipb_reg_v(1 downto 0);
	signal i2c_stat : ipb_reg_v(0 downto 0);
	

begin




-- ipbus address decode
		
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_ipbus_example(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );

-- Slave 0: id / rst reg

	slave0: entity work.ipbus_ctrlreg_v
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_CTRL_REG),
			ipbus_out => ipbr(N_SLV_CTRL_REG),
			d => stat,
			q => ctrl
		);
		
		stat(0) <= X"abcdfedc";
		soft_rst <= ctrl(0)(0);
		nuke <= ctrl(0)(1);

-- Slave 1: register

	slave1: entity work.ipbus_reg_v
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_REG),
			ipbus_out => ipbr(N_SLV_REG),
			q => open
		);

-- Slave 2: 1kword RAM

	slave4: entity work.ipbus_ram
		generic map(ADDR_WIDTH => 10)
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_RAM),
			ipbus_out => ipbr(N_SLV_RAM)
		);
	
-- Slave 3: peephole RAM

	slave5: entity work.ipbus_peephole_ram
		generic map(ADDR_WIDTH => 10)
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_PRAM),
			ipbus_out => ipbr(N_SLV_PRAM)
		);

	hgroc_sc : entity work.hgroc_slow_control 
	     port map(
	       sc_clk => ipb_clk,  --clock for slow-control, does not need to be synchronous to ipbus
	       ipbus_clk => ipb_clk,
	       reset => ipb_rst,
	       
	       ipbus_in => ipbw(N_SLV_HGROC_SC),
           ipbus_out => ipbr(N_SLV_HGROC_SC),
	       
           FMC_ck_sc        => fmc_lpc_p(1),        --D8  OUT: clock for SC register chain
           FMC_srin_ck      => fmc_lpc_n(1),        --D9  OUT: data shifted into SC register chain, sampled on rising edge
           FMC_rstb_sc      => fmc_lpc_p(5),        --D11 OUT: "reset the not triplicated cells"
           FMC_W_Rb         => fmc_lpc_n(5),        --D12 OUT: write(1)/read(0) of the SC chain
           FMC_loadb        => fmc_lpc_p(9),        --D14 OUT: load SC registers from SC chain (falling edge)
           FMC_srout_sc     => fmc_lpc_n(9),        --D15 IN:  data shifted out of SC register chain
           FMC_parity_out   => fmc_lpc_p(13),       --D17 IN:  xor of all cells
           FMC_error_outb   => fmc_lpc_n(13),       --D18 IN:  error in a triplicated cell (OC before LS)
           
               dac_mosi     => fmc_lpc_p(22),       --din_dac_FMC G24 OUT: data to DAC8411*
               dac_sclk     => fmc_lpc_n(22),       -- sclk_dac_FMC
               dac_sync3    => fmc_lpc_n(12),       --G16 OUT: /sync to DAC8411 (dac_out)
               dac_sync2    => fmc_lpc_p(16),       --G18 ...                   (outM)
               dac_sync1    => fmc_lpc_n(16)            
                      
	     );	
	     
	     
--remapping because we aren't allowed to map non-continuous ranges with map
FMC_data_P(0)     <= fmc_lpc_p(11);       --H16 IN: 
FMC_data_N(0)     <= fmc_lpc_n(11);       --H17 IN:
FMC_data_P(1)     <= fmc_lpc_p(15);       --H19 IN: via S35
FMC_data_N(1)     <= fmc_lpc_n(15);       --H20 IN: via S31
FMC_data_P(2)     <= fmc_lpc_p(19);       --H22 IN: 
FMC_data_N(2)     <= fmc_lpc_n(19);       --H23 IN:
FMC_data_P(3)     <= fmc_lpc_p(21);       --H25 IN:
FMC_data_N(3)     <= fmc_lpc_n(21);       --H26 IN:
FMC_data_P(4)     <= fmc_lpc_p(24);       --H28 IN: via S34
FMC_data_N(4)     <= fmc_lpc_n(24);       --H29 IN: via S30
FMC_data_P(5)     <= fmc_lpc_p(28);       --H31 IN:
FMC_data_N(5)     <= fmc_lpc_n(28);       --H32 IN: 
FMC_data_P(6)     <= fmc_lpc_p(30);       --H34 IN:
FMC_data_N(6)     <= fmc_lpc_n(30);       --H35 IN: 
FMC_data_P(7)     <= fmc_lpc_p(32);       --H37 IN: via R150 (!R151)
FMC_data_N(7)     <= fmc_lpc_n(32);       --H38 IN: via R136 (!R137)	     
	     
	     
	hgroc_daq : entity work.hgroc_daq 
              port map(                
                ipbus_clk => ipb_clk,
                reset => ipb_rst,
                
                ipbus_in => ipbw(N_SLV_HGROC_DAQ),
                ipbus_out => ipbr(N_SLV_HGROC_DAQ),
                
                clk_320 => clk_320,
                clk_640 => clk_640,
                rst_daq => rst_daq,                             
              
                FMC_data_P => FMC_data_P,
                FMC_data_N => FMC_data_N, 
              
                FMC_ck_40p  => fmc_lpc_p(0),
                FMC_ck_40n  => fmc_lpc_n(0),
                
                FMC_ck_320p  => fmc_lpc_p(2),
                FMC_ck_320n  => fmc_lpc_n(2),
                FMC_data_32p => fmc_lpc_p(4),
                FMC_data_32n => fmc_lpc_n(4),
                FMC_trigger_P   => fmc_lpc_p(8)&fmc_lpc_p(20), 
                FMC_trigger_N   => fmc_lpc_n(8)&fmc_lpc_n(20),
                FMC_ReSyncLoad_P => fmc_lpc_p(25),
                FMC_ReSyncLoad_N => fmc_lpc_n(25),
                FMC_SendSyncPattern_P => fmc_lpc_p(29),
                FMC_SendSyncPattern_N => fmc_lpc_n(29),
                FMC_StartAcq    => fmc_lpc_p(27),
                FMC_rstb        => fmc_lpc_n(27), 
                
                FMC_StartReadOut=> fmc_lpc_p(26),
                
			    user_sma_gpio_p => user_sma_gpio_p,
                user_sma_gpio_n => user_sma_gpio_n,
                
                FMC_strobe       => fmc_lpc_n(26)                 
                
--                FMC_trig_p(0)      => fmc_lpc_p(31),
--                FMC_trig_p(1)      => fmc_lpc_p(33),
--                FMC_trig_n(0)      => fmc_lpc_n(31),
--                FMC_trig_n(1)      => fmc_lpc_n(33)
              );    	     










ipb_i2c_slave: entity work.ipbus_ctrlreg_v
        generic map (N_CTRL => 2, N_STAT=>1) 
		port map(
			clk => ipb_clk,
			reset => ipb_rst,
			ipbus_in => ipbw(N_SLV_I2C),
			ipbus_out => ipbr(N_SLV_I2C),
			d => i2c_stat,
			q => i2c_ctrl
		);



i2c_m: entity work.i2c_master_top

generic map (nbr_of_busses => 1) port map (
    clk               => ipb_clk,
    reset             => ipb_rst,
    ------------------
    id_o              => open, 
    id_i              => x"00",
    enable            => i2c_ctrl(0)(15),
    bus_select        => i2c_ctrl(0)(12 downto 10),
    prescaler         => i2c_ctrl(0)( 9 downto  0),
    command           => i2c_ctrl(1),
    reply             => i2c_stat(0),
   ------------------
    scl_io(0)         => i2c_scl,           
    sda_io(0)         => i2c_sda

);           


    



end rtl;
