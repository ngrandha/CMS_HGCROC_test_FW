2019/09/13

Firmware Version is 0x00010007

Firmware upgrade.
Use 'FMC_probe_ToT' signal, 'FMC_probe_ToA' signal or an external signal to trig the FSM of the DAQ controller.

Change the 'hgroc_test_address_table.xml' file:
	- change the name of the 'hgroc_daq.trig' register to 'hgroc_daq.daq_start'
	- add a new 'hgroc_daq.synchro' register to enable or not the synchronization
	- add a new 'hgroc_daq.daq_start_source' register to choose the source to start the FSM of the DAQ controller:
		./fmc_hgroc  -W hgroc_daq.synchro = 1, 2, 4 or 8
		* 1 : the source is in the script file (.sh) : ./fmc_hgroc -W hgroc_daq.daq_start=1
		* 2 : the source is an external signl recieved on the GPIO_N of the KCU105 board
		* 4 : the source is the 'FMC_probe_ToT' signal
		* 8 : 'FMC_probe_ToA' signal

Change 'inout' to 'in' for the 'FMC_probe_ToT' signal.

