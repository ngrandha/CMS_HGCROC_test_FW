2019/08/01

Firmware Version is 0x00010004

Firmware upgrade.
Add a Reset FIFO feature and Default values of ipbus registers


*******************************************************************
Add a Reset FIFO feature.
./fmc_hgroc -W hgroc_daq.fifo_reset=0x01

Writing 1 in this register generate a pulse that erase all FIFOs

*******************************************************************
Change Default values of ipbus registers at FPGA startup, from 0 to 1 for only those signals:

FMC_I2C_rstb
FMC_Sel_ck_ext
FMC_VADJ_ON
IIC_MUX_RESET_B_LS

******************************************************************
