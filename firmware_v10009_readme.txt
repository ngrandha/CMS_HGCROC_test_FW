2019/10/22

Firmware Version is 0x00010009

Firmware upgrade.
Add I2C error outputs on the FMC HPC.
Change the slew rate of the FMC_SCL signal in the XDC file.