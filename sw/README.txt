To save yourself trouble, please consider installing CERN CENTOS 7 from:
http://linux.web.cern.ch/linux/centos7/docs/install.shtml
 Write boot.iso to a usbstick or use it as boot media for virtualbox.
 At the configuration screen, first enable network,
 then enter http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/
 You'll need to click the partitioning and package selection buttons,
 but the defaults are fine. After this you should be able to proceed with
 the install.

% cp etc/ipbus-sw.repo /etc/yum.repos.d/ipbus-sw.repo
% yum groupinstall uhal

% cd CMS_HGCROC_test_FW/sw/fmc_ctrl/
% source setup.sh
% ./mk hgcal_fmc

Now you can use hgcal_fmc to perform simple tasks:

For example, to read from the I2C device (U28) at address 0x74 on the KCU105 use:
% ./fmc_hgroc  -I 0x74

To write one byte:
./fmc_hgroc -I 0x74=1

To write 2 bytes (1 followed by 2):
./fmc_hgroc -I 0x74=1:2


To talk to the HGCROC instead use -J








