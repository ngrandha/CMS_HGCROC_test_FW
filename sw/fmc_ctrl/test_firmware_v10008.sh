#!/bin/bash

#check which type of board this firmware is meant for
./fmc_hgroc -R reg.board_id 
#check the firmware version (last field of bitfile name)
./fmc_hgroc -R reg.version

#fifo 0..3 is Trigger 0..3
#fifo 4,5 is DAQ 0,1

#flush fifos 
#./fmc_hgroc -R hgroc_daq.config
#./fmc_hgroc -W hgroc_daq.fifo_reset=0x1
#./fmc_hgroc -R hgroc_daq.config

# Set PLL config
./fmc_hgroc -J 0x29=0x05
./fmc_hgroc -J 0x28=0x80
# ./fmc_hgroc -J 0x2A=0x10 #Kill PLL_lock and other digital blocks
./fmc_hgroc -J 0x2A=0x30 #Kill PLL_lock 'only'

./fmc_hgroc -J 0x28=0x81
./fmc_hgroc -J 0x2A=0x19 # Enable high capa

./fmc_hgroc -J 0x28=0x83
./fmc_hgroc -J 0x2A=0x7F #Max charge pump bias

./fmc_hgroc -J 0x28=0x85
./fmc_hgroc -J 0x29=0x5
./fmc_hgroc -J 0x2a=0x3f

./fmc_hgroc -J 0x28
./fmc_hgroc -J 0x29
./fmc_hgroc -J 0x2a

#configure delays for clk_ext=0
#./fmc_hgroc  -W hgroc_daq.config=0x0
#./fmc_hgroc  -W hgroc_daq.delay.fifo0=0x105
#./fmc_hgroc  -W hgroc_daq.delay.fifo1=0x0f5
#./fmc_hgroc  -W hgroc_daq.delay.fifo2=0x127
#./fmc_hgroc  -W hgroc_daq.delay.fifo3=0x125
#./fmc_hgroc  -W hgroc_daq.delay.fifo4=0x101
#./fmc_hgroc  -W hgroc_daq.delay.fifo5=0x106
#./fmc_hgroc  -W hgroc_daq.config=0x10000

#configure delays for clk_ext=1
./fmc_hgroc  -W hgroc_daq.config=0x0
./fmc_hgroc  -W hgroc_daq.delay.fifo0=0x1b9
./fmc_hgroc  -W hgroc_daq.delay.fifo1=0x1a7
./fmc_hgroc  -W hgroc_daq.delay.fifo2=0x1cf
./fmc_hgroc  -W hgroc_daq.delay.fifo3=0x1cb
./fmc_hgroc  -W hgroc_daq.delay.fifo4=0x1b3
./fmc_hgroc  -W hgroc_daq.delay.fifo5=0x1b6
./fmc_hgroc  -W hgroc_daq.config=0x10000

strobe=1;

./fmc_hgroc -J 0x28=0x64
./fmc_hgroc -J 0x29=0x20
./fmc_hgroc -J 0x2a=0x4

./fmc_hgroc -J 0x28=0x7
./fmc_hgroc -J 0x29=0x5
./fmc_hgroc -J 0x2a=0x7f
sleep 0.1;

## configure

#configure DAQ controller
./fmc_hgroc -W hgroc_daq.synchro=0
./fmc_hgroc -W hgroc_daq.daq_start_source=16

## disable Reset and ResyncLoad
./fmc_hgroc  -W hgroc_daq.event.rst_start=0
./fmc_hgroc  -W hgroc_daq.event.rst_stop=0
./fmc_hgroc  -W hgroc_daq.event.ReSyncLoad_ext_start=100
./fmc_hgroc  -W hgroc_daq.event.ReSyncLoad_ext_stop=120
./fmc_hgroc  -W hgroc_daq.event.trig1_start=100
./fmc_hgroc  -W hgroc_daq.event.trig1_stop=120

#Fast command triggers, must be on a an integer multiple of 8 to have any effect.
#./fmc_hgroc  -W hgroc_daq.event.fcmd_orbit=10000
#./fmc_hgroc  -W hgroc_daq.event.fcmd_calib=$((strobe*8))
#./fmc_hgroc  -W hgroc_daq.event.fcmd_trig=$((10*8+8*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fcmd_orbit_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_orbit_stop=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_calib_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_calib_stop=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_trig_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_trig_stop=232
./fmc_hgroc  -W hgroc_daq.event.fcmd_sync_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_sync_stop=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_dump_start=200
./fmc_hgroc  -W hgroc_daq.event.fcmd_dump_stop=200

#To send Sync set Orbit and Calib to the same value
#To send Dump set Calib and Trig to the same value
#fast commands can be skewed with respect to clk40 and clk320 by adjusting hgroc_daq.delay.fcmd

./fmc_hgroc  -W hgroc_daq.event.gpio_p_start=200
./fmc_hgroc  -W hgroc_daq.event.gpio_p_stop=232

./fmc_hgroc  -W hgroc_daq.event.gpio_n_start=0
./fmc_hgroc  -W hgroc_daq.event.gpio_n_stop=32

./fmc_hgroc  -W hgroc_daq.event.fifo0_capture_start=$((30*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fifo0_capture_stop=$((30*8+strobe*8+40*8))
./fmc_hgroc  -W hgroc_daq.event.fifo1_capture_start=$((30*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fifo1_capture_stop=$((30*8+strobe*8+40*8))
./fmc_hgroc  -W hgroc_daq.event.fifo2_capture_start=$((30*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fifo2_capture_stop=$((30*8+strobe*8+40*8))
./fmc_hgroc  -W hgroc_daq.event.fifo3_capture_start=$((30*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fifo3_capture_stop=$((30*8+strobe*8+40*8))
./fmc_hgroc  -W hgroc_daq.event.fifo4_capture_start=$((30*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fifo4_capture_stop=$((30*8+strobe*8+40*8))
./fmc_hgroc  -W hgroc_daq.event.fifo5_capture_start=$((30*8+strobe*8))
./fmc_hgroc  -W hgroc_daq.event.fifo5_capture_stop=$((30*8+strobe*8+40*8))

./fmc_hgroc  -W hgroc_daq.event.daq_stop=$((30*8+strobe*8+40*8+8))

#bit position for each link (5 bits out of every 8 bits, starting from data0 at bitpos0<4:0>
#used only in unsynchronized capture mode
./fmc_hgroc  -W hgroc_daq.data_bitpos0=0x01010101
./fmc_hgroc  -W hgroc_daq.data_bitpos1=0x0101

######### ACQ

# clear files
prefix='run_'$(date '+%Y-%m-%d-%H%M%S')'_'
echo $prefix

for j in `seq 0 5`; 
do
    fname="data/fifo$j.txt"
    echo fifo$j > $fname
done

nevents=0
for event in `seq 0 $nevents`;
do
    #flush fifos ./
    echo "FIFO before flush"
    ./fmc_hgroc -R hgroc_daq.config

    ./fmc_hgroc -W hgroc_daq.fifo_reset=0x01

    echo "FIFO after flush"
    ./fmc_hgroc -R hgroc_daq.config

    #run DAQ controller, =0 for unsynchronized capture, =1 to synchronize each stream to the predefined pattern
    #./fmc_hgroc -W hgroc_daq.trig=0 new name for the register
    #./fmc_hgroc -W hgroc_daq.daq_start=1

    echo "FIFO after trig"
    ./fmc_hgroc -R hgroc_daq.config

    fifo_depth=10

    for j in `seq 0 5`; do
	fname="data/fifo$j.txt"
	echo "#evt"$event >> $fname
	./fmc_hgroc -R hgroc_daq.fifo$j:$fifo_depth >> $fname
    done

    echo "FIFO after read"
    ./fmc_hgroc -R hgroc_daq.config

    echo "FIFO flush"
    ./fmc_hgroc -W hgroc_daq.fifo_reset=0x01

    echo "FIFO after flush"
    ./fmc_hgroc -R hgroc_daq.config
done

