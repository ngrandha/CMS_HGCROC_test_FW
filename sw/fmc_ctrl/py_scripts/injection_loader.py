import sys, os, subprocess, datetime
import fpga_control as fpga
from config import ROC, DAQ
from common_acq import *
from injection_scan import acq

def main():
    for tc in [0,4]:
        fname = "configs_injection/roc_config_ChargeLin_TC_"+str(tc)+".yaml"
        roc = ROC(fname)
        roc.config_sc_params()
        print('loaded parameters from TC'+str(tc))
    print('acquiring data...')
    acq(50)

if __name__ == "__main__":
    main()
