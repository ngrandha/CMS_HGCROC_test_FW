#!/usr/bin/python
import sys, os, datetime
import numpy as np
from common import *

def acq(n_events = 1):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/bitpos/run_" + timestamp
    odir = os.path.abspath(odir) + "/"

    if not os.path.exists(odir):
        os.makedirs(odir)

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    strobe = 0

    # Fast CMDs
    #set_fcmds(orb = strobe, l1a = 1e4, calib = strobe)
    disable_fcmds()

    set_fcmd_sync(strobe, strobe + 1)
    set_gpio_p(strobe, strobe + 1)

    c_start = strobe
    c_stop = c_start + 400
    set_capture_window(c_start, c_stop)

    # check fifo is empty
    print "Fifo full?", check_fifo_full()
    print ("Taking data...")

    data_size = 300

    good_bitposs = {i:[] for i in range(6)}

    for bitpos in range(8):

        flush_fifos()

        outdir = odir + "/bitpos_%i_" %bitpos
        set_bit_pos({i:bitpos for i in range(6)})

        trig_fsm(0)

        if check_fifo_full():
            for ififo in range(6):
                read_fifo(ififo, outdir, data_size)
                fname = outdir + "fifo%i.raw" % ififo

                nidles = 0
                with open(fname) as f:
                    #lines = f.readlines()
                    lines = f.read().splitlines()
                    nidles = lines.count(idle_word)
                    #print ififo, nidles

                if nidles > 1:
                    good_bitposs[ififo].append(bitpos)

                '''
                ## Trigger should have 258 idles
                if (ififo < 4) and (nidles >= 256):
                    good_bitposs[ififo].append(bitpos)
                ## Data should have all idles
                if (ififo > 3) and (nidles == data_size):
                    good_bitposs[ififo].append(bitpos)
                '''

    print ("... done")

    ### Compute best bitposs
    print 80*"#"
    best_bitposs = {}

    for ififo in range(6):
        dels = good_bitposs[ififo]
        bdel = 0
        if len(dels) > 0:
            bdel = int(np.mean(dels))
            #print ififo, len(dels), bdel, int(np.std(dels))
        else:
            print ififo, " not found"
        best_bitposs[ififo] = bdel

    ## set bitposs
    if np.sum(np.diff(best_bitposs.values())) == 0:
        print best_bitposs.values()
        print("Setting best bit position")
        set_bit_pos(best_bitposs)#[0])

        daq.link_bitpos = best_bitposs
        daq.save_config()#"configs/test.yaml")

    else:
        print("Best bit pos different!")

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
