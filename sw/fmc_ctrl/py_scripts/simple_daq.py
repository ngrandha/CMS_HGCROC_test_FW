# -*- coding: utf-8 -*-
#!/usr/bin/python
import sys, os, subprocess, datetime
import fpga_control as fpga
from config import ROC, DAQ

# define event size and number of consecutive triggers (L1A)
ev_size = 43

def configure_acq(daq, n_events = 1, n_bxs = 1):
    #n_bxs = number of consecutive L1A (max for same Orbit is 13)

    if n_events < n_bxs:
        print(80*"#")
        print("# Nevents requested lower than #L1A: using single L1A")
        print(80*"#")
        n_bxs = 1

    n_events /= n_bxs

    # Configure fast CMDs
    fpga.disable_fcmds()

    strobe = 0
    orbit = strobe
    fpga.set_fcmd_orbit(orbit, orbit + 1)

    l1a_start = orbit + daq.l1a_offset #- 1
    l1a_stop = l1a_start + 1

    if n_bxs > 1:
        l1a_stop += daq.capture_offset
        l1a_stop += ev_size * (n_bxs - 1)
    #print l1a_start, l1a_stop

    fpga.set_fcmd_l1a(l1a_start, l1a_stop)

    # Data capture
    c_start = l1a_start + daq.capture_offset
    c_stop = c_start + ev_size * n_bxs
    fpga.set_capture_window(c_start, c_stop)
    #fpga.set_gpio_p(c_start, c_stop)

    #print l1a_start, l1a_stop, c_start, c_stop

    '''
    ## trigger readout
    tp_link_offset = 7
    tp_c_start = orbit + tp_link_offset
    tp_c_stop = tp_c_start + 10

    tp_ev_size = 44
    tp_data_size = n_events * tp_ev_size

    fpga.set_fifo_capture_window(0, tp_c_start, tp_c_stop)
    '''

    data_size = ev_size * n_events * n_bxs

    if data_size > daq.max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = daq.max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
        data_size = ev_size * n_events

    daq.n_events = n_events
    daq.n_bxs = n_bxs
    daq.data_size = data_size

    return daq

def make_outdir(run_type = "acq",suffix = ""):
    # Create output folder
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/%s/run_" %run_type + timestamp + suffix
    odir = os.path.abspath(odir) + "/"
    if not os.path.exists(odir): os.makedirs(odir)
    print("Output dir:")
    print(odir)
    return odir

def simple_acq(daq,roc,run_type,n_events = 1, suffix = ""):

    outdir = make_outdir(run_type,suffix)

    # change L1A offset
    l1a_offset = 10
    roc.set_parameter("DigitalHalf", "all","L1Offset", l1a_offset)
    # update value in daq object
    daq.l1a_offset = l1a_offset

    ## configure acquisition
    daq = configure_acq(daq, n_events, n_bxs = 1)

    ## save DAQ and ROC configs to outdir
    daq.save_config(outdir + "daq_config.yaml")
    roc.save_config(outdir + "roc_config.yaml")

    ## Acquisition
    fpga.flush_fifos()
    # check fifo is empty
    print("Fifo content: " + bin(fpga.check_fifo_content()))

    print("Taking data...")
    # launch acquisition loop
    fpga.bash_acq_loop(daq.n_events, 0)

    print("Fifo content: " + bin(fpga.check_fifo_content()))

    print("Reading data...")
    fpga.read_fifos(range(6),outdir, daq.data_size)

    # for separate capture of data and trigger f
    #read_fifos(range(4),odir + "/", tp_data_size)
    #read_fifos([4,5],odir + "/", data_size)

    # check fifo is empty
    print("Fifo content: " + bin(fpga.check_fifo_content()))

    return outdir

def main():
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    roc = ROC(roc_conf_fname)
    print('roc and daq variables initialized')
    n_events = 1
    suffix = ""

    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suffix = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suffix)


    simple_acq(daq,roc,"acq",n_events, suffix)

if __name__ == "__main__":
    main()

