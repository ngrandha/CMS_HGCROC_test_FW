#!/usr/bin/python
import sys
from common import *
import fpga_control as fpga

def main(roc_conf_fname = "configs/roc_config.yaml"):

    check_fw_version()

    daq = DAQ("configs/daq_params.yaml")
    roc = ROC(roc_conf_fname)

    # set delays and bitpos
    fpga.set_link_delays(daq.link_delays)
    fpga.set_bit_pos(daq.link_bitpos)

    # Disable all fast CMDs
    fpga.disable_fcmds()

    # reset ROC digital part
    fpga.reset_roc_digital()

    # flush FPGA FIFOs
    fpga.flush_fifos()

    # ROC config
    fpga.reset_i2c_params()

    # Configure all parameters from config file
    roc.config_sc_params()

    # read IDLE frame pattern
    idle_frame = roc.read_parameter("DigitalHalf", 0,"IdleFrame")
    print("Idle frame: " + hex(idle_frame))

    # reset Master TDC:
    #"MasterTdc" "START_COUNTER" to 0 and then to 1.
    roc.set_parameter("MasterTdc", "all", "START_COUNTER", 0)
    roc.set_parameter("MasterTdc", "all", "START_COUNTER", 1)

    return

if __name__ == "__main__":

    if len(sys.argv) > 1:
        fname = sys.argv[1]
        print("Using ROC config file: %s" %fname)
        main(fname)
    else:
        main()
