#!/usr/bin/python
from fmc_wrapper import *

fw_version = 9

def check_fw_version():
    cmd = " -R reg.version"
    ret = exec_cmd(cmd)
    print("FW version read: " + str(ret[:-1]))

    read_fw_version = ret.split()[-1][-1]
    read_fw_version = int(read_fw_version, 16)
    if read_fw_version != fw_version:
        print(80*"!")
        print("Code status: FW version %i" % fw_version)
        print(80*"!")
        print(80*"!")

def set_vadj_power(value = 1):
    ''' Enable power to FMC '''
    cmd = " -W reg.ctrl.FMC_VADJ_ON=%i" %value
    ret = exec_cmd(cmd)

def set_ext_clock(val = 1):
    ''' Select external clock for PLL input '''
    cmd = " -W reg.ctrl.FMC_Sel_ck_ext=%i" %val # 1 INT, 0 EXT
    ret = exec_cmd(cmd)

def reset_i2c_params():
    ''' Reset to Default parameters '''
    cmd = "-W reg.ctrl.FMC_I2C_rstb=0"
    ret = exec_cmd(cmd)

    cmd = "-W reg.ctrl.FMC_I2C_rstb=1"
    ret = exec_cmd(cmd)

### DAQ / FSM

def set_serdes_vtc(is_on = 0):

    val = 0x10000 if is_on else 0

    # set config to 0
    cmd = "-W hgroc_daq.config=" + hex(val)
    ret = exec_cmd(cmd)

    return

def set_link_delays(delays, vtc_on = 1):

    if len(delays) != 6:
        print("Can't set delays, only %i given!" %len(delays))
        return 0

    set_serdes_vtc(0)

    for ififo in range(6):
        val = delays[ififo]
        #print("Setting %i fifo delay to %i" %(ififo, val))
        cmd = "-W hgroc_daq.delay.fifo%i=%i" %(ififo, val)
        ret = exec_cmd(cmd)

    if vtc_on: set_serdes_vtc(1)

    return

#def set_bit_pos(pos = 0):
def set_bit_pos(bit_pos = {i:0 for i in range(6)}):
    ''' bit position for each link (5 bits out of every 8 bits, starting from data0 at bitpos0<4:0>'''
    #used only in unsynchronized capture mode

    ## bitpos0
    val = 0
    for i in range(4):
        pos = bit_pos[5 - i]
        tmp = pos << i*8
        val = val | tmp
        # print hex(tmp), hex(val)

    cmd = "-W hgroc_daq.data_bitpos0=" + hex(val)
    ret = exec_cmd(cmd)

    ## bitpos1
    val = 0
    for i in range(2):
        pos = bit_pos[1 - i]
        tmp = pos << i*8
        val = val | tmp
        # print hex(tmp), hex(val)

    cmd = "-W hgroc_daq.data_bitpos1=" + hex(val)
    ret = exec_cmd(cmd)

def flush_fifos():
    cmd = "-W hgroc_daq.fifo_reset=0x01"
    ret = exec_cmd(cmd)

def check_fifo_content():
    cmd = "-R hgroc_daq.config"
    ret = exec_cmd(cmd)

    return int(ret,16) & 0x2f

def check_fifo_empty(ififo = 0):
    fifo_status = check_fifo_content()

    return (fifo_status >> ififo) & 1

def check_fifo_full():
    cmd = "-R hgroc_daq.config"
    ret = exec_cmd(cmd)

    #if len(ret) == 1:
    val = int(ret,16)
    #print "val", val, bin(val)
    #print "#####", 63 - val

    val = val & 0x3f

    # val == 0x3f if all 6 fifos are empty
    if val == 0: ## if all 6 fifos are filled
        return 1
    else:
        return 0

# read fifo
def read_fifos(fifos = range(6), pattern = "./test_", depth = 32769, wr_mode = "w"):
    '''
    if not check_fifo_full():
        print("FIFO not filled")
        return 0
    '''

    for ififo in fifos:
        if check_fifo_empty(ififo): continue

        try:
            read_fifo(ififo, pattern, depth, wr_mode)
        except ValueError:
            print("Error")
            return 0

    return 1

def read_fifo(ififo = 0, pattern = "./test_", depth = 32769, wr_mode = "w"):
    fname = pattern + "fifo%i.raw" % ififo

    #cmd = "-R hgroc_daq.fifo$j:$fifo_depth >> $fname"

    wr_pipe = ">" if wr_mode == "w" else ">>"

    cmd = "-R hgroc_daq.fifo%i:%i %s %s" %(ififo, depth, wr_pipe, fname)
    ret = exec_cmd(cmd)

def read_fifo2(ififo = 0, fname = "fifo.txt", depth = 32769, wr_mode = "w"):
    wr_pipe = ">" if wr_mode == "w" else ">>"

    cmd = "-R hgroc_daq.fifo%i:%i %s %s" %(ififo, depth, wr_pipe, fname)
    ret = exec_cmd(cmd)

def config_daq_controller(rst_str = 0, rst_stp = 0, resync_str = 0, resync_stp = 0):
    '''configure DAQ controller'''

    ## Set reset (digital)
    cmd = "-W hgroc_daq.event.rst_start=%i" %(rst_str)
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.rst_stop=%i" %(rst_stp)
    ret = exec_cmd(cmd)

    ## Set resynch_load
    cmd = "-W hgroc_daq.event.ReSyncLoad_ext_start=%i" %(resync_str)
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.ReSyncLoad_ext_stop=%i" %(resync_stp)
    ret = exec_cmd(cmd)

    ## Set other commands to 0
    cmds = ["ReSync_ext_start", "ReSync_ext_stop", "L1_ext_start", "L1_ext_stop",
            "Strobe_ext_start","Strobe_ext_stop","trig1_start","trig1_stop","trig2_start","trig2_stop"]

    for cmd in cmds:
        cmd = "-W hgroc_daq.event." + cmd + "=0"
        ret = exec_cmd(cmd)

    return 0

def set_reset_signal(start = 0, stop = 0):
    ## Set reset (digital)
    cmd = "-W hgroc_daq.event.rst_start=%i" %(start)
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.rst_stop=%i" %(stop)
    ret = exec_cmd(cmd)

    return

def set_resynch_signal(start = 0, stop = 0):
    ## Set resynch_load
    cmd = "-W hgroc_daq.event.ReSyncLoad_ext_start=%i" %(start)
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.ReSyncLoad_ext_stop=%i" %(stop)
    ret = exec_cmd(cmd)

    return

def reset_roc_digital():

    set_reset_signal(0,40)
    set_resynch_signal(60,100)

    cmd = "-W hgroc_daq.event.daq_stop=%i" %150
    ret = exec_cmd(cmd)

    trig_fsm()

    # reset signals to 0
    set_reset_signal()
    set_resynch_signal()

    return

def set_daq_stop(stop):
    stop *= 8

    cmd = "-W hgroc_daq.event.daq_stop=%i" %stop
    ret = exec_cmd(cmd)

    return

def set_fifo_capture_window(ififo, start, stop):
    start *= 8; stop *= 8

    cmd = "-W hgroc_daq.event.fifo%i_capture_start=%i" %(ififo, start)
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.fifo%i_capture_stop=%i" %(ififo, stop)
    ret = exec_cmd(cmd)

    return

def set_capture_window(start = 0, stop = 0):

    for ififo in range(6):
        set_fifo_capture_window(ififo, start, stop)

    set_daq_stop(stop)

    return

def set_gpio_p(start = 0, stop = 0):
    start *= 8; stop *= 8

    cmd = "-W hgroc_daq.event.gpio_p_start=%i" %start
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.gpio_p_stop=%i" %stop
    ret = exec_cmd(cmd)

def set_gpio_n(start = 0, stop = 0):
    print("!!! GPIO_N not usable as output from FW7! ")

#### Fast commands

def set_fcmd_l1a(start = 0, stop = 0):
    ''' Fast command L1A trigger, must be on a an integer multiple of 8 to have any effect '''
    start *= 8
    stop *= 8

    cmd = "-W hgroc_daq.event.fcmd_trig_start=%i" % int(start);
    ret = exec_cmd(cmd)

    cmd = "-W hgroc_daq.event.fcmd_trig_stop=%i" % int(stop);
    ret = exec_cmd(cmd)

    return

def set_fcmd_calib(start = 0, stop = 0):
    ''' Fast command Calib, must be on a an integer multiple of 8 to have any effect '''
    start *= 8; stop *= 8 # convert from 40 MHz BX to 320 clk

    cmd = "-W hgroc_daq.event.fcmd_calib_start=%i" % int(start);
    ret = exec_cmd(cmd)

    cmd = "-W hgroc_daq.event.fcmd_calib_stop=%i" % int(stop);
    ret = exec_cmd(cmd)

    return

def set_fcmd_orbit(start = 0, stop = 0):
    ''' Fast command Orbit, must be on a an integer multiple of 8 to have any effect '''
    start *= 8; stop *= 8 # convert from 40 MHz BX to 320 clk

    cmd = "-W hgroc_daq.event.fcmd_orbit_start=%i" % int(start);
    ret = exec_cmd(cmd)

    cmd = "-W hgroc_daq.event.fcmd_orbit_stop=%i" % int(stop);
    ret = exec_cmd(cmd)

    return

def set_fcmd_sync(start = 0, stop = 0):
    ''' Fast command Sync, must be on a an integer multiple of 8 to have any effect '''
    start *= 8; stop *= 8 # convert from 40 MHz BX to 320 clk

    cmd = "-W hgroc_daq.event.fcmd_sync_start=%i" % int(start);
    ret = exec_cmd(cmd)

    cmd = "-W hgroc_daq.event.fcmd_sync_stop=%i" % int(stop);
    ret = exec_cmd(cmd)

    return

def set_fcmd_dump(start = 0, stop = 0):
    ''' Fast command dump, must be on a an integer multiple of 8 to have any effect '''
    start *= 8; stop *= 8 # convert from 40 MHz BX to 320 clk

    cmd = "-W hgroc_daq.event.fcmd_dump_start=%i" % int(start);
    ret = exec_cmd(cmd)

    cmd = "-W hgroc_daq.event.fcmd_dump_stop=%i" % int(stop);
    ret = exec_cmd(cmd)

    return

def disable_fcmds():

    set_fcmd_sync()
    set_fcmd_orbit()
    set_fcmd_l1a()
    set_fcmd_calib()
    set_fcmd_dump()

    return

def disable_fcmds():

    set_fcmd_sync()
    set_fcmd_orbit()
    set_fcmd_l1a()
    set_fcmd_calib()
    set_fcmd_dump()

    return

### Legacy
def set_fcmds(orb = 0, l1a = 0, calib = 0):

    set_fcmd_orbit(start = orb, stop = orb + 1)
    set_fcmd_l1a(start = l1a, stop = l1a + 1)
    set_fcmd_calib(start = calib, stop = calib + 1)

    #To send Sync set Orbit and Calib to the same value
    #To send Dump set Calib and Trig to the same value
    #fast commands can be skewed with respect to clk40 and clk320 by adjusting hgroc_daq.delay.fcmd

## Output signals

def set_toa_trig1(bx, offset_320 = 0, length_320= 0 ):
    """ Set trig1 output: BX is in 40 MHz clock counts, offset and length in 320 MHz clock counts """

    start = int(bx * 8 + offset_320)
    stop = int(start + length_320)

    cmd = "-W hgroc_daq.event.trig1_start=%i" %start
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.trig1_stop=%i" %stop
    ret = exec_cmd(cmd)

    return

def set_toa_trig2(bx, offset_320 = 0, length_320= 0 ):
    """ Set trig2 output: BX is in 40 MHz clock counts, offset and length in 320 MHz clock counts """

    start = int(bx * 8 + offset_320)
    stop = int(start + length_320)

    cmd = "-W hgroc_daq.event.trig2_start=%i" %start
    ret = exec_cmd(cmd)
    cmd = "-W hgroc_daq.event.trig2_stop=%i" %stop
    ret = exec_cmd(cmd)

    return

## FSM/trigger

def set_trig_source(source = 1):
    ## trigger sources:
    # 0 - none, 1 - software (IPbus), 2 - ext (GPIO_N), 4 - probe TOT, 8 - probe TOA, 16 - continuous

    if source not in [0,1,2,4,8,16]:
        print("Warning! trigger source %i not in list of allowed sources!" %source)
        exit(0)

    cmd = "-W hgroc_daq.daq_start_source=%i" %source
    ret = exec_cmd(cmd)

    return

def set_trig_sync(sync_on = 0):
    # =0 for unsynchronized capture
    # =1 to synchronize each stream to the predefined pattern
    cmd = "-W hgroc_daq.synchro=%i" %sync_on
    ret = exec_cmd(cmd)

    return

def trig_fsm(synchro = 0, source = 1):
    set_trig_sync(synchro)
    set_trig_source(source)

    cmd = "-W hgroc_daq.daq_start=0"
    ret = exec_cmd(cmd)

    return

def bash_acq_loop(nevents, synchro = 0, source = 1):
    set_trig_sync(synchro)
    set_trig_source(source)

    cmd = "for i in `seq 1 %i`; do ./fmc_hgroc -W hgroc_daq.daq_start=0; done" %(nevents)
    ret = subprocess.check_output(cmd, shell=True,cwd = cwd).split()

    return ret
