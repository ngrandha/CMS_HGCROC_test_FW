# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
font = {'size'   : 20}
mpl.rc('font', **font)
df = pd.read_hdf('injection_allchans.h5')

#print(df)

chans = np.unique(df['chan'])
chans = [36]

"""
for c in chans :
    sub = (df.chan == c) & (df.channel == c) & (df.bx == 0)

    df_sub1 = df[sub].groupby("calibDAC")["adc"].median()

    plt.plot(df_sub1.index,df_sub1.values,label = "channel" + str(c)) 

plt.xlabel("calib_DAC")
plt.ylabel('ADC value')
plt.legend()
"""
a = True
def fit_adc(ser):
    i = 1
    while ser[ser.index[i]]>ser[ser.index[i-1]]:
        i+=1
    X = ser.index[1:i]
    [a,b] = np.polyfit(X,ser[X],1)
    return (a,b,np.array([-3*b/(2*a),2000]),a*np.array([-3*b/(2*a),2000])+b)

def fit_tot(ser):
    sub = (ser.values > 0) & (ser.values < 127)
    X = ser.index[sub]
    l = len(X)//5
    X = X[l:]
    [a,b] = np.polyfit(X,ser[X],1)
    return (a,b,np.array([-3*b/(2*a),2000]),a*np.array([-3*b/(2*a),2000])+b)
 
def plot():
    for c in chans :
        fig, axes = plt.subplots(figsize = (12,12))
        sub = (df.channel == c) & (df.bx == 0) & (df.chan == 0)
        df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
        df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
        
        a_adc,b_adc,X1,Y1 = fit_adc(df_sub1)
        a_tot,b_tot,X2,Y2 = fit_tot(df_sub2)
        mult_factor = a_adc/a_tot
        add_factor = b_adc - b_tot*mult_factor
        plt.plot(df_sub1.index,df_sub1.values,label = "ADC")
        plt.plot(X1,Y1,label = "ADC fit a="+str(round(a_adc,3))+' b='+str(round(b_adc,3)))
        if a :
            plt.plot(df_sub2.index,df_sub2.values*mult_factor + add_factor,label = "Adjusted TOT")
            plt.plot(X2,Y2*mult_factor+ add_factor ,label = "Multiplied TOT fit a="+str(round(a_tot,3))+' b='+str(round(b_tot,3)))
        else :
            plt.plot(df_sub2.index,df_sub2.values,label = "TOT")
            plt.plot(X2,Y2,label = "TOT fit a="+str(round(a_tot,3))+' b='+str(round(b_tot,3)))
        plt.xlabel("calib_DAC")
        plt.ylabel('value')
        plt.grid()
        plt.legend()
        plt.title('chan '+str(c))#+ " : multfactor =" +str(round(mult_factor,3))+', add_factor =' + str(round(add_factor,3)))
        plt.ylim(-50,1000)
        if a :
            plt.savefig('multfact/chan_'+str(c)+'_adjusted.png')
        else :
            plt.savefig('multfact/chan_'+str(c)+'_short.png')

faulty = [9,34]

def plot_all():
    dico = {'multfact' : [0]*72, 'thresh' : [0]*72 , 'plateau' : [0]*72}
    data_resu = pd.DataFrame(dico, columns = ['multfact','thresh','plateau'],index = range(0,72))
    for k in range(0,36):
        for c in [k,k+36]:
            if c not in faulty :
                print(c)
                sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
                df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
                df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
                a_adc,b_adc,X1,Y1 = fit_adc(df_sub1)
                a_tot,b_tot,X2,Y2 = fit_tot(df_sub2)
                mult_factor = a_adc/a_tot
                R = np.abs((a_tot*df_sub2.index + b_tot - df_sub2.values)/df_sub2.values)
                i = 0
                while R[i]>0.03:
                    i = i+1
                fig, axes = plt.subplots(nrows=2, ncols=1,figsize = (15,15), sharex = True)
                axes[0].plot(df_sub2.index,df_sub2.values, label = "TOT")
                axes[0].plot(df_sub2.index,a_tot * df_sub2.index + b_tot, label = "TOT fit, a="+str(round(a_tot,3))+' b='+str(round(b_tot,3)) )
                axes[1].plot(df_sub2.index,R,label = "Residuals, CalibDAC = " + str(round(df_sub2.index[i],3))+ ", TOT_Th =" + str(round(df_sub2.values[i],3)))
                axes[1].plot([0,2000],[0.03,0.03],label = '0.03 Threshold')
                axes[0].plot([0,2000],[round(b_tot,3),round(b_tot,3)], label = 'TOT_Pn')
                axes[0].plot([0,2000],[round(df_sub2.values[i],3),round(df_sub2.values[i],3)],label = 'TOT_TH') 
                axes[0].set_title("TOT residuals, chan"+str(c) + ", Slope="+str(round(a_tot,3))+', TOT_Pn='+str(round(b_tot,3))  + ", TOT_Th =" + str(round(df_sub2.values[i],3)))
                axes[1].set_xlabel("Calib_DAC")
                axes[0].set_ylabel("TOT")
                axes[1].set_ylabel("Residual fraction")
                axes[0].legend()
                axes[1].legend()
                axes[0].grid()
                axes[1].grid()
                data_resu['multfact'][c] = mult_factor
                data_resu['thresh'][c] = df_sub2.values[i]
                data_resu['plateau'][c] =b_tot
                plt.savefig('multfactcomplete2/chan_'+str(c)+'_residuals_complete.png')
                plt.close(fig)
    fig, axes = plt.subplots(nrows=3, ncols=1,figsize = (25,25), sharex = True)
    axes[0].plot(data_resu.index,data_resu.multfact, label = 'Mult factor')
    axes[1].plot(data_resu.index,data_resu.thresh, label = 'TOT Threshold')
    axes[2].plot(data_resu.index,data_resu.plateau, label = 'TOT Plateau') 
    axes[0].legend()
    axes[1].legend()
    axes[2].legend()
    axes[0].grid()
    axes[1].grid()
    axes[2].grid()
    axes[2].set_xlabel('Channel')
    axes[0].set_title('Evolution of injection related TOT variables')
    plt.savefig('multfactcomplete2/global_summary.png')
    

    
def plot_all_adc():
    dico = {'slope' : [0.0]*72, 'zerocross' : [0.0]*72}
    data_resu = pd.DataFrame(dico, columns = ['slope','zerocross'],index = range(0,72))
    for k in range(0,36):
        for c in [k,k+36]:
            if c not in faulty :
                print(c)
                sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
                df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
                a_adc,b_adc,X1,Y1 = fit_adc(df_sub1)
                fig, axes = plt.subplots(figsize = (12,12))
                plt.plot(df_sub1.index,df_sub1.values,label = 'ADC value')
                plt.plot(X1,Y1,label = "ADC fit")
                plt.xlabel('Calib_DAC')
                plt.ylabel('ADC')
                plt.title('ADC Chan'+str(c)+", Slope="+str(round(a_adc,3))+', zero_cross =' + str(round(b_adc,3)))
                plt.legend()
                plt.grid()
                plt.savefig('adccomplete/chan'+str(c)+'.png')
                data_resu['slope'][c]=a_adc
                data_resu['zerocross'][c]=b_adc
                
                plt.close(fig)
    
    fig, axes = plt.subplots(nrows=2, ncols=1,figsize = (25,25), sharex = True)
    axes[0].plot(data_resu.index,data_resu.slope, label = 'slope')
    axes[1].plot(data_resu.index,data_resu.zerocross, label = 'zerocross')
    axes[0].legend()
    axes[1].legend()
    axes[1].set_xlabel('Channel')
    axes[0].set_title('Evolution of ADC fit per channel')
    plt.savefig('adccomplete/adc_summary.png')

def plot_raw_tot(l):
    fig, axes = plt.subplots(figsize = (25,25), sharex = True)
    for c in l:
        k = c%36
        sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
        df_sub=df[sub]
        plt.plot(df_sub.calibDAC,df_sub.tot,'bo')
    plt.xlabel('Calib_DAC')
    plt.ylabel('TOT')
    plt.title("TOT values in channels " + str(l) )
    plt.grid()
    
def plot_raw_tot_hist(l):
    fig, axes = plt.subplots(figsize = (25,25), sharex = True)
    for c in l:
        k = c%36
        sub = (df.channel == c) & (df.bx == 0) & (df.chan == k) & (df.tot != 0)
        df_sub=df[sub]
        plt.hist(df_sub.tot,bins = range(50,200,2),alpha = 0.5, label= "channel" + str(c))
    plt.ylabel('TOT')
    plt.title("TOT histograms in channels " + str(l) )
    plt.legend()
    plt.grid()
        
def residual():
    for c in chans :
        fig, axes = plt.subplots(nrows=2, ncols=1,figsize = (15,15), sharex = True)
        sub = (df.channel == c) & (df.bx == 0) & (df.chan == 0)
        df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
        a_tot,b_tot,X2,Y2 = fit_tot(df_sub2)
        R = np.abs((a_tot*df_sub2.index + b_tot - df_sub2.values)/df_sub2.values)
        i = 0
        while R[i]>0.03:
            i = i+1
        print('Calib_DAC stop value would be : ' +str(df_sub2.index[i]))
        print('TOT Plateau value would be : ' +str(df_sub2.values[i]))
        axes[0].plot(df_sub2.index,df_sub2.values, label = "TOT")
        axes[0].plot(df_sub2.index,a_tot * df_sub2.index + b_tot, label = "TOT fit, a="+str(round(a_tot,3))+' b='+str(round(b_tot,3)) )
        axes[1].plot(df_sub2.index,R,label = "Residuals, CalibDAC = " + str(round(df_sub2.index[i],3))+ ", TOT_Th =" + str(round(df_sub2.values[i],3)))
        axes[1].plot([0,2000],[0.03,0.03],label = '0.03 Threshold')
        axes[0].plot([0,2000],[round(b_tot,3),round(b_tot,3)], label = 'TOT_Pn')
        axes[0].plot([0,2000],[round(df_sub2.values[i],3),round(df_sub2.values[i],3)],label = 'TOT_TH') 
        axes[0].set_title("TOT residuals, chan"+str(c) + ", Slope="+str(round(a_tot,3))+', TOT_Pn='+str(round(b_tot,3))  + ", TOT_Th =" + str(round(df_sub2.values[i],3)))
        axes[1].set_xlabel("Calib_DAC")
        axes[0].set_ylabel("TOT")
        axes[1].set_ylabel("Residual fraction")
        axes[0].legend()
        axes[1].legend()
        axes[0].grid()
        axes[1].grid()
        plt.savefig('multfact/chan_'+str(c)+'_residuals_complete.png')


### Analysis
        
def analysis():
    dico = {'multfact' : [0.0]*72, 'thresh' : [0.0]*72 , 'plateau' : [0.0]*72}
    data_resu = pd.DataFrame(dico, columns = ['multfact','thresh','plateau'],index = range(0,72))
    for k in range(0,36):
        for c in [k,k+36]:
            if c not in faulty :
                print(c)
                sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
                df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
                df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
                a_adc,b_adc,X1,Y1 = fit_adc(df_sub1)
                a_tot,b_tot,X2,Y2 = fit_tot(df_sub2)
                mult_factor = a_adc/a_tot
                R = np.abs((a_tot*df_sub2.index + b_tot - df_sub2.values)/df_sub2.values)
                i = 0
                while R[i]>0.03:
                    i = i+1
                data_resu['multfact'][c] = mult_factor
                data_resu['thresh'][c] = df_sub2.values[i]
                data_resu['plateau'][c] =b_tot
    return data_resu

df_param = analysis()

def parameters_analysis(df_param):
    dico = {'multfact_mean' : [0.0]*8, 'thresh_mean' : [0.0]*8 , 'plateau_mean' : [0.0]*8, 'multfact_rms' : [0.0]*8, 'thresh_rms' : [0.0]*8 , 'plateau_rms' : [0.0]*8}
    columns = ['multfact_mean','thresh_mean','plateau_mean','multfact_rms','thresh_rms','plateau_rms']
    title = ['multfact','thresh','plateau']
    data_resu = pd.DataFrame(dico, columns = columns ,index = range(0,8))
    for k in range(8):
        sub = [True] * 72
        for i in faulty:
            sub[i] = False
        df_sub = df_param[sub][9*k : 9*k+9]
        data_resu['multfact_mean'][k] = df_sub.multfact.mean()
        data_resu['thresh_mean'][k] = df_sub.thresh.mean()
        data_resu['plateau_mean'][k] =df_sub.plateau.mean()
        data_resu['multfact_rms'][k] = df_sub.multfact.std()
        data_resu['thresh_rms'][k] = df_sub.thresh.std()
        data_resu['plateau_rms'][k] =df_sub.plateau.std()
    for i in range(3):
        fig, axes = plt.subplots(nrows=2, ncols=1,figsize = (15,15), sharex = True)
        axes[0].plot(data_resu.index,data_resu[columns[i]],label = columns[i])
        axes[1].plot(data_resu.index,data_resu[columns[i+3]],label = columns[i+3])
        axes[0].legend()
        axes[1].legend()
        axes[0].grid()
        axes[1].grid()
        axes[1].set_xlabel('Trigger Cell')
        axes[0].set_title('Evolution of ' +title[i])
        plt.savefig('params/'+title[i]+'.png')
        plt.close(fig)
    return(data_resu)
    
def parameters_band(df_param):
    dico = {'multfact_mean' : [0.0]*8, 'thresh_mean' : [0.0]*8 , 'plateau_mean' : [0.0]*8, 'multfact_rms' : [0.0]*8, 'thresh_rms' : [0.0]*8 , 'plateau_rms' : [0.0]*8}
    columns = ['multfact_mean','thresh_mean','plateau_mean','multfact_rms','thresh_rms','plateau_rms']
    title = ['multfact','thresh','plateau']
    data_resu = pd.DataFrame(dico, columns = columns ,index = range(0,8))
    for k in range(8):
        sub = [False] * 72
        for i in range(9*k,9*k+9):
            if i not in faulty :
                sub[i] = True  
        df_sub = df_param[sub]
        data_resu['multfact_mean'][k] = df_sub.multfact.mean()
        data_resu['thresh_mean'][k] = df_sub.thresh.mean()
        data_resu['plateau_mean'][k] =df_sub.plateau.mean()
        data_resu['multfact_rms'][k] = df_sub.multfact.std()
        data_resu['thresh_rms'][k] = df_sub.thresh.std()
        data_resu['plateau_rms'][k] =df_sub.plateau.std()
    for i in range(3):
        fig, axes = plt.subplots(figsize = (15,15), sharex = True)
        plt.plot(data_resu.index,data_resu[columns[i]],label = 'mean')
        plt.plot(data_resu.index,data_resu[columns[i]]+ data_resu[columns[i+3]],'r',label ='rms')
        plt.plot(data_resu.index,data_resu[columns[i]]- data_resu[columns[i+3]],'r',)
        plt.legend()
        plt.grid()
        plt.xlabel('Trigger Cell')
        plt.title('Evolution of ' +title[i])
        for k in range(8):
            sub = [False] * 72
            for z in range(9*k,9*k+9):
                if z not in faulty :
                    sub[z] = True   
            df_sub = df_param[sub]
            n = len(df_sub)
            plt.plot([k]*n,df_sub[title[i]],'o')
        plt.savefig('params/'+title[i]+'_visu.png')
        plt.close(fig)
    return(data_resu)
    
def charge(TOT,ADC,th,pl,mu):
    if TOT > th:
        TOT1=TOT - pl
    else :
        TOT1 = th - pl
    if TOT != 0 :
        return TOT1*mu
    else :
        return ADC
    
    
tc_param = parameters_band(df_param)

def plot_charge_chan():
    for k in range(0,36):
        for c in [k,k+36]:
            if c not in faulty :
                print(c)
                chrg = []
                th = df_param['thresh'][c]
                pl = df_param['plateau'][c]
                mu = df_param['multfact'][c]
                sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
                df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
                df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
                n = len(df_sub1)
                for i in range(n):
                    chrg+=[charge(df_sub2.values[i],df_sub1.values[i],th,pl,mu)]
                fig, axes = plt.subplots(figsize = (12,12))
                plt.plot(df_sub1.index,df_sub1.values, label = "ADC")
                plt.plot(df_sub1.index,df_sub2.values, label = "TOT")
                plt.plot(df_sub1.index,chrg, label = "charge")
                plt.legend()
                plt.grid()
                plt.xlabel('CalibDAC')
                plt.title('Channel' + str(c))
                plt.savefig('charge_chan/channel' + str(c) + '.png')
                plt.close(fig)
        
def plot_charge_tc():
    df['tc']=df.channel//9
    for k in range(8):
        sub = (df.tc == k) & (df.bx == 0) & (df.chan == df.channel%36) #& (df.channel not in faulty)
        df_sub = df[sub].groupby('calibDAC').median()
        chrg=[]
        th = tc_param['thresh_mean'][k]
        pl = tc_param['plateau_mean'][k]
        mu = tc_param['multfact_mean'][k]
        for i in df_sub.index :
            chrg += [charge(df_sub.tot[i],df_sub.adc[i],th,pl,mu)]
        fig, axes = plt.subplots(figsize = (12,12))
        plt.plot(df_sub.index,df_sub.adc, label = "ADC")
        plt.plot(df_sub.index,df_sub.tot, label = "TOT")
        plt.plot(df_sub.index,chrg, label = "charge")
        plt.legend()
        plt.grid()
        plt.xlabel('CalibDAC')
        plt.title('Trigger cell' + str(k))
        plt.savefig('charge_tc/tc' + str(k) + '.png')
        plt.close(fig)
        
def plot_charge_comp():
        for k in range(0,36):
            for c in [k,k+36]:
                if c not in faulty :
                    print(c)
                    chrg_chan = []
                    th = df_param['thresh'][c]
                    pl = df_param['plateau'][c]
                    mu = df_param['multfact'][c]
                    sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
                    df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
                    df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
                    n = len(df_sub1)
                    for i in range(n):
                        chrg_chan+=[charge(df_sub2.values[i],df_sub1.values[i],th,pl,mu)]
                    fig, axes = plt.subplots(nrows=2, ncols=1,figsize = (12,12))
                    axes[0].plot(df_sub1.index,chrg_chan, label = "charge_chan")
                    axes[0].set_title('Channel' + str(c))
                    
                    tr=c//9
                    sub = (df.tc == tr) & (df.bx == 0) & (df.chan == df.channel%36) #& (df.channel not in faulty)
                    df_sub = df[sub].groupby('calibDAC').median()
                    chrg_tc=[]
                    th = tc_param['thresh_mean'][tr]
                    pl = tc_param['plateau_mean'][tr]
                    mu = tc_param['multfact_mean'][tr]
                    for i in df_sub.index :
                        chrg_tc += [charge(df_sub.tot[i],df_sub.adc[i],th,pl,mu)]
                    axes[0].plot(df_sub.index,chrg_tc, label = "charge_tc")             
                    axes[0].legend()
                    axes[0].grid()
                    
                    ch_ch = np.array(chrg_chan)
                    ch_tc = np.array(chrg_tc)
                    
                    res = abs((ch_ch - ch_tc))/ch_ch
                    axes[1].plot(df_sub.index,res,label = "Residuals")
                    
                    axes[1].legend()
                    axes[1].grid()
                    axes[1].set_xlabel('CalibDAC')
                    
                    plt.savefig('charge_comp/channel' + str(c) + '.png')
                    plt.close(fig)
