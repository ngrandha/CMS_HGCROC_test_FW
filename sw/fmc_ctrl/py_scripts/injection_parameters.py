# -*- coding: utf-8 -*-

import sys, os, fnmatch
import pandas as pd
import numpy as np
from argparse import ArgumentParser
from common import ROC,DAQ

def fit_adc(ser):
    i = 1
    while ser[ser.index[i]]>ser[ser.index[i-1]]:
        i+=1
    X = ser.index[1:i]
    [a,b] = np.polyfit(X,ser[X],1)
    return (a,b,np.array([-3*b/(2*a),2000]),a*np.array([-3*b/(2*a),2000])+b)

def fit_tot(ser):
    sub = (ser.values > 0) & (ser.values < 127)
    X = ser.index[sub]
    l = len(X)//5
    X = X[l:]
    [a,b] = np.polyfit(X,ser[X],1)
    return (a,b,np.array([-3*b/(2*a),2000]),a*np.array([-3*b/(2*a),2000])+b)
 
def chan_analysis(df,faulty):
    dico = {'multfact' : [0.0]*72, 'thresh' : [0.0]*72 , 'plateau' : [0.0]*72}
    data_resu = pd.DataFrame(dico, columns = ['multfact','thresh','plateau'],index = range(0,72))
    for k in range(0,36):
        for c in [k,k+36]:
            if c not in faulty :
                print(c)
                sub = (df.channel == c) & (df.bx == 0) & (df.chan == k)
                df_sub1 = df[sub].groupby("calibDAC")["adc"].median()
                df_sub2 = df[sub].groupby("calibDAC")["tot"].median()
                a_adc,b_adc,X1,Y1 = fit_adc(df_sub1)
                a_tot,b_tot,X2,Y2 = fit_tot(df_sub2)
                mult_factor = a_adc/a_tot
                R = np.abs((a_tot*df_sub2.index + b_tot - df_sub2.values)/df_sub2.values)
                i = 0
                while R[i]>0.03:
                    i = i+1
                data_resu['multfact'][c] =max(0,mult_factor)
                data_resu['thresh'][c] = max(0,df_sub2.values[i])
                data_resu['plateau'][c] =max(0,b_tot - b_adc/mult_factor)
    return data_resu

def tc_analysis(df_param,faulty):
    dico = {'multfact_mean' : [0.0]*8, 'thresh_mean' : [0.0]*8 , 'plateau_mean' : [0.0]*8, 'multfact_rms' : [0.0]*8, 'thresh_rms' : [0.0]*8 , 'plateau_rms' : [0.0]*8}
    columns = ['multfact_mean','thresh_mean','plateau_mean','multfact_rms','thresh_rms','plateau_rms']
    title = ['multfact','thresh','plateau']
    data_resu = pd.DataFrame(dico, columns = columns ,index = range(0,8))
    for k in range(8):
        sub = [False] * 72
        for i in range(9*k,9*k+9):
            if i not in faulty :
                sub[i] = True  
        df_sub = df_param[sub]
        data_resu['multfact_mean'][k] = df_sub.multfact.mean()
        data_resu['thresh_mean'][k] = df_sub.thresh.mean()
        data_resu['plateau_mean'][k] =df_sub.plateau.mean()
        data_resu['multfact_rms'][k] = df_sub.multfact.std()
        data_resu['thresh_rms'][k] = df_sub.thresh.std()
        data_resu['plateau_rms'][k] =df_sub.plateau.std()
    return data_resu

def main(indir):
    df = pd.read_hdf(indir)
    faulty = [9,34] #Needs implementing a detection of faulty channels
    print('Computing channel wise parameters')
    chan_param = chan_analysis(df,faulty)
    print('Computing TC wise parameters')
    tc_param = tc_analysis(chan_param, faulty)
    
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    
    for ch in range(72):
        if ch not in faulty:
            tc = ch//9
            half = tc//4
            tc_r = tc%4
            roc = ROC(roc_conf_fname)
            roc.add_sc_param('DigitalHalf',half,'Tot_TH'+str(tc_r),int(round(chan_param['thresh'][ch])))
            roc.add_sc_param('DigitalHalf',half,'Tot_P'+str(tc_r),int(round(chan_param['plateau'][ch])))
            roc.add_sc_param('DigitalHalf',half,'MultFactor',int(round(chan_param['multfact'][ch])))
            roc.save_config("configs_injection/roc_config_ChargeLin_Ch_"+str(ch)+".yaml")
    for tc in range(8):
        half = tc//4
        tc_r = tc%4
        roc = ROC(roc_conf_fname)
        roc.add_sc_param('DigitalHalf',half,'Tot_TH'+str(tc_r),int(round(tc_param['thresh_mean'][tc])))
        roc.add_sc_param('DigitalHalf',half,'Tot_P'+str(tc_r),int(round(tc_param['plateau_mean'][tc])))
        roc.add_sc_param('DigitalHalf',half,'MultFactor',int(round(tc_param['multfact_mean'][tc])))
        roc.save_config("configs_injection/roc_config_ChargeLin_TC_"+str(tc)+".yaml")
            



if __name__ == "__main__":

    parser = ArgumentParser()
    # parser arguments
    parser.add_argument("input_dir", type=str, default="./",
                        help="Input directory")

    args = parser.parse_args()
    main(args.input_dir)
