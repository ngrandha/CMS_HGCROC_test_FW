#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 1):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    #odir = "./data/injection/run_" + timestamp
    odir = "./data/injection/"#run_" + timestamp

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    # config inj

    ## disable all channels:
    if False:
        set_toa_threshold(255)
        set_tot_threshold(255)

        print("Disabling all channels")
        for chan in range(72):
            #disable_ch_inj(chan)
            mask_toa(chan, 1)
            mask_tot(chan, 1)

    chans = [10]
    # enable chans
    for chan in chans:
        enable_ch_inj(chan, range_capa = "low")
        mask_toa(chan, 0)
        mask_tot(chan, 0)

        set_probe_toa(chan, 1)
        sel_trigger_toa(chan, 1)

        set_probe_tot(chan, 1)
        sel_trigger_tot(chan, 1)

    phase = 14
    bx_offset = -3

    set_roc_parameter(df_params, "Top", "all","Phase", phase)
    print "Setting clock phase to %i" %phase , " with word" , ret

    strobe = 0

    # Trig 1 and Trig 2 offset and length in 320 MHz clock cycles
    trig1_offset = 0
    trig1_length = 7

    trig2_offset = 1
    trig2_length = 60

    set_toa_trig1(bx = strobe, offset_320 = trig1_offset, length_320 = trig1_length)
    set_toa_trig2(bx = strobe, offset_320 = trig2_offset, length_320 = trig2_length)

    #calib = strobe
    l1a = strobe + 10 + bx_offset + l1a_offset

    c_start = l1a + capture_offset
    ev_size = 40
    c_stop = c_start + ev_size + 1

    print strobe, l1a, c_start, c_stop

    # Fast CMDs
    #set_fcmds(orb = 1e4, l1a = l1a, calib = calib)
    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib()
    set_fcmd_orbit()
    set_fcmd_sync()
    set_fcmd_dump()

    #set_gpio_p(calib, calib +1)
    set_gpio_p(strobe, strobe + 1)
    set_gpio_n(l1a, l1a +1)

    set_capture_window(c_start, c_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
    #print strobe, l1a, c_start, c_stop, ev_size

    calib_dac = 500

    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all","Calib_dac", phase)

    #outdir = odir + "/calibDAC_%i" %calib_dac
    outdir = odir + "/"
    outdir = os.path.abspath(outdir) + "/"

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    ## selRawData
    select_raw_data(1)

    flush_fifos()
    bash_acq_loop(n_events, 0)

    #print "Fifo full?", check_fifo_full()
    #print("Reading data...")
    #wr_mode = "w" if event == 0 else "a"
    if check_fifo_full():
        for ififo in range(0,6):
            try:
                read_fifo(ififo,outdir + "/", ev_size * n_events)
                continue
            except ValueError:
                print("Error")

    # check fifo is empty
    #print "Fifo full?", check_fifo_full()

    for ch in chans:
        disable_ch_inj(ch)

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
