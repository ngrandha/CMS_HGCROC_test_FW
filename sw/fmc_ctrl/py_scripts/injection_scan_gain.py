# -*- coding: utf-8 -*-
#!/usr/bin/python
import sys, os, subprocess, datetime
import fpga_control as fpga
from config import ROC, DAQ
from common_acq import *
from common import *

high = 0
phases = [2, 1, 2]
def acq(n_events = 50):

    # initialize DAQ and ROC objects
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs_injection/roc_config_ChargeLin_TC_0.yaml"
    roc = ROC(roc_conf_fname)

    ## configure acquisition
    daq = configure_calib_acq(daq, n_events, n_bxs = 5)

    ## Output folder
    odir = make_outdir("injection")

    ## save DAQ config to outdir
    daq.save_config(odir + "daq_config.yaml")

    ## Calib DAC values to scan
    calib_dacs = [-1] + range(0,2000,50)

    ## disable all channels:
    print("Disabling all channels")
    roc.set_parameter("ch", "all", "LowRange", 0)
    
    cf_comp = 200 #code = 0
    roc.set_parameter("GlobalAnalog", "all", "Cf_comp", 200)

    cf_codes = [0,4,10]
    rf_codes = range(1,15,1)

    rc_ref = 33.3*400
    rf_cf_vals = []
    
    i = 0

    for cf_code in cf_codes:
        roc.set_parameter('GlobalAnalog',"all","Cf",cf_code)
        cf_val = compute_cf(cf_code)
        cf_val += cf_comp

        #print "# Cf: code, value:", cf_code, cf_val
        print "# Cf: value:", cf_val

        for rf_code in rf_codes:
            rf_val = compute_rf(rf_code)
            roc.set_parameter("GlobalAnalog", "all", "Rf",rf_code)
            
            
            if abs(rf_val * cf_val - rc_ref) / rc_ref < 0.3:
                #print "## Rf: code, value:", rf_code, rf_val
                print "## Rf: value:", rf_val
                
                print "Setting clock phase to  " + str(phases[i])
                roc.set_parameter("Top", 0,"Phase", phases[i])
                
                ## Channel loop
                for chan in range(0,36):
                    chans = [chan, chan+36]
                    print("Enabling channels:")
                    print(chans)
            
                    # Enable channels for injection
                    for ch in chans:
                        if not high :
                            roc.set_parameter("ch", ch, "LowRange", 1)
                        else : 
                            roc.set_parameter("ch", ch, "HighRange", 1)
                            roc.set_parameter('Top','all',"Phase",12)
                            
                            
                    for calib_dac in calib_dacs:
                        print("Setting calib DAC to %i" % calib_dac)
                        if calib_dac >= 0:
                            roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", calib_dac)
                            roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 1)
                        else:
                            roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", 0)
                            roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 0)
            
                        outdir = odir + "Rf_"+str(rf_val)+"/Cf_"+str(cf_val)+"/phase_"+str(phases[i])+"/chan_"+str(chan)+"/calibDAC_"+str(calib_dac)+"/"
                        outdir = os.path.abspath(outdir) + "/"
                        if not os.path.exists(outdir):
                            os.makedirs(outdir)
            
                        ## save ROC config to outdir
                        roc.save_config(outdir + "roc_config.yaml")
            
                        fpga.flush_fifos()
                        fpga.bash_acq_loop(daq.n_events, 0)
            
                        # read data
                        fpga.read_fifos([4,5],outdir + "/", daq.data_size)
                        # read triger
                        fpga.read_fifos(range(4),outdir + "/", daq.tp_data_size)
                break

                ## end calib loop
        
                # disable channels
                for ch in chans:
                    if not high:
                        roc.set_parameter("ch", ch, "LowRange", 0)
                    else :
                        roc.set_parameter("ch", ch, "HighRange", 0)
                i+=1

    ## end chan loop
    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
