import sys,os, subprocess, datetime
from common import *
import fpga_control as fpga

ev_size = 43

timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
odir = "./data/voltage_analysis/run_" + timestamp
odir = os.path.abspath(odir) + "/"
print "Output dir:"
print odir
roc_conf_fname = "configs/roc_config.yaml"
roc = ROC(roc_conf_fname)
daq = DAQ("configs/daq_params.yaml")

def configure_acq(daq, n_events = 1, n_bxs = 1):
    #n_bxs = number of consecutive L1A (max for same Orbit is 13)

    if n_events < n_bxs:
        print(80*"#")
        print("# Nevents requested lower than #L1A: using single L1A")
        print(80*"#")
        n_bxs = 1

    n_events /= n_bxs

    # Configure fast CMDs
    fpga.disable_fcmds()

    strobe = 0
    orbit = strobe
    fpga.set_fcmd_orbit(orbit, orbit + 1)

    l1a_start = orbit + l1a_offset #- 1
    l1a_stop = l1a_start + 1

    if n_bxs > 1:
        l1a_stop += capture_offset
        l1a_stop += ev_size * (n_bxs - 1)
    #print l1a_start, l1a_stop

    fpga.set_fcmd_l1a(l1a_start, l1a_stop)

    # Data capture
    c_start = l1a_start + capture_offset
    c_stop = c_start + ev_size * n_bxs
    fpga.set_capture_window(c_start, c_stop)
    #fpga.set_gpio_p(c_start, c_stop)

    #print l1a_start, l1a_stop, c_start, c_stop

    data_size =ev_size * n_events * n_bxs

    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
        data_size = ev_size * n_events

    daq.n_events = n_events
    daq.n_bxs = n_bxs
    daq.data_size = data_size

    return daq

#roc.set_parameter("ReferenceVoltage","all","Inv_vref",260)
#roc.set_parameter("ReferenceVoltage","all","Noinv_vref",0)

daq = configure_acq(daq,10,1)

data_size = daq.data_size


for i in range(0,513,32):
    roc.set_parameter('ReferenceVoltage','all','Inv_vref',i)
    print('Processing for v_ref_inv =' +str(i))
    for j in range(0,513,32):
        roc.set_parameter('ReferenceVoltage','all','Noinv_vref',j)
        outdir = odir + "/Vrefinv_"+str(i)+"/Vrefnoinv_"+str(j)
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        flush_fifos()
        bash_acq_loop(10,0)
        read_fifos([4,5], outdir + "/", data_size)

print('Complete')
