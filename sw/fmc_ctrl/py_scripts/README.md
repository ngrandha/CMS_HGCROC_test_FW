# Python wrapper for fmc_hgroc

## Python package installation
If not already done, install packages for python:
`pip install --user -r requirements.txt`

### HGCROC analysis submodule
If you clone the repository use the option `--recurse-submodules` to clone the submodules as well: 
```
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/CMS_HGCROC_tests/CMS_HGCROC_test_FW.git
```

In order to "load" the submodule into an existing repository execute this command: `git submodule update --init`.

To update the submodule execute `git submodule update`.

## Startup sequence:
1. `./init.py` for initial configuration of the HGCROCv2 (Vbg, PLL, etc)
2. `./scan_delays.py` to find best link delays (finding the best delay for the idle pattern, i.e. the character recognition)
3. `./scan_bitpos.py` to find best bit positions for each link (in fact, it is the character alignment in the 32b words)
4. `./scan_capture_offset.py` determine offset (in BX) between L1A issuing and capture start window

The DAQ "config" is saved in the file `configs/daq_params.yaml` after the execution of every script. Normally, no manual interaction is needed to change settings.

## Further scripts

In the following the capture is done unsynronised (no auto word alignment) such that always 1 full event is being captured:
header + 38 words + 4 idle = 43 words

* `simple_daq.py`: simple data acquisition (argument = number of events).
* `sampling_scan.py`: scan the phase shifter of the internat40 MHz clock. With injection to sample the pulse shape. (Also scanning the BX offset of L1A wrt calib FastCMD)
* `injection_scan.py`: scan the calibration dac for injection

## ROC configuration class
ROC class similar to the DAQ class, which allows loading a default ROC slow control (sc) configuration from a file, add/modify parameters and configure the ROC through the set/read_parameter functions similar to before:

Initialize ROC object (load config from yaml file into `sc` object)
```
roc = ROC("configs/roc_config.yaml") 
```

Set ROC parameter (issues the I2C transaction, but does not modify `sc` object)
```
roc.set_parameter("ch", "1", "LowRange", 1)
```

Set parameter values in `sc` object
```
roc.add_sc_param("ReferenceVoltage","all","LowRange", 1)                                                                                                                                                
roc.add_sc_param("ch",1,"Sel_trigger_toa", 1)                                                                                                                                                           
```

Configure all parameters from SC object
```
roc.config_sc_params()
```

Save config (`sc` object content) into a yaml file:
```
roc.save_config("configs/roc_test.yaml")
```

A new parsing allows setting blockIds in a range or providing a list:                                                                                                                                                                                                
```
roc.set_parameter("ch", "1-20", "LowRange", 1)
roc.set_parameter("ch", "1,2,3", "LowRange", 1)
```

The difference between `set_parameter` and `add_sc_param` is whether the parameter will be configured, or whether it will be added to the SC parameter list, respectively.

The default `roc_config.yaml` is used in the init file from now, but any other file can be provided in the argument to `init.py`. 

Default config file:
```
sc:                                                                                                                                                                                                         
  DigitalHalf:                                                                                                                                                                                              
    all:                                                                                                                                                                                                    
      L1Offset: 10                                                                                                                                                                                          
  GlobalAnalog:                                                                                                                                                                                             
    all:                                                                                                                                                                                                    
      Delay87: 1                                                                                                                                                                                            
      Delay9: 2                                                                                                                                                                                             
  ReferenceVoltage:                                                                                                                                                                                         
    all:                                                                                                                                                                                                    
      Vbg_1v: 4                                                                                                                                                                                             
  Top:                                                                                                                                                                                                      
    all:                                                                                                                                                                                                    
      BIAS_I_PLL_D: 63                                                                                                                                                                                      
      EN_HIGH_CAPA: 1                                                                                                                                                                                       
      EN_LOCK_CONTROL: 0                                                                                                                                                                                    
      ERROR_LIMIT_SC: 0                                                                                                                                                                                     
conf_fname:                                                                                                                                                                                                 
  configs/roc_config.yaml                                                                                                                                                                                   
bitmap_fname:                                                                                                                                                                                               
  configs/HGCROCv2_I2C_Params.csv           
```