#!/usr/bin/env python2
import sys
from fmc_wrapper import exec_cmd

###### I2C param
parfile = "chan_subaddr.csv"
import pandas as pd
df_sub_adds = pd.read_csv(parfile, sep = ",")

## HACK for calib/CM channels:
## define their IDs as follows:
aux_ch_ids = {
    -1: "calib0",
    -2: "cm0",
    -3: "cm1",
    72: "calib1",
    73: "cm2",
    74: "cm3"
}

def get_chan_subadd(channel = 0):

    if channel in aux_ch_ids:
        ch_string = aux_ch_ids[channel]
    else:
        ch_string = "ch%i" %channel

    sel = df_sub_adds["chan"] == ch_string

    if sum(sel) != 1:
        print("Chan: " + ch_string + " does not exist in list!")
        return -1

    sub_add = int(df_sub_adds[sel]["subadd"].values[0])
    return sub_add

def get_par_addr(sub_addr, hgroc_reg):

    full_addr = (sub_addr << 5) + hgroc_reg

    val_R0 = full_addr & 0xff
    val_R1 = ((full_addr) >> 8) & 0xff

    #print val_R0, val_R1

    return hex(val_R0), hex(val_R1)

############# I2C

### R0
def read_I2C_R0():
    cmd = " -J 0x28"
    ret = exec_cmd(cmd)
    print("Read R0", ret)

def set_I2C_R0(val):
    cmd = " -J 0x28=" + str(val)
    ret = exec_cmd(cmd)
    #print("Setting R0, ret:", ret)

### R1
def read_I2C_R1():
    cmd = " -J 0x29"
    ret = exec_cmd(cmd)
    print("Read R1", ret)

def set_I2C_R1(val):
    cmd = " -J 0x29=" + str(val)
    ret = exec_cmd(cmd)
    #print("Setting R1, ret:", ret)

### R2
def read_I2C_R2():
    cmd = " -J 0x2A"
    ret = exec_cmd(cmd)
    val = ret.split()[-1]

    #print("Read R2: " + val)
    return val

def set_I2C_R2(val):
    cmd = " -J 0x2A=" + str(val)
    ret = exec_cmd(cmd)
    #print("Setting R2, ret:", ret)

### R3
def read_I2C_R3():
    cmd = " -J 0x2B"
    ret = exec_cmd(cmd)
    val = ret.split()[-1]
    #print("Read R3: " + val)
    return val

def set_I2C_R3(val):
    cmd = " -J 0x2B=" + str(val)
    ret = exec_cmd(cmd)

def bash_read_R3_loop(n_reg):
    cmd = "for i in `seq 1 %i`; do ./fmc_hgroc -J 0x2B; done" %(n_reg)
    ret = subprocess.check_output(cmd, shell=True,cwd = cwd).split()

    return ret


### All together

def set_I2C_param(R0, R1, R2):

    set_I2C_R0(R0);
    set_I2C_R1(R1);
    set_I2C_R2(R2);

def read_I2C_param(R0, R1):

    set_I2C_R0(R0);
    set_I2C_R1(R1);
    ret = read_I2C_R2();

    return ret

### save setting of I2C registers (with previous reading)

def set_I2C_param_mask(R0, R1, param, mask):

    cur_val = read_I2C_param(R0, R1)
    cur_val = int(cur_val, 16)
    inv_mask = 0xff - mask

    cur_val = cur_val & inv_mask # bits that should not change

    # provided param is in correct place
    new_val = cur_val + param
    set_I2C_param(R0, R1, new_val)

    return


def main():

    if len(sys.argv) == 3:
        print sys.argv
        sub_addr = int(sys.argv[1])
        reg = int(sys.argv[2])

        print sub_addr, reg

        print get_par_addr(sub_addr, reg)
    else:
        print("Usage: ./roc_i2c sub_address register")

if __name__ == "__main__":

    main()
