# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-

import sys, os, subprocess, datetime, fnmatch, glob
import fpga_control as fpga
import numpy as np
import pandas as pd
import hgcroc_analysis.process_raw as praw
import hgcroc_analysis.process_ped_scan as pped
from config import ROC, DAQ
from pedestal_scan import *


def fit(df): #fits after the earliest maximum
    imax = df.index[df == df.max()].max()
    #print imax, df.max()

    X1 = df.index[(df > df.min()) & (df.index >= imax)]

    [a1,b1] = np.polyfit(X1,df[X1],1)

    Y1 = a1*X1 + b1

    #print X1, df[X1], a1, b1

    return X1,Y1,a1,b1

def scan_vref_noinv(roc,daq,odir,vrefs=range(0,512,10)):
    roc.set_parameter("ReferenceVoltage","all","Noinv_vref",0)
    print("V_ref_noinv set to zero")
    daq = configure_acq(daq,10,1)

    #Manual acquisition over 1D scan, step can be adjusted

    for i in vrefs:
        roc.set_parameter('ReferenceVoltage','all','Inv_vref',i)
        fpga.flush_fifos()
        fpga.bash_acq_loop(10, 0)
        outdir = odir + "/Vrefinv_"+str(i)
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        fpga.read_fifos([4,5], outdir + "/", daq.data_size)

def get_vref_from_scan(df_fname,vref_type = "Vrefinv",target_pedestal = 100):
    df = pd.read_hdf(df_fname)

    best_vref = {}

    for half in [0,1]:
        if half == 0:
            sub = (df.channel < 36)
        else:
            sub = (df.channel > 35)

        df_sub = df[sub].groupby(vref_type)["adc"].median()

        #Once the 1D scan is complete, we fit the data received from the latest maximum
        X1,Y1,a1,b1 = fit(df_sub)

        x1 = int(round((target_pedestal - b1)/a1))
        x1 = max(0,x1)
        print('Picking ' +str(x1) + ' as %s for half %i' % (vref_type,half) )

        best_vref[half] = x1

    return best_vref

def scan_vref_inv(roc,daq,odir,vrefs=range(0,512,10)):
    for i in vrefs:
        roc.set_parameter('ReferenceVoltage','all','Noinv_vref',i)
        fpga.flush_fifos()
        fpga.bash_acq_loop(10, 0)
        outdir = odir + "/Vrefnoinv_"+str(i)
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        fpga.read_fifos([4,5], outdir + "/", daq.data_size)

def adjust_local_pedestals(daq, roc):


    #Acquire data for pedestal analysis, similar to ped_scan with custom address
    out1 = acq(daq,roc,"ped_adj",10,"", 8)
    print('First acquisition done, treating data')

    # converting data to dataframe
    pped.main(out1)
    out1b = out1 +"/dataframe.h5"

    # reading dataframe
    df = pd.read_hdf(out1b)
    df = df.sort_values(["channel","ped_DAC"])

    chans = df.channel.unique()
    test = np.ones(np.size(chans))
    #Some channels can be faulty (eg : stay to zero) so we have to ignore them (this is the test array)
    #Maybe there are more kind of faults that channels have ?
    print('Eliminating faulty channels')

    for i in range(np.size(chans)) :
        chan = chans[i]
        sub = (df.channel==chan)
        if df[sub].groupby("ped_DAC")["adc"].mean().mean() < 5:
            test[i] = 0
            print("eliminated channel " + str(chan))

    #The optimal value is the higher adc at pedestal zero
    sub = (df.ped_DAC == 0)
    # reject saturated values
    sub &= df.adc < 1023
    # find max value
    maxi = df[sub]['adc'].max()

    print("Optimal adc value is "+str(maxi))

    max_ped_DAC = df.ped_DAC.max()
    sub = (df.ped_DAC == max_ped_DAC)
    mini = df[sub]['adc'].min()

    if mini > maxi:
        print("WARNING : The optimal value is not working for all channels")

    pDAC = []

    #Setting each pedestal
    for i in range(np.size(chans)):
        chan = chans[i]
        if test[i] > -1:
            sub = (df.channel==chan)
            # exclude saturated and 0 values
            sub &= (df.adc < 1023) & (df.adc > 0)
            # check if entries left
            if np.count_nonzero(sub) == 0:
                pDAC += [-1]
                continue

            df_sub = df[sub].groupby("ped_DAC")["adc"].mean()
            # new meth
            a,b = np.polyfit(df_sub.index,df_sub.values,1)
            ## compute ped value for maxi
            x = int((maxi - b)/a)
            pDAC += [x]
        else:
            pDAC +=[-1]

    print("Pedestal values are : ")
    print(pDAC)

    chans = np.array(range(-3,75))

    dicto = [("cm",1) , ("cm",0) , ("calib",0)] + [("ch", i ) for i in range(72)] + [("calib",1) , ("cm",2) , ("cm",3)]

    for i in range(78):
        if pDAC[i] != -1:
            roc.set_parameter(dicto[i][0],dicto[i][1],"Ref_dac_inv", pDAC[i])
            roc.add_sc_param(dicto[i][0],dicto[i][1],"Ref_dac_inv", pDAC[i])

    print("Pedestals has been adjsuted locally")
    roc.save_config(out1 + "/roc_config_ped_Adj.yaml")

    return roc

def main(vref_step = 10):
    #Create DAQ and ROC
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    roc = ROC(roc_conf_fname)

    odir = make_outdir("pedvol","")

    roc = adjust_local_pedestals(daq, roc)

    print("Finding appropriate reference voltages")

    print("1D scan of v_ref_inv done, treating data")
    out2 = odir+'vol1/'
    os.makedirs(out2)

    print('Starting 1D scan with step' + str(vref_step))
    scan_vref_noinv(roc,daq,out2,vrefs=range(0,512,vref_step))

    praw.main(out2)
    out2b = out2 +"/dataframe.h5"

    vrefs_inv = get_vref_from_scan(out2b,"Vrefinv",target_pedestal = 100)

    for half,vref in vrefs_inv.iteritems():
        #Setting parameters given by fit
        roc.set_parameter('ReferenceVoltage',half,'Inv_vref',vref)
        #saving parameters given by fit
        roc.add_sc_param('ReferenceVoltage',half,'Inv_vref', vref)

    #Manual acquisition over 1D scan, step can be adjusted
    out3 = odir+'vol2/'

    print('Starting 1D scan with step ' + str(vref_step))
    scan_vref_inv(roc,daq,out3,vrefs=range(0,512,vref_step))

    print("1D scan of v_ref_noinv done, treating data")
    praw.main(out3)
    out3b = out3 +"/dataframe.h5"

    vrefs_noinv = get_vref_from_scan(out3b,"Vrefnoinv",target_pedestal = 20)

    for half,vref in vrefs_noinv.iteritems():
        #Setting parameters given by fit
        roc.set_parameter('ReferenceVoltage',half,'Noinv_vref',vref)
        #saving parameters given by fit
        roc.add_sc_param('ReferenceVoltage',half,'Noinv_vref', vref)


    roc.save_config(odir + "/roc_config_ped_Adj.yaml")
    print('Pedestals and reference voltages successfully initialized')

if __name__ == "__main__":

    if len(sys.argv) > 1:
        step = int(sys.argv[1])
        main(step)
    else:
        main()
