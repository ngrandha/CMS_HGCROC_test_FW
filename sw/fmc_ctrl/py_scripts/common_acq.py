#!/usr/bin/python
import sys, os, subprocess, datetime
import fpga_control as fpga
from config import ROC, DAQ

# define event size and number of consecutive triggers (L1A)
ev_size = 43

def configure_calib_acq(daq, n_events = 1, n_bxs = 5):
    n_bxs = 5 # number of consecutive L1A (max for same Orbit is 12)
    data_size = ev_size * n_events * n_bxs

    if data_size > daq.max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = daq.max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
        data_size = ev_size * n_events

    ## Fast commands
    fpga.disable_fcmds()

    strobe = 0
    # calibration pulse position
    calib = strobe + 1
    calib_stop = calib + 1
    fpga.set_fcmd_calib(calib, calib_stop)
    #fpga.set_gpio_p(calib, calib_stop)

    orbit = calib + daq.calib_offset
    fpga.set_fcmd_orbit(orbit, orbit + 1)

    # data readout
    l1a_start = orbit + daq.l1a_offset
    l1a_stop = l1a_start + 1

    if n_bxs > 1:
        l1a_stop += daq.capture_offset
        l1a_stop += ev_size * (n_bxs - 1)

    fpga.set_fcmd_l1a(l1a_start, l1a_stop)

    c_start = l1a_start + daq.capture_offset
    c_stop = c_start + ev_size * n_bxs
    fpga.set_capture_window(c_start, c_stop)

    #print l1a_start, c_start, c_stop

    ## trigger readout
    tp_capture_offset = daq.capture_offset + 5
    tp_ev_size = 1 * n_bxs
    tp_data_size = n_events * tp_ev_size

    tp_c_start = calib + tp_capture_offset
    tp_c_stop = tp_c_start + tp_ev_size

    for i in range(4):
        fpga.set_fifo_capture_window(i, tp_c_start, tp_c_stop)
    #print tp_c_start, tp_c_stop

    # store DAQ info
    daq.n_events = n_events
    daq.n_bxs = n_bxs
    daq.data_size = data_size
    daq.tp_data_size = tp_data_size

    return daq

def make_outdir(run_type = "acq",suffix = ""):
    # Create output folder
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/%s/run_" %run_type + timestamp + suffix
    odir = os.path.abspath(odir) + "/"
    if not os.path.exists(odir): os.makedirs(odir)
    print("Output dir:")
    print(odir)
    return odir
