#!/usr/bin/python
import sys, os, subprocess, datetime
import fpga_control as fpga
from config import ROC, DAQ
from common_acq import *

def acq(n_events = 10, suffix = ""):

    # initialize DAQ and ROC objects
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    roc = ROC(roc_conf_fname)

    ## configure acquisition
    daq = configure_calib_acq(daq, n_events, n_bxs = 5)

    ## Set DAC values
    calib_dac = 500
    print "Setting calib_dac", calib_dac
    roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 1)
    roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", calib_dac)

    ## Output folder
    suffix = "_calDAC_%i"%calib_dac + suffix
    odir = make_outdir("sampling_scan",suffix)

    ## disable all channels:
    print("Disabling all channels")
    #roc.set_parameter("ch", "all", "LowRange", 0)

    ## save DAQ config to outdir
    daq.save_config(odir + "daq_config.yaml")

    ## Channel loop
    for chan in range(0,36,90):
        chans = [chan, chan+36]
        print("Enabling channels:")
        print(chans)

        # Enable channels for injection
        for ch in chans:
            roc.set_parameter("ch", ch, "LowRange", 1)
            #roc.set_parameter("ch", ch, "HighRange", 0)

        # Loop through phase parameters
        for phase in range(16):
            print("Setting clock phase to %i" %phase)
            roc.set_parameter("Top", "all","Phase", phase)

            outdir = odir + "/chan_%i/phase_%i" %(chan,phase)
            outdir = os.path.abspath(outdir) + "/"

            outdir = os.path.abspath(outdir) + "/"
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            ## save ROC config to outdir
            roc.save_config(outdir + "roc_config.yaml")

            fpga.flush_fifos()
            fpga.bash_acq_loop(daq.n_events, 0)

            # read data
            fpga.read_fifos([4,5],outdir + "/", daq.data_size)
            # read triger
            fpga.read_fifos(range(4),outdir + "/", daq.tp_data_size)

        ## end phase loop

        # Disable channels
        for ch in chans:
            #disable_ch_inj(ch)
            roc.set_parameter("ch", ch, "LowRange", 0)
            #roc.set_parameter("ch", ch, "HighRange", 0)

    ## end chan loop
    return

def main():

    n_events = 10
    suf = ""
    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)

    if len(sys.argv) > 2:
        suf = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suf)

    acq(n_events, suf)

if __name__ == "__main__":

    main()
