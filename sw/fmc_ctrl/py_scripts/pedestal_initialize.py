import sys, os, subprocess, datetime
from common import *
import fpga_control as fpga

roc=ROC("configs/roc_config.yaml")
print("ROC object initialized")

ped_values = [14, 15, 15, 25, 17, 23, 17, 14, 26, 18, 22, 18, -1, 16, 16, 23, 10, 17, 19, 16, 17, 16, 20, 16, 20, 18, 13, 24, 10, 16, 21, 20, 22, 17, 22, 20, 17, 14, 21, 8, 1, 5, 0, 8, 2, 9, 8, 2, 9, 3, 9, 2, 4, 11, 6, 10, 10, 5, 6, 5, 10, 6, 9, 12, 2, 7, 5, 16, 7, 7, 8, 10, 11, 6, 9, 6, 6, 5]

chans = np.array(range(-3,75))

dicto = [("cm",1) , ("cm",0) , ("calib",0)] + [("ch", i ) for i in range(72)] + [("calib",1) , ("cm",2) , ("cm",3)]

for i in range(78):
    if ped_values[i] != -1 :
        print(dicto[i])
        #roc.add_sc_param("ch",chans[i],"Ref_dac_inv",ped_values[i])
        roc.set_parameter(dicto[i][0],dicto[i][1],"Ref_dac_inv", ped_values[i])

#roc.config_sc_params()
#roc.save_config("configs/roc_test.yaml")
#print("config file updated")
print("parameters have been set locally")

roc.set_parameter("ReferenceVoltage","all","Inv_vref",329)
roc.set_parameter("ReferenceVoltage",1,"Noinv_vref",149)
roc.set_parameter("ReferenceVoltage",0,"Noinv_vref",123)

print("parameters have been set globally")
print("V_ref_inv = 329 ; V_ref_no_inv = 135")
