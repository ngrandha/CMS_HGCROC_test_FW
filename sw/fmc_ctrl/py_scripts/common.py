#!/usr/bin/python
import os, subprocess, time

from fmc_wrapper import exec_cmd
from fpga_control import *
#from roc_i2c import *

from hgcroc_i2c_wrapper import *
df_params = load_param_d_map()

from config import *

daq = DAQ("configs/daq_params.yaml")
#print daq

max_fifo_size = daq.max_fifo_size
idle_word = daq.idle_word # latency wrt L1A
calib_offset = daq.calib_offset # due to ROC align buffer?
align_offset = daq.align_offset # due to ROC align buffer?
l1a_offset = daq.l1a_offset # ROC SC parameter
capture_offset = daq.capture_offset # latency wrt L1A

### ROC GAIN calculator

def compute_cf(value):
    val_enabled_bits = np.array( [1*(((value >> i) & 1) != 0) for i in range(4)])

    cf_vals = np.array([50, 100, 200, 400])
    cf_value = np.sum(cf_vals * val_enabled_bits)

    return cf_value

def set_Cf(value, half = "all"):
    """ Set feedback resistance """

    set_roc_parameter(df_params, "GlobalAnalog", half, "Cf", value)

    cf_value = compute_cf(value)

    return cf_value

def read_Cf(half = "all"):
    """ Set feedback resistance """

    ret = read_roc_parameter(df_params, "GlobalAnalog", half, "Cf")

    if half == "all":
        return [compute_cf(value) for value in ret.values()]
    else:
        return compute_cf(ret)

def compute_rf(value):
    if value == 0: return None

    val_enabled_bits = np.array( [1*(((value >> i) & 1) != 0) for i in range(4)])

    rf_vals = np.array([100, 66.66, 50, 25])
    rf_value = 1/np.sum((1/rf_vals) * val_enabled_bits)

    return rf_value

def set_Rf(value, half = "all"):
    """ Set feedback resistance """

    set_roc_parameter(df_params, "GlobalAnalog", half, "Cf", value)
    rf_value = compute_rf(value)

    return rf_value

def read_Rf(half = "all"):
    """ Set feedback resistance """

    ret = read_roc_parameter(df_params, "GlobalAnalog", half, "Rf")

    if half == "all":
        return [compute_rf(value) for value in ret.values()]
    else:
        return compute_rf(ret)
