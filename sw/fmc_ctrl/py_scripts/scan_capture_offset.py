#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq():

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    ev_size = 2
    strobe = 18

    #for bx_offset in range(5,15):
    for bx_offset in range(8,25):

        l1a = strobe

        c_start = l1a + bx_offset
        c_stop = c_start+ev_size+1
        #print strobe, l1a, c_start, c_stop, ev_size

        # Fast CMDs
        set_fcmds(orb = 1e4, l1a = l1a, calib = 1e4)
        set_capture_window(c_start, c_stop)

        print("Setting BX offset for capture wrt L1A to %i "% bx_offset)

        flush_fifos()

        trig_fsm(0)

        if check_fifo_full():
            ififo = 4
            fname = "/tmp/fifo%i.raw" % ififo

            try:
                read_fifo2(ififo,fname, ev_size)
            except ValueError:
                print("Error")

            nidles = -1
            with open(fname) as f:
                lines = f.read().splitlines()
                nidles = lines.count(idle_word)

            if nidles == 0:
                print lines
                print("Capture offset wrt L1A is: %i" %bx_offset)

                daq.capture_offset = bx_offset
                daq.save_config()

                return bx_offset
    return -1

def main():

    acq()

if __name__ == "__main__":

    main()
