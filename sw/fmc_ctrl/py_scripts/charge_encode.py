# -*- coding: utf-8 -*-

import sys, os, fnmatch
import pandas as pd
import numpy as np
from argparse import ArgumentParser
from common import ROC,DAQ
from hgcroc_analysis.decode_trigger import encode_tc_val



def charge(TOT,ADC,th,pl,mu):
    if TOT > th:
        TOT1=TOT - pl
    else :
        TOT1 = th - pl
    if TOT != 0 :
        return TOT1*mu
    else :
        return ADC

def make_chan_database(yaml):
    dico = {'multfact' : [0]*72, 'thresh' : [0]*72 , 'plateau' : [0]*72}
    data_resu = pd.DataFrame(dico, columns = ['multfact','thresh','plateau'],index = range(0,72))
    for ch in range(72):
        pth = yaml+"/roc_config_ChargeLin_Ch_"+str(ch)+".yaml"
        if os.path.exists(pth):
            tc = ch//9
            half = tc//4
            tc_r = tc%4
            roc = ROC(pth)
            th = roc.sc["DigitalHalf"][half]["Tot_TH"+str(tc_r)]
            pl = roc.sc["DigitalHalf"][half]["Tot_P"+str(tc_r)]
            mu= roc.sc["DigitalHalf"][half]["MultFactor"]
            data_resu['multfact'][ch] = mu
            data_resu['thresh'][ch] = th
            data_resu['plateau'][ch] = pl
    return data_resu

def make_tc_database(yaml):
    dico = {'multfact' : [0]*8, 'thresh' : [0]*8 , 'plateau' : [0]*8}
    data_resu = pd.DataFrame(dico, columns = ['multfact','thresh','plateau'],index = range(0,8))
    for tc in range(8):
        pth = yaml+"/roc_config_ChargeLin_TC_"+str(tc)+".yaml"
        if os.path.exists(pth):
            half = tc//4
            tc_r = tc%4
            roc = ROC(pth)
            th = roc.sc["DigitalHalf"][half]["Tot_TH"+str(tc_r)]
            pl = roc.sc["DigitalHalf"][half]["Tot_P"+str(tc_r)]
            mu= roc.sc["DigitalHalf"][half]["MultFactor"]
            data_resu['multfact'][tc] = mu
            data_resu['thresh'][tc] = th
            data_resu['plateau'][tc] = pl
    return data_resu
    
def main(dataframe,yaml):
    print('Getting channel information from yaml files')
    chan_par = make_chan_database(yaml)
    print('Getting TC information from yaml files')
    tc_par = make_tc_database(yaml)
    print('Importing dataframe')
    df_init = pd.read_hdf(dataframe)
    sub = df_init.bx==0
    df = df_init[sub]
    df['charge_chan']= 0
    df['charge_TC']=0
    df['charge_chan_compressed']=0
    df['charge_TC_compressed']=0
    print('Computing channel level charge')
    sel = df.ch_type == 0
    df.loc[sel,'charge_chan'] = df[sel].apply(lambda row: charge(row.tot,row.adc,chan_par["thresh"][row.channel],chan_par["plateau"][row.channel],chan_par["multfact"][row.channel]),axis = 1)
    print('Computing TC level charge ')
    df.loc[sel,'charge_TC'] = df[sel].apply(lambda row: charge(row.tot,row.adc,tc_par["thresh"][row.channel//9],tc_par["plateau"][row.channel//9],tc_par["multfact"][row.channel//9]),axis = 1)
    print('Encoding channel level charge')
    df['charge_chan_compressed'] = df.apply(lambda row: encode_tc_val(row.charge_chan), axis = 1)
    print('Encoding TC level charge')
    df['charge_TC_compressed'] = df.apply(lambda row: encode_tc_val(row.charge_TC), axis = 1)
    output = dataframe[:-3] + '_with_charge.h5'
    print("Saving results under " + output)
    df.to_hdf(output, key = "data", complevel = 9)
    
if __name__ == "__main__":

    parser = ArgumentParser()
    # parser arguments
    parser.add_argument("input_dataframe", type=str, default="./",
                        help="Input dataframe")
    parser.add_argument("yaml_files_folder", type=str, default="./",
                        help="yaml files folder")

    args = parser.parse_args()
    main(args.input_dataframe,args.yaml_files_folder)
