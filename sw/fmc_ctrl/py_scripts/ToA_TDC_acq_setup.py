#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

##### Probe PLL
# set_roc_parameter(df_params, "Top", 0, "En_pll_ext", probe)

##### Bypass PLL
# set_ext_clock(ext) #IPbus register to set FMC_Sel_ck_ext signal
# set_roc_parameter(df_params, "Top", 0, "Sel_40M_ext", ext) #ROC register in Top sub-block

##### Master TDC sub block parameters list
# # Register 0
# GLOBAL_TA_SELECT_GAIN_TOA
# GLOBAL_TA_SELECT_GAIN_TOT

# # Register 1
# GLOBAL_MODE_NO_TOT_SUB
# GLOBAL_LATENCY_TIME
# GLOBAL_MODE_FTDC_TOA_S0
# GLOBAL_MODE_FTDC_TOA_S1
# GLOBAL_SEU_TIME_OUT

# # Register 2
# BIAS_FOLLOWER_CAL_P_D
# BIAS_FOLLOWER_CAL_P_EN
# INV_FRONT_40MHZ
# START_COUNTER
# CALIB_CHANNEL_DLL

# # Register 3
# VD_CTDC_P_D
# VD_CTDC_P_DAC_EN
# EN_MASTER_CTDC_VOUT_INIT
# EN_MASTER_CTDC_DLL

# # Register 4
# BIAS_CAL_DAC_CTDC_P_D # MSB in register 7
# CTDC_CALIB_FREQUENCY

# # Register 5
# GLOBAL_MODE_TOA_DIRECT_OUTPUT
# BIAS_I_CTDC_D
# FOLLOWER_CTDC_EN

# # Register 6
# GLOBAL_EN_BUFFER_CTDC
# VD_CTDC_N_FORCE_MAX
# VD_CTDC_N_D
# VD_CTDC_N_DAC_EN

# # Register 7
# CTRL_IN_REF_CTDC_P_D
# CTRL_IN_REF_CTDC_P_EN
# BIAS_CAL_DAC_CTDC_P_D # LSB in register 4

# # Register 8
# CTRL_IN_SIG_CTDC_P_D
# CTRL_IN_SIG_CTDC_P_EN
# GLOBAL_INIT_DAC_B_CTDC
# BIAS_CAL_DAC_CTDC_P_EN

# # Register 9
# VD_FTDC_P_D
# VD_FTDC_P_DAC_EN
# EN_MASTER_FTDC_VOUT_INIT
# EN_MASTER_FTDC_DLL

# # Register 10
# BIAS_CAL_DAC_FTDC_P_D # MSB in register 14
# FTDC_CALIB_FREQUENCY

# # Register 11
# EN_REF_BG
# BIAS_I_FTDC_D
# FOLLOWER_FTDC_EN

# # Register 12
# GLOBAL_EN_BUFFER_FTDC
# VD_FTDC_N_FORCE_MAX
# VD_FTDC_N_D
# VD_FTDC_N_DAC_EN

# # Register 13
# CTRL_IN_SIG_FTDC_P_D
# CTRL_IN_SIG_FTDC_P_EN
# GLOBAL_INIT_DAC_B_FTDC
# BIAS_CAL_DAC_FTDC_P_EN

# # Register 14
# CTRL_IN_REF_FTDC_P_D
# CTRL_IN_REF_FTDC_P_EN
# BIAS_CAL_DAC_FTDC_P_D # LSB in register 10

# # Register 15
# GLOBAL_DISABLE_TOT_LIMIT
# GLOBAL_FORCE_EN_CLK
# GLOBAL_FORCE_EN_OUTPUT_DATA
# GLOBAL_FORCE_EN_TOT

def toa_tdc_acq_setup(channel = 40, trigger_num = 1):
    """ Setup testbench config for acquisition on one ToA TDC channel from external trigger signal. """
    """ trigger_num = 1 for Trig1 / trigger_num = 2 for Trig2 """
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/toa_tdc_acq/"#run_" + timestamp

    print "Output dir:"
    print odir

    n_events = 1 # Get one event for setup to verify configuration

    # config DAQ signals
    config_daq_controller() #by default to 0

    ## disable all channels discriminators:
    # set_toa_threshold(0xff)
    set_roc_parameter(df_params, "ReferenceVoltage", 0, "Toa_vref", 0x3FF)
    set_roc_parameter(df_params, "ReferenceVoltage", 1, "Toa_vref", 0x3FF) # other half

    # set_tot_threshold(0xff)
    set_roc_parameter(df_params, "ReferenceVoltage", 0, "Tot_vref", 0x3FF)
    set_roc_parameter(df_params, "ReferenceVoltage", 1, "Tot_vref", 0x3FF) # other half

    for chan in range(72):
        #disable channels preamp
        # disable_channel(chan)
        set_roc_parameter(df_params, "ch", chan, "Channel_off", 1)

        #disable channels TDC
        # mask_toa(chan, 1)
        # mask_tot(chan, 1)
        set_roc_parameter(df_params, "ch", chan, "Mask_toa", 1)
        set_roc_parameter(df_params, "ch", chan, "Mask_tot", 1)


    ext_trig_number = 0 #Select TDC external start input : 0 for Trig1 - 1 for Trig2
    if trigger_num != 1:
        ext_trig_number = 1 # else: ext_trig_number = 0 # boolean style
        print "ToA TDC acq setup on Trig2 for channel " + str(channel)
    else:
        print "ToA TDC acq setup on Trig1 for channel " + str(channel)

    # Enable ToA TDC channel
    # mask_toa(channel, 0)
    set_roc_parameter(df_params, "ch", channel, "Mask_toa", 0)

    # Enable probe_ToA output
    # set_probe_toa(channel, 1)
    set_roc_parameter(df_params, "ch", channel, "Probe_toa", 1)

    #Select ToA TDC external start input : 0 for Trig1 - 1 for Trig2
    # sel_trigger_toa(channel, ext_trig_number)
    set_roc_parameter(df_params, "ch", channel, "Sel_trigger_toa", ext_trig_number)



    ####### ToT #######

    # # Enable ToT TDC channel
    # # mask_tot(channel, 0)
    # set_roc_parameter(df_params, "ch", channel, "Mask_tot", 0)

    # # Enable probe_ToA output
    # # set_probe_tot(channel, 1)
    # set_roc_parameter(df_params, "ch", channel, "Probe_tot", 1)

    # # Select ToT TDC external start input : 0 for Trig1 - 1 for Trig2
    # # sel_trigger_tot(channel, ext_trig_number)
    # set_roc_parameter(df_params, "ch", channel, "Sel_trigger_tot", ext_trig_number)

    phase = 14
    bx_offset = -3

    set_roc_parameter(df_params, "Top", "all","Phase", phase)
    print "Setting clock phase to %i" %phase , " with word" , ret

    strobe = 0

    #Disable trig1 and trig2 signals from FPGA / FMC
    set_toa_trig1(0,0,0)
    set_toa_trig2(0,0,0)

    #calib = strobe
    l1a = strobe + 10 + bx_offset + l1a_offset

    c_start = l1a + capture_offset
    ev_size = 40
    c_stop = c_start + ev_size + 1

    print strobe, l1a, c_start, c_stop

    # Fast CMDs
    #set_fcmds(orb = 1e4, l1a = l1a, calib = calib)
    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib()
    set_fcmd_orbit()
    set_fcmd_sync()
    set_fcmd_dump()

    #set_gpio_p(calib, calib +1)
    set_gpio_p(strobe, strobe + 1)
    set_gpio_n(l1a, l1a +1)

    set_capture_window(c_start, c_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
    #print strobe, l1a, c_start, c_stop, ev_size

    calib_dac = 500

    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all","Calib_dac", phase)

    #outdir = odir + "/calibDAC_%i" %calib_dac
    outdir = odir + "/"
    outdir = os.path.abspath(outdir) + "/"

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    ## selRawData
    select_raw_data(1)

    flush_fifos()
    bash_acq_loop(n_events, 0)

    #print "Fifo full?", check_fifo_full()
    #print("Reading data...")
    #wr_mode = "w" if event == 0 else "a"
    if check_fifo_full():
        for ififo in range(0,6):
            try:
                read_fifo(ififo,outdir + "/", ev_size * n_events)
                continue
            except ValueError:
                print("Error")

    # check fifo is empty
    #print "Fifo full?", check_fifo_full()

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        channel_num = int(sys.argv[1])
        trig_num = int(sys.argv[2])
        print("## Setup of channel %i for ToA TDC acquisition" %channel_num)
        toa_tdc_acq_setup(channel_num, trig_num)
    else:
        toa_tdc_acq_setup()

if __name__ == "__main__":

    main()
