#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 1):

    ## disable all channels:
    #print("Disabling all channels")
    #set_roc_parameter(df_params, "ch", "all", "LowRange", 0)
    '''
    set_roc_parameter(df_params, "ch", "all", "HighRange", 0)
    set_roc_parameter(df_params, "ch", "all", "Probe_toa", 0)
    set_roc_parameter(df_params, "ch", "all", "Probe_tot", 0)
    set_roc_parameter(df_params, "ch", "all", "Probe_pa", 0)
    set_roc_parameter(df_params, "ch", "all", "Probe_noinv", 0)
    '''

    chans = [9, 40]

    # enable chans
    for ch in chans:
        set_roc_parameter(df_params, "ch", ch, "LowRange", 1)
        '''
        #set_roc_parameter(df_params, "ch", ch, "HighRange", 0)
        set_roc_parameter(df_params, "ch", ch, "Probe_toa", 1)
        set_roc_parameter(df_params, "ch", ch, "Probe_tot", 0)
        #set_roc_parameter(df_params, "ch", ch, "Probe_pa", 0)
        set_roc_parameter(df_params, "ch", ch, "Probe_noinv", 1)
        '''

    l1a_offset = 10
    set_roc_parameter(df_params, "DigitalHalf", "all","L1Offset", l1a_offset)

    phase = 14
    print "Setting clock phase to %i" %phase
    set_roc_parameter(df_params, "Top", "all","Phase", phase)

    # Fast CMDs
    disable_fcmds()

    strobe = 0
    calib = strobe
    calib_stop = calib + 5

    orbit = calib + calib_offset
    set_fcmd_orbit(orbit, orbit + 1)

    l1a = calib + calib_offset + l1a_offset

    if calib_stop >= l1a: calib_stop = l1a - 1

    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib(calib, calib_stop)
    set_gpio_p(calib, calib_stop)

    # Data capture window
    c_start = l1a + capture_offset
    ev_size = 44
    c_stop = c_start + ev_size + 1
    set_capture_window(c_start, c_stop)

    ## Trigger capture window
    c_trig_start = calib + calib_offset + capture_offset - 5
    c_trig_stop = c_trig_start + 10
    for ififo in range(4):
        set_fifo_capture_window(ififo, c_trig_start, c_trig_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))

    calib_dac = 1000
    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", calib_dac)

    # Prep output folder
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/injection/run_" #+ timestamp

    outdir = odir + "/"
    outdir = os.path.abspath(outdir) + "/"

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    print "Output dir:"
    print outdir

    flush_fifos()
    bash_acq_loop(n_events, 0)
    read_fifos(range(6),outdir, data_size)

    for ch in chans:
        #disable_ch_inj(ch)
        set_roc_parameter(df_params, "ch", ch, "LowRange", 0)
        '''
        set_roc_parameter(df_params, "ch", ch, "HighRange", 0)
        set_roc_parameter(df_params, "ch", ch, "Probe_toa", 0)
        set_roc_parameter(df_params, "ch", ch, "Probe_tot", 0)
        set_roc_parameter(df_params, "ch", ch, "Probe_pa", 0)
        '''

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
