# -*- coding: utf-8 -*-
import sys, os, subprocess, datetime, fnmatch, glob
import fpga_control as fpga
import numpy as np
import pandas as pd
import hgcroc_analysis.process_raw as praw
from common import ROC, DAQ
from simple_daq import simple_acq

def main():
    #Create DAQ and ROC
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    roc = ROC(roc_conf_fname)
    outdir = simple_acq(daq,roc,"ped_sub",100,"")
    print('acquisition done, treating data')
    praw.main(outdir)
    out1b = outdir +"/dataframe.h5"

    df = pd.read_hdf(out1b)
    df = df.groupby('channel')['adc'].mean()
    print("ADC pedestal values are : " )
    print(df.values)
    a = df.values
    dicto = [("cm",1) , ("cm",0) , ("calib",0)] + [("ch", i ) for i in range(72)] + [("calib",1) , ("cm",2) , ("cm",3)]
    
    for i in range(78):
        roc.set_parameter(dicto[i][0], dicto[i][1],'Adc_pedestal',int(round(a[i])))
        
    print('ADC pedestal set per channel')
    return

if __name__ == "__main__":
        main()
