#!/usr/bin/python
import yaml

from hgcroc_i2c_wrapper import *

### generic Config class
class Config:

    def __init__(self, conf_fname = None):
        if conf_fname is not None:
            self.conf_fname = conf_fname
            self.load_config(conf_fname)
        return

    def load_config(self, conf_fname):
        self.conf_fname = conf_fname

        # read config file
        with open(conf_fname) as f:
            params = yaml.load(f, Loader=yaml.FullLoader)

            #self.param_names += params.keys()

            for param, value in params.iteritems():
                setattr(self, param, value)

            print("Loaded parameters from %s"%conf_fname )
            #print self #params.keys()

        return

    def get_params(self):
        params = {}
        #for param in self.param_names:
        for param in self.__dict__.keys():
            params[param] = getattr(self, param)

        return params

    def __repr__(self):
        params = self.get_params()
        par_str = "Key:\tvalue\n"
        par_str += "\n".join([(str(key) + ":\t" + str(val)) for key, val in params.iteritems()])

        return par_str #params.__str__

    def print_param_names(self):
        print("Parameters:")
        #print(self.param_names)
        print(self.__dict__.keys())

        return

    def save_config(self, conf_fname = None):
        if conf_fname is None:
            if hasattr(self, "conf_fname"):
                conf_fname = self.conf_fname
            else:
                self.conf_fname = conf_fname
        print("Writing parameters to %s" %conf_fname)

        params = self.get_params()

        # read config file
        with open(conf_fname, "w") as f:
            yaml.dump(params, f, sort_keys=True)

        return conf_fname
## End of Config class

## DAQ class
class DAQ(Config):
    def __init__(self, conf_fname):
        print("Creating DAQ object")
        Config.__init__(self, conf_fname)

## End of DAQ class

## ROC class
class ROC(Config):
    def __init__(self, conf_fname = None):
        print("Creating ROC object")
        Config.__init__(self, conf_fname)

        if hasattr(self, "bitmap_fname"):
            self.load_bitmap()

    def load_bitmap(self, bitmap_fname = None):
        if bitmap_fname is None: bitmap_fname = self.bitmap_fname

        self.bitmap = load_param_d_map(bitmap_fname)

    # overwrite get_params to exclude bitmap
    def get_params(self):
        params = {}
        #for param in self.param_names:
        for param in self.__dict__.keys():
            if param == "bitmap": continue
            params[param] = getattr(self, param)

        return params

    def add_sc_param(self, block, blockId, param, value):

        if not hasattr(self, "sc"):
            print("ROC has no SC param list defined. Creating empty sc object.")
            self.sc = {}

        if block not in self.sc: self.sc[block] = {}
        if blockId not in self.sc[block]: self.sc[block][blockId] = {}

        self.sc[block][blockId][param] = value

        return 1

    def update_sc_from_config(self, conf_fname):
        # read config file
        with open(conf_fname) as f:
            params = yaml.load(f, Loader=yaml.FullLoader)

            if "sc" in params:
                self.sc.update(params["sc"])

            print("Update sc from %s"%conf_fname )
        return

    def set_parameter(self, block, blockId, param, value = 0):
        #self.add_sc_param(block, blockId, param, value)
        return set_roc_parameter(self.bitmap, block, blockId, param, value)

    def read_parameter(self, block, blockId, param):
        return read_roc_parameter(self.bitmap, block, blockId, param)

    def config_sc_params(self):
        for block in self.sc:
            for blockId in self.sc[block]:
                for param, value in self.sc[block][blockId].iteritems():
                    #print "Read", self.read_parameter(block, blockId, param)
                    print("Setting parameter %s %s %s %i" % ( block, str(blockId), param, value) )
                    self.set_parameter(block, blockId, param, value)
                    #print "Read", self.read_parameter(block, blockId, param)

    def read_sc_params(self):
        for block in self.sc:
            for blockId in self.sc[block]:
                for param, value in self.sc[block][blockId].iteritems():
                    ret = self.read_parameter(block, blockId, param)
                    print("Reading parameter %s %s %s %s" % ( block, str(blockId), param, str(ret)) )
                    print("Expecting: %i" %value)

## End of ROC class

if __name__ == "__main__":

    '''
    daq = DAQ("configs/daq_params.yaml")

    print daq.get_params()
    print daq.l1a_offset
    print daq.__dict__.keys()

    daq.new_param = 1234

    daq.link_delays = [0,1,2,3,4,5,6]
    daq.link_bitpos = [0,0,0,0,0,0]

    print daq
    daq.save_config("configs/test.yaml")
    '''

    #roc = ROC("configs/roc_config.yaml")
    #print(roc)

    '''
    print hex(roc.read_parameter("DigitalHalf",0,"IdleFrame"))
    print roc.set_parameter("DigitalHalf",0,"IdleFrame",0xccccccc)
    print hex(roc.read_parameter("DigitalHalf",0,"IdleFrame"))
    '''
    #roc.config_sc_params()

    '''
    print roc.set_parameter("GlobalAnalog","all","SelExtADC",1)

    for ch in range(72):
        print roc.read_parameter("ch",ch,"ExtData")
        roc.set_parameter("ch",ch,"ExtData",ch)
        print roc.read_parameter("ch",ch,"ExtData")
    '''

    '''

    print roc.set_parameter("ch","1-35","Sel_trigger_toa", 0)
    print roc.set_parameter("ch","1-35","Sel_trigger_tot", 0)
    print roc.set_parameter("ch","36-71","Sel_trigger_toa", 1)
    print roc.set_parameter("ch","36-71","Sel_trigger_tot", 1)
    '''

    roc = ROC()
    print roc.add_sc_param("ch","1-35","Sel_trigger_toa", 0)
    print roc
    roc.save_config("configs/roc_test.yaml")

    roc.update_sc_from_config("configs/roc_config.yaml")
    print roc
