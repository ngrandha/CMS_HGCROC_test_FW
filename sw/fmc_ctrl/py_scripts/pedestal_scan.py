# -*- coding: utf-8 -*-
#!/usr/bin/python
import sys, os, subprocess, datetime
from common import ROC,DAQ
import fpga_control as fpga
from simple_daq import configure_acq, make_outdir
# define event size and number of consecutive triggers (L1A)
ev_size = 43


def acq(daq,roc,run_type,n_events = 10, suffix = "",step=1):

    # initialize DAQ and ROC objects
    
    outdir = make_outdir(run_type,suffix)

    ## configure acquisition
    daq = configure_acq(daq, n_events, n_bxs = 1)

    ## save DAQ config to outdir
    daq.save_config(outdir + "daq_config.yaml")

    # values of 5bit-pedDAC to scan
    ped_vals = range(0,32,step)

    roc.set_parameter("ReferenceVoltage","all","Inv_vref",260)
    roc.set_parameter("ReferenceVoltage","all","Noinv_vref",100)

    for ped_val in ped_vals:
        print("Setting pedestal DAC 5b value to %i" %ped_val)

        # Ref_dac_inv is the ped DAC setting
        roc.set_parameter("ch", "all","Ref_dac_inv", ped_val) # normal channels
        roc.set_parameter("cm", "all","Ref_dac_inv", ped_val) # common-mode channels
        roc.set_parameter("calib", "all","Ref_dac_inv", ped_val) # calib channels

        odir = outdir + "/pedDAC_%i" %(ped_val)
        odir = os.path.abspath(odir) + "/"

        if not os.path.exists(odir):
            os.makedirs(odir)

        # save ROC config to odir
        roc.save_config(odir + "roc_config.yaml")

        ## Acquisition
        fpga.flush_fifos()
        fpga.bash_acq_loop(daq.n_events, 0)
        fpga.read_fifos([4,5],odir, daq.data_size)

    print("Resetting pedestal values")
    ped_val = 0
    roc.set_parameter("ch", "all","Ref_dac_inv", ped_val) # normal channels
    roc.set_parameter("ch", "cm","Ref_dac_inv", ped_val) # common-mode channels
    roc.set_parameter("ch", "calib","Ref_dac_inv", ped_val) # calib channels

    return outdir

def main():
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    roc = ROC(roc_conf_fname)

    n_events = 10
    suffix = ""

    print(sys.argv)

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suffix = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suffix)


    acq(daq,roc,"ped_scan",n_events, suffix,1)

if __name__ == "__main__":

    main()
