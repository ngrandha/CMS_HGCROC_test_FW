#!/usr/bin/python
import sys, os, subprocess, datetime
import fpga_control as fpga
from config import ROC, DAQ
from common_acq import *

def acq(n_events = 50):

    # initialize DAQ and ROC objects
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs_injection/roc_config_ChargeLin_TC_0.yaml"
    roc = ROC(roc_conf_fname)

    ## configure acquisition
    daq = configure_calib_acq(daq, n_events, n_bxs = 5)

    phase = 14
    print "Setting clock phase to %i" %phase
    roc.set_parameter("Top", 0,"Phase", phase)

    ## Output folder
    odir = make_outdir("injection")

    ## save DAQ config to outdir
    daq.save_config(odir + "daq_config.yaml")

    ## Calib DAC values to scan
    calib_dacs_1 = [-1] + range(0,800,50)
    calib_dacs_2 = range(700,2000,50)

    ## disable all channels:
    print("Disabling all channels")
    roc.set_parameter("ch", "all", "LowRange", 0)
    ## Channel loop
    for chan in range(0,8,4):
        chans = [chan, chan+36]
        print("Enabling channels:")
        print(chans)

        # Enable channels for injection
        for ch in chans:
            roc.set_parameter("ch", ch, "LowRange", 1)
            #roc.set_parameter("ch", ch, "HighRange", 0)
        for k in [0,1]:
            roc.set_parameter('half',k,"Phase",12)
        for calib_dac in calib_dacs_1:
            print("Setting calib DAC to %i" % calib_dac)
            if calib_dac >= 0:
                roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", calib_dac)
                roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 1)
            else:
                roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", 0)
                roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 0)

            outdir = odir + "phase_12/chan_"+str(chan)+"/calibDAC_"+str(calib_dac)+"/"
            outdir = os.path.abspath(outdir) + "/"
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            ## save ROC config to outdir
            roc.save_config(outdir + "roc_config.yaml")

            fpga.flush_fifos()
            fpga.bash_acq_loop(daq.n_events, 0)

            # read data
            fpga.read_fifos([4,5],outdir + "/", daq.data_size)
            # read triger
            fpga.read_fifos(range(4),outdir + "/", daq.tp_data_size)
        
        for k in [0,1]:
            roc.set_parameter('half',k,"Phase",14)
        for calib_dac in calib_dacs_2:
            print("Setting calib DAC to %i" % calib_dac)
            if calib_dac >= 0:
                roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", calib_dac)
                roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 1)
            else:
                roc.set_parameter("ReferenceVoltage", "all", "Calib_dac", 0)
                roc.set_parameter("ReferenceVoltage", "all", "IntCtest", 0)

            outdir = odir + "phase_14/chan_"+str(chan)+"/calibDAC_"+str(calib_dac)+"/"
            outdir = os.path.abspath(outdir) + "/"
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            ## save ROC config to outdir
            roc.save_config(outdir + "roc_config.yaml")

            fpga.flush_fifos()
            fpga.bash_acq_loop(daq.n_events, 0)

            # read data
            fpga.read_fifos([4,5],outdir + "/", daq.data_size)
            # read triger
            fpga.read_fifos(range(4),outdir + "/", daq.tp_data_size)

        ## end calib loop

        # disable channels
        for ch in chans:
            roc.set_parameter("ch", ch, "LowRange", 0)
            #roc.set_parameter("ch", ch, "HighRange", 0)

    ## end chan loop
    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
        acq(n_events)
    else:
        acq()

if __name__ == "__main__":

    main()
