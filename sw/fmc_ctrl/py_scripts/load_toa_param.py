#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *


def injection(n_events = 10,start = 0, stop = 300, step = 10, odir="./data/toa_dac_adj/run_", n_loop = 0):
    
    odir = odir + "/n_loop%i" %n_loop 
    # Fast CMDs
    disable_fcmds()

    strobe = 0
    calib = strobe
    calib_stop = calib + 20
    l1a = calib + calib_offset + l1a_offset + 1

    if calib_stop >= l1a: calib_stop = l1a - 1

    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib(calib, calib_stop)

    # Data capture window
    c_start = l1a + capture_offset
    ev_size = 44
    c_stop = c_start + ev_size + 1
    set_capture_window(c_start, c_stop)

    ## Trigger capture window
    c_trig_start = calib + calib_offset + capture_offset - 5
    c_trig_stop = c_trig_start + 10
    for ififo in range(4):
        set_fifo_capture_window(ififo, c_trig_start, c_trig_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))

    calib_dacs = range(start,stop,step)

    print(80*"#")

    for calib_dac in calib_dacs:
        print "Setting calib DAC to", calib_dac
        set_roc_parameter(df_params,"ReferenceVoltage", "all", "Calib_dac", calib_dac)
        outdir = odir + "/calibDAC_%i" %calib_dac
        outdir = os.path.abspath(outdir) + "/"

        if not os.path.exists(outdir):
            os.makedirs(outdir)

        flush_fifos()
        bash_acq_loop(n_events, 0)
        read_fifos([4,5],outdir + "/", data_size)
    print(80*"#")
    set_roc_parameter(df_params,"ReferenceVoltage", "all", "Calib_dac", 0)

    return odir

def process_data(indir):
    
    fnames = glob.glob(indir + "/*/fifo4.raw")
    dfs = []

    for i, fname in enumerate(sorted(fnames)):
        pos = fname.find("calibDAC_")
        part = fname[pos+9:]
        pos = part.find("/f")
        calib_dac = part[:pos]

        fname1 = fname
        fname2 = fname.replace("fifo4","fifo5")

        df_chans = create_full_df(fname1, fname2)
        if df_chans is None: continue

        df_chans["cal_dac"] = int(calib_dac)
        dfs.append(df_chans)

    # merger dfs
    df_chans = pd.concat(dfs)
    df_chans.sort_values("cal_dac", inplace=True)

    #hdfname = indir + "/inj_scan_DAC.h5"
    #df_chans.to_hdf(hdfname, key = "data")

    print(20*"#" + " Processing the data " + 20*"#")
    '''
    df_chans["toa_sc"] = 0
    for chan in sorted(df_chans.channel.unique()):
        for calib_dac in sorted(df_chans.cal_dac.unique()):
            sel = df_chans.channel == chan
            sel &= df_chans.cal_dac == calib_dac
            df_sel = df_chans[sel]
            #for toa_v in range(len(df_sel.toa)):
            ret1 = []
            te = df_sel.groupby("toa").size().index[0]
            if te == 0:
                if df_sel.groupby("toa").size().values[0] == len(df_sel.toa):
                    ret1.append(0)
                else:
                    val = 1 - (float(df_sel.groupby("toa").size().values[0]) / len(df_sel.toa))
                    ret1.append(val)
            else:
                ret1.append(1)
            df_chans.loc[sel,"toa_sc"] = np.mean(ret1)
    '''

    return df_chans


def plot_sc(df_chans, indir, n_loop):
    f, axs = plt.subplots(1,2,figsize = (15,8), sharey = True)

    sel = df_chans.ch_type == "normal"
    df_sel = df_chans[sel]
    chans = sorted(df_sel.channel.unique())

    for chan in chans:#hit_chans:
        sel = df_chans.channel == chan
        df_sel = df_chans[sel]
        ax = axs[0] if chan < 36 else axs[1]
        prof = df_sel.groupby("cal_dac")["toa"].mean()
        
        #y = 0 if prof.values == 0 else 1
        x = prof.index
        y = prof.apply(lambda x: 0 if x == 0 else 1).values
        ax.plot(x,y , ".-", label = "Channel %i" %chan)

    for ax in axs: 
        ax.grid()
        ax.legend(loc = "lower right", ncol = 2, fontsize =8)
        ax.set_xlabel("Calib DAC")
        ax.set_ylabel("TDC")
    print("Saving plot toa_s_curves_loop%i" %n_loop + ".png")
    plt.savefig(indir + "_toa_s_curves_loop%i.png" %n_loop)

def main():

    if len(sys.argv) == 2:
        indir = str(sys.argv[1])
        fname = indir + "toa.txt"
        f=open(fname, "r")
        oparam = f.read()
        f.close()
        pos = oparam.find("[")
        part = oparam[pos+1:]
        pos = part.find("]")
        param = part[:pos]
        iparam = param.split(",")
        print iparam
        for i in range(len(iparam)):
            print("Setting toa_dac channel%i to %s" %(i,iparam[i]))
            print iparam[i]
            val = int(iparam[i])
            set_roc_parameter(df_params, "ch", i, "Ref_dac_toa", val)

        
    else:
        print("Need directory as argument")


        
if __name__ == "__main__":

    main()
