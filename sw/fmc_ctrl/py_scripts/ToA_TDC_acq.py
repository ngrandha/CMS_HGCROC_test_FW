#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def toa_tdc_acq(channel = 40, n_events = 1):
    """ Acquire a given number of events from the specified ToA TDC channel. """
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/toa_tdc_acq/"#run_" + timestamp

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by default to 0
    
    
    
    phase = 14
    bx_offset = -3

    set_roc_parameter(df_params, "Top", "all","Phase", phase)
    print "Setting clock phase to %i" %phase , " with word" , ret

    strobe = 0

    #calib = strobe
    l1a = strobe + 10 + bx_offset + l1a_offset

    c_start = l1a + capture_offset # + channel offset
    ev_size = 1
    c_stop = c_start + ev_size + 1 # not + 1 ?

    print strobe, l1a, c_start, c_stop

    # Fast CMDs
    #set_fcmds(orb = 1e4, l1a = l1a, calib = calib)
    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib()
    set_fcmd_orbit()
    set_fcmd_sync()
    set_fcmd_dump()

    #set_gpio_p(calib, calib +1)
    set_gpio_p(strobe, strobe + 1)
    set_gpio_n(l1a, l1a +1)

    set_capture_window(c_start, c_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))
    #print strobe, l1a, c_start, c_stop, ev_size

    calib_dac = 500

    print "Setting calib_dac", calib_dac
    set_roc_parameter(df_params, "ReferenceVoltage", "all","Calib_dac", phase)

    #outdir = odir + "/calibDAC_%i" %calib_dac
    outdir = odir + "/"
    outdir = os.path.abspath(outdir) + "/"

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    ## selRawData
    select_raw_data(1)

    flush_fifos()
    bash_acq_loop(n_events, 0)

    #print "Fifo full?", check_fifo_full()
    #print("Reading data...")
    #wr_mode = "w" if event == 0 else "a"
    if check_fifo_full():
        for ififo in range(0,6):
            try:
                read_fifo(ififo,outdir + "/", ev_size * n_events)
                continue
            except ValueError:
                print("Error")

    # check fifo is empty
    #print "Fifo full?", check_fifo_full()

    return

def main():

    if len(sys.argv) > 1:
        print sys.argv
        channel_num = int(sys.argv[1])
        n_events = int(sys.argv[2])
        print("## %i events from ToA TDC channel %i requested" %n_events, %channel_num)
        toa_tdc_acq(channel_num, n_events)
    else:
        toa_tdc_acq()
        
if __name__ == "__main__":

    main()
