# -*- coding: utf-8 -*-

import sys
import numpy as np
import pandas as pd
import hgcroc_analysis.process_raw as praw
from common import ROC, DAQ
from simple_daq import simple_acq

def main(n_sigma =5):
    #Create DAQ and ROC
    daq = DAQ("configs/daq_params.yaml")
    roc_conf_fname = "configs/roc_config.yaml"
    roc = ROC(roc_conf_fname)
    outdir = simple_acq(daq,roc,"noise_threshold",100,"")
    print('acquisition done, treating data')
    praw.main(outdir)
    out1b = outdir +"/dataframe.h5"
    df = pd.read_hdf(out1b)
    for half in [0,1] : 
        if half == 0:
            sub = df.channel < 36
        else :
            sub = df.channel > 35
        df_sub = df[sub]
        rms = df_sub.groupby('channel').std()['adc'].median()
        std = df_sub.groupby('channel').std()['adc'].std()
        print("Average ADC sigma for half "+str(half)+" is " + str(rms))
        print("Standard deviation of ADC sigma for half "+str(half)+ " is " +str(std))
        roc.set_parameter("DigitalHalf",half,"Adc_TH", int(round(n_sigma*rms)))
        print("ADC Threshold parameter set to " +str(int(round(n_sigma*rms))))
    return

if __name__ == "__main__":

    if len(sys.argv) > 1:
        n_sigma = int(sys.argv[1])
        main(n_sigma)
    else:
        main()

    
    
