#!/usr/bin/python
import sys
import init, scan_delays, scan_bitpos, scan_capture_offset

def main(roc_conf_fname = "configs/roc_config.yaml"):

    init.main(roc_conf_fname)
    scan_delays.scan_delays(32)
    scan_bitpos.acq()
    scan_capture_offset.acq()

    return

if __name__ == "__main__":

    if len(sys.argv) > 1:
        fname = sys.argv[1]
        print("Using ROC config file: %s" %fname)
        main(fname)
    else:
        main()
