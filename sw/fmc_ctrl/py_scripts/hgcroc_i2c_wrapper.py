import sys, pickle

import numpy as np
import pandas as pd

from fmc_wrapper import exec_cmd

### I2C

## single register operations
def read_I2C_register(reg_id = 0, chip_id = 5):

    i2c_addr = hex((chip_id << 3) + reg_id)

    cmd = " -J " + i2c_addr
    ret = exec_cmd(cmd)

    if "I2C error" in ret:
        print "## Error in reading!"
        print ret
        print "## Trying again:"
        ret = read_I2C_register(reg_id, chip_id)
        print ret

        return ret

    ret = ret.split("\n")[-2]
    ret = int(ret, 16)

    return ret

def set_I2C_register(reg_id = 0, value = 0, chip_id = 5):

    i2c_addr = hex((chip_id << 3) + reg_id)

    cmd = " -J " + i2c_addr + "=" + str(value)
    ret = exec_cmd(cmd)

    if "I2C error" in ret:
        print "## Error in writing!"
        print ret
        print "## Trying again:"
        ret = set_I2C_register(reg_id, value, chip_id)
        print ret

    return ret

## multi-register operations
def read_I2C_parameter(val_r0, val_r1, chip_id = 5):
    set_I2C_register(0, val_r0, chip_id)
    set_I2C_register(1, val_r1, chip_id)
    ret = read_I2C_register(2, chip_id)
    return ret #int(ret, 16)

def set_I2C_parameter(val_r0, val_r1, val_r2, chip_id = 5):

    set_I2C_register(0, val_r0, chip_id)
    set_I2C_register(1, val_r1, chip_id)
    ret = set_I2C_register(2, val_r2, chip_id)

    return ret

## masked setting of register
def set_I2C_parameter_masked(val_r0, val_r1, value, mask, chip_id = 5):

    if mask == 0xff:
        val_r2 = value
    else:
        # read current value
        cur_val = read_I2C_parameter(val_r0, val_r1)
        inv_mask = 0xff - mask

        cur_val = cur_val & inv_mask # bits that should not change

        # provided param is in correct place
        val_r2 = cur_val + value

    ret = set_I2C_parameter(val_r0, val_r1, val_r2, chip_id)

    return ret

def set_dummy_I2C(r0,r1,r2):
    #print("Setting dummy: r0/r1/r2:")
    #print(r0,r1,r2)
    return r0,r1,r2

def read_dummy_I2C(r0,r1, val = "0xff"):
    return int(val, 16)

def get_I2C_reg_val(sub_addr, reg):
    address = (sub_addr << 5) + reg
    val_R0 = address & 0xff
    val_R1 = (address >> 8) & 0xff

    return hex(val_R0), hex(val_R1)
    #return val_R0, val_R1

def get_lsb_id(x):
    return int(np.log(x & -x)/np.log(2))

#### Get I2C block, blockid and parameter list
def get_block_list(d_map):
    return sorted(d_map.keys())

def get_blockid_counts(d_map):
    return {blockid:len(d_map[blockid]) for blockid in d_map}

def get_blockid_list(d_map, block):
    return sorted(d_map[block].keys())

def get_param_list(d_map, block):
    return sorted(d_map[block][0].keys())

### Using blockided d_map
def get_param_nbits(d_map, block, param):
    #return len(d_map.loc[(block, 0, param)])
    return sum([bin(reg["param_mask"]).count("1") for reg in d_map[block][0][param].values()])

def get_param_max(d_map, block, param):
    return 2**get_param_nbits(d_map, block, param)

def get_param_default(d_map, block, blockid, param):
    """ Compute parameter value from d_map """

    defval = 0
    for k,reg in d_map[block][blockid][param].iteritems():
        defval += reg["defval_mask"] << reg["param_minbit"]
    return defval

def set_roc_parameter(d_map, block, blockid, name, value = 0):
    """ Set ROC parameter """

    # expand blockIds if not a single value given
    if not isinstance(blockid, int):
        if blockid == "all":
            # Set param for all blockIDs matching subblock name
            b_ids = get_blockid_list(d_map, block)
        elif "," in blockid:
            # Split into list
            b_ids = [int(bid) for bid in blockid.split(",")]
        elif "-" in blockid:
            # Create range of ids
            limits = [int(bid) for bid in blockid.split("-")]
            b_ids = range(limits[0], limits[1] + 1)
        else:
            return "Unknown blockId", blockid

        for b_id in b_ids:
            #print("Setting blockID %i" %b_id)
            set_roc_parameter(d_map, block, b_id, name, value)
        return

    par_regs = d_map[block][blockid][name]

    param_parts = {}

    for reg_id,reg in par_regs.iteritems():

        # apply mask to parameter
        reg_data = value & reg["param_mask"]
        reg_data >>= get_lsb_id(reg["param_mask"])
        reg_data <<= get_lsb_id(reg["reg_mask"])

        param_parts[reg_id] = hex(reg_data)

        # dummy set
        #set_dummy_I2C(reg['R0'],reg['R1'], reg_data)
        # real I2C setting
        set_I2C_parameter_masked(reg['R0'],reg['R1'], reg_data, reg["reg_mask"])

    return param_parts

def reset_roc_parameter(d_map, block, blockid, name):
    """ Reset ROC parameter to default """

    # Set param for all blockIDs matching subblock name
    if blockid == "all":
        b_ids = get_blockid_list(d_map, block)
        for b_id in b_ids:
            #print("Setting blockID %i" %b_id)
            set_param_default(d_map, block, b_id, name, value)
        return

    value = get_param_default(d_map, block, blockid, name)
    return set_roc_parameter(d_map, block, blockid, name, value)

def read_roc_parameter(d_map, block, blockid, name):
    """ Read ROC parameter """

    # read param for all blockIDs matching subblock name
    if blockid == "all":
        b_ids = get_blockid_list(d_map, block)
        rets = {}
        for b_id in b_ids:
            #print("Reading blockID %i" %b_id)
            rets[b_id] = read_roc_parameter(d_map, block, b_id, name)
        return rets

    # read all params matching subblock name for given blockID
    if name == "all":
        params = get_param_list(d_map, block)
        rets = {}

        for param in params:
            rets[param] = read_roc_parameter(d_map, block, blockid, param)
        return rets

    par_regs = d_map[block][blockid][name]

    ret_val = 0

    for reg_id,reg in par_regs.iteritems():
        # dummy read
        #ret = read_dummy_I2C(reg['R0'],reg['R1'], hex(reg["defval_mask"])) # test with default value
        #ret = read_dummy_I2C(reg['R0'],reg['R1'], "0xff") # test with maximum value
        # real read register
        ret = read_I2C_parameter(reg['R0'],reg['R1'])

        # apply register mask
        ret &= reg["reg_mask"]
        # shift read value by LSB position in register mask
        ret >>= get_lsb_id(reg["reg_mask"])
        # shift by LSB position in param mask
        ret <<= get_lsb_id(reg["param_mask"]) # equivalent of param_minbit value

        ret_val += ret

    return ret_val

def load_param_d_map(fname = "configs/HGCROCv2_I2C_params_regmap_dict.pickle"):

    with open(fname, 'rb') as handle:
        d_map = pickle.load(handle)
    return d_map

def main():

    d_map = load_param_d_map()

    '''
    print get_block_list(d_map)
    print get_blockid_counts(d_map)
    print get_blockid_list(d_map, "ch")
    print get_param_list(d_map, "ReferenceVoltage")
    print get_param_nbits(d_map,"ch","Adc_pedestal")
    print get_param_nbits(d_map,"ReferenceVoltage","Calib_dac")
    print get_param_nbits(d_map,"DigitalHalf","IdleFrame")

    print get_param_default(d_map, )
    '''
    #print get_param_list(d_map, "ReferenceVoltage")

    print read_roc_parameter(d_map,"ReferenceVoltage",0,"Inv_vref")
    print read_roc_parameter(d_map,"ReferenceVoltage",0,"Noinv_vref")

    print read_roc_parameter(d_map, "GlobalAnalog",0,"Cf")
    print read_roc_parameter(d_map, "GlobalAnalog",0,"Rf")

    #print get_param_nbits(d_map,"ReferenceVoltage","ExtCtest")
    print read_roc_parameter(d_map, "ReferenceVoltage", 0, "ExtCtest")
    print hex(read_roc_parameter(d_map, "DigitalHalf",0,"IdleFrame"))

    #print set_roc_parameter(d_map,  "DigitalHalf",0,"IdleFrame", 0xfabcdef)

if __name__ == "__main__":

    main()
