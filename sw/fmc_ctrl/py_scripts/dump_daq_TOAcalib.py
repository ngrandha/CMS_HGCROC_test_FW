#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 1, suffix = ""):

    ## Choose channels to enable
    chans = [0,6,12,18,24,30]
    n_columns = 80

    ev_size = 44
    #data_size = (ev_size - 1) * n_columns * n_events
    data_size = (ev_size - 1) * n_columns

    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_columns = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_columns))
        data_size = (ev_size - 1) * n_columns

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/ram_dump_toacalib/run_" + timestamp + suffix
    odir = os.path.abspath(odir) + "/"

    if not os.path.exists(odir):
        os.makedirs(odir)

    print "Output dir:"
    print odir

    # config DAQ signals
    config_daq_controller() #by defaul to 0

    # Fast CMDs
    set_fcmd_sync()
    set_fcmd_calib()
    set_fcmd_l1a() # disable L1A to use Dump

    # set L1 offset
    l1a_offset = 511
    set_roc_parameter(df_params, "DigitalHalf", 0, "L1Offset", l1a_offset)
    set_roc_parameter(df_params, "DigitalHalf", 1, "L1Offset", l1a_offset)

    strobe = 0
    orbit = strobe
    set_fcmd_orbit(orbit, orbit + 1)
    #set_fcmd_orbit()

    dump_start = orbit + 511 #4095
    dump_stop = dump_start + 1

    if n_columns > 1:
        dump_stop += capture_offset
        dump_stop += (ev_size - 1) * (n_columns - 1)
    #print dump_start, dump_stop

    set_fcmd_dump(dump_start, dump_stop)

    c_start = dump_start + capture_offset
    c_stop = c_start + (ev_size - 1) * n_columns + 1
    set_capture_window(c_start, c_stop)

    #set_gpio_p(dump_start, dump_stop)
    #set_gpio_n(c_start, c_stop)

    print dump_start, dump_stop, c_start, c_stop
    #print strobe, dump, c_start, c_stop, ev_size

    #TOA threshold values to get hits from the noise
    toa_ths = [72,74,71,69,74,72,71,73,71,72,72,71,72,71,71,71,70,72,71,71,71,72,74,73,72,71,69,70,71,71,72,70,71,70,71,71]

    #for toa_th in toa_ths:
    #print("Setting toa_th to %i" %toa_th)	
    #set_roc_parameter(df_params, "ReferenceVoltage", "all", "Toa_vref", toa_th)
    #for toa_ped in toa_peds:
    
    #Turn off TOA for all channels
    for chann in range(72):
	set_roc_parameter(df_params, "ch", chann, "Mask_toa", 1) 
    
    for chan in chans:
	set_roc_parameter(df_params, "ch", chan, "Mask_toa", 0)
	print ("Enabling toa for channel %i" %chan)

    for ch in chans:
        for chan in chans:        
	    if chan <> ch: 
		set_roc_parameter(df_params, "ch", chan, "Mask_toa", 1)	
	    else:
		set_roc_parameter(df_params, "ch", chan, "Mask_toa", 0)
		print ("Enabling toa for channel %i" %chan)
	
        print("Setting toa_th to %i for channel %i" %(toa_ths[ch],ch))
        set_roc_parameter(df_params, "ReferenceVoltage", "all", "Toa_vref", toa_ths[ch])
	    
	for iev in range(n_events):

            flush_fifos()
            wr_mode = "w" if iev == 0 else "a"

            # check fifo is empty
            print "Fifo full?", check_fifo_full()
            print ("Taking data...")
    
            ## Directory 
	    if len(chans) > 1:
	        outdir = odir + "/chan_%i/" %ch
	    else:
	        outdir = odir
            outdir = os.path.abspath(outdir) + "/"
            ## Create folder if it doesn't exist
            if not os.path.exists(outdir):
                os.makedirs(outdir)
 
            # take one event
            #trig_fsm(trig = 0)
            #bash_acq_loop(n_events, 0)
            bash_acq_loop(1, 0)

            print "Fifo full?", check_fifo_full()
            print("Reading data...")

            read_fifos([4,5],outdir + "/", data_size, wr_mode)

            # check fifo is empty
            print "Fifo full?", check_fifo_full()
            flush_fifos()

    return

def main():

    n_events = 1
    suffix = ""
    
    print sys.argv

    if len(sys.argv) > 1:
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suffix = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suffix)


    acq(n_events, suffix)

if __name__ == "__main__":

    main()
