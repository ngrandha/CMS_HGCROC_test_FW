2019/09/19

Firmware Version is 0x00010008

Firmware upgrade.
Create a 'capture' signal for each FIFO.
Add an automatic trigger for the DAQ controller.

Change the 'hgroc_test_address_table.xml' file:
	- create 'capture' registers for each FIFO:
		* fifo0_capture_start/fifo0_capture_stop
		* fifo1_capture_start/fifo1_capture_stop
		* fifo2_capture_start/fifo2_capture_stop
		* fifo3_capture_start/fifo3_capture_stop
		* fifo4_capture_start/fifo4_capture_stop
		* fifo5_capture_start/fifo5_capture_stop
	- add a new value for the 'hgroc_daq.daq_start_source' register :
		./fmc_hgroc  -W hgroc_daq.synchro = 16
		* 16 : automatic trigger for the DAQ controller.