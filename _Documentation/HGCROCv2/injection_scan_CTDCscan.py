#!/usr/bin/python
import sys, os, subprocess, datetime
from common import *

def acq(n_events = 30, suffix = ""):

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    odir = "./data/injection_test/run_" + timestamp + suffix 

    print "Output dir:"
    print odir

    
    # Setting phase
    phase = 14
    print "Setting clock phase to %i" %phase
    set_roc_parameter(df_params, "Top", "all","Phase", phase)
    
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "ExtCtest", 0)
    set_roc_parameter(df_params, "Top", "all", "Sel_STROBE_fcmd", 1)
    set_roc_parameter(df_params, "Top", "all", "En_PhaseShift", 1)

    # Fast CMDs
    disable_fcmds()

    strobe = 0
    calib = strobe
    calib_stop = calib + 20
    l1a = calib + calib_offset + l1a_offset #-2 #+ 1

    if calib_stop >= l1a: calib_stop = l1a - 1

    set_fcmd_l1a(l1a, l1a+1)
    set_fcmd_calib(calib, calib_stop)

    # Data capture window
    c_start = l1a + capture_offset
    ev_size = 44
    c_stop = c_start + ev_size + 1
    set_capture_window(c_start, c_stop)

    ## Trigger capture window
    c_trig_start = calib + calib_offset + capture_offset - 5
    c_trig_stop = c_trig_start + 10
    for ififo in range(4):
        set_fifo_capture_window(ififo, c_trig_start, c_trig_stop)

    ## Data size
    data_size = ev_size * n_events
    if data_size > max_fifo_size:
        print("!!!! Warning: requested more data than fits into single fifo")
        n_events = max_fifo_size/ev_size
        print("Using maximum possible events in fifo: %i" %(n_events))

    calib_dacs = [-1] + range(0,2000,1)

    print("Disabling all channels")
    ## disable all channels:
    for chan in range(72):
        #disable_ch_inj(chan)
        set_roc_parameter(df_params, "ch", chan, "LowRange", 0)
        set_roc_parameter(df_params, "ch", chan, "HighRange", 0)

    set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)
    set_roc_parameter(df_params, "ReferenceVoltage", "all", "ExtCtest", 0)

    print("Starting scan")
    
    ## Select values for CTDC parameter of TDC
    dacs = range(0,32,4)
    ## Choose channels to enable
    chans = range(0,72,6)

    for dac in dacs:
	for ch in chans:
	    ## Low Range configuration
	    set_roc_parameter(df_params, "ch", ch, "LowRange", 1)

	    ## High Range configuration
	    #set_roc_parameter(df_params, "ch", ch, "HighRange", 1)

	    print("Setting dac %i for channel %i" %(dac,ch))
	    set_roc_parameter(df_params, "ch", ch, "DAC_CAL_CTDC_TOA", dac)
	   
	for calib_dac in calib_dacs:
	    print "Setting calib DAC to", calib_dac
	    #set_calib_dac(calib_dac)

	    if calib_dac >= 0:
		set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 1)	        
		set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", calib_dac)

	    else:
	        set_roc_parameter(df_params, "ReferenceVoltage", "all", "IntCtest", 0)
		set_roc_parameter(df_params, "ReferenceVoltage", "all", "Calib_dac", 0)

	    ## Name of directory 
	    outdir = odir + "/dac_%i/calibDAC_%i" %(dac, calib_dac)
	    outdir = os.path.abspath(outdir) + "/"

	    ## Create folder if it doesn't exist
	    if not os.path.exists(outdir):
	        os.makedirs(outdir)

	    flush_fifos()
	    bash_acq_loop(n_events, 0)
	    read_fifos([4,5],outdir + "/", data_size)

	print
	## end calib loop

	# disable channels
	for ch in chans:
	    set_roc_parameter(df_params, "ch", ch, "LowRange", 0)
	    set_roc_parameter(df_params, "ch", ch, "HighRange", 0)

    for chan in range(72):
        #disable_ch_inj(chan)
        set_roc_parameter(df_params, "ch", chan, "LowRange", 0)
        set_roc_parameter(df_params, "ch", chan, "HighRange", 0)

    ## end chan loop
    return

def main():
    n_events = 50
    suf = ""

    if len(sys.argv) > 1:
        print sys.argv
        n_events = int(sys.argv[1])
        print("## %i events requested" %n_events)
    if len(sys.argv) > 2:
        suf = "_" + str(sys.argv[2])
        print("## %s suffix requested" %suf)
    '''
    for ch in range(72):
        set_roc_parameter(df_params, "ch", ch,"DIS_TDC", 1)
        set_roc_parameter(df_params, "ch", ch,"Mask_adc", 1)
        set_roc_parameter(df_params, "ch", ch,"Mask_AlignBuffer", 1)

    for ch in [2,27,38,63]:
        set_roc_parameter(df_params, "ch", ch,"DIS_TDC", 0)
        set_roc_parameter(df_params, "ch", ch,"Mask_adc", 0)
        set_roc_parameter(df_params, "ch", ch,"Mask_AlignBuffer", 0)
    '''
    acq(n_events, suf)


if __name__ == "__main__":

    main()
