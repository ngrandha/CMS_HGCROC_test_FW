#!/usr/bin/env python2
import sys, os, pickle
import pandas as pd
import numpy as np

def get_R0(row):
    sub_addr = row.SubAddress
    reg = row.Register

    address = (sub_addr * 2**5) + reg
    val_R0 = address & 0xff

    return val_R0

def get_R1(row):
    sub_addr = row.SubAddress
    reg = row.Register

    address = (sub_addr * 2**5) + reg
    val_R1 = (address // 2**8) & 0xff

    return val_R1

# # mask functions
def get_param_mask(row):

    param_bit_positions = row.param_bit
    param_mask = np.sum(2**(param_bit_positions))

    return param_mask

def get_param_minbit(row):

    param_bit_positions = row.param_bit

    param_minbit = min(param_bit_positions)

    return param_minbit

def get_reg_mask(row):
    reg_bit_positions = row.reg_bit
    reg_mask = np.sum(2**(reg_bit_positions))

    return reg_mask

def get_def_mask(row):
    reg_bit_positions = row.reg_bit

    # register bits that are "on"
    reg_on_bits = row.Default

    reg_data = np.sum(reg_on_bits * (2**(reg_bit_positions)))

    return reg_data

# recurrent nested dict creation (split tuple in key)
def recur_dictify(d):
    if not isinstance(d, dict):
        return d

    key = next(iter(d))
    if isinstance(key, tuple):
        if len(key) < 2: return d
        d2 = {}
        for key,item in d.iteritems():
            key1 = key[:-1] if len(key) > 2 else key[0]
            key2 = key[-1]
            if key1 in d2:
                d2[key1][key2] = item
            else:
                d2[key1] = {key2: item}
        return recur_dictify(d2)
    return d

def convert_map(map_fname = "./silicon_version/HGCROCv2_I2C_params.csv",
                outfname = None):

    # read CSV map into dataframe
    print("# Reading in dataframe from CSV file: %s" %map_fname )
    df_map = pd.read_csv(map_fname)

    print("# Computing R0/R1 values")
    # compute R0/R1 register values from subAdd and register
    df_map["R0"] = df_map.apply(get_R0, axis = 1)
    df_map["R1"] = df_map.apply(get_R1, axis = 1)

    print("# Grouping by register")
    # make group by object to compute per-register masks
    gb_df = df_map.groupby(["SubBlock","BlockID","parameter","R0","R1"])

    print("# Computing register-level masks")
    param_mask = gb_df.apply(get_param_mask)
    reg_mask = gb_df.apply(get_reg_mask)
    def_mask = gb_df.apply(get_def_mask)
    param_minbit = gb_df.apply(get_param_minbit)

    print("# Making mask dataframe")
    # create new datframe with register masks
    df_mask = pd.DataFrame({"param_mask":param_mask,"reg_mask":reg_mask,
                            "defval_mask":def_mask, "param_minbit":param_minbit})

    # assign the register ID (order) within parameter
    df_mask["reg_id"] = df_mask.groupby(["SubBlock","BlockID","parameter"]).cumcount()

    df_mask.reset_index(inplace = True)

    print("# Converting dataframe to nested dict")
    # convert df to dict
    d_mask = df_mask.set_index(["SubBlock","BlockID","parameter","reg_id"]).T.to_dict()
    # make nested dict
    d_mask_nested = recur_dictify(d_mask)

    ## Save files
    if outfname is None:
        outfname = map_fname.replace(".csv","_regmap.csv")

    # save dataframe as csv
    print("# Saving mask dataframe to CSV %s"  %outfname)
    df_mask.to_csv(outfname, index = False)

    # save dict as pickle
    outfname = outfname.replace(".csv","_dict.pickle")
    print("# Saving dict to pickle %s"  %outfname)

    with open(outfname, 'wb') as handle:
        pickle.dump(d_mask_nested, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return 1

def main():

    if len(sys.argv) > 1:
        infname = sys.argv[1]
    else:
        infname = "./silicon_version/HGCROCv2_I2C_params.csv"

    if len(sys.argv) > 2:
        outfname = sys.argv[2]
    else:
        outfname = None

    convert_map(infname, outfname)

if __name__ == "__main__":

    main()
