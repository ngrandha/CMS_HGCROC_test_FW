#!/usr/bin/env python2
import sys, os
import pandas as pd
import numpy as np
from glob import glob

def split_param_name(name):

    hasdigit = all([c.isdigit() for c in name.split("_")[-1]])

    if not hasdigit:
        return name
    else:
        parts = name.split("_")
        if len(parts) > 1:
            pref = "_".join(parts[:-1])
        else:
            pref = parts[0]

    return pref

def split_param_bit(name):
    hasdigit = all([c.isdigit() for c in name.split("_")[-1]])

    if not hasdigit:
        return 0 #-1
    else:
        parts = name.split("_")
        if len(parts) > 1:
            suf = int(parts[-1])
        else:
            suf = 0 #-1

    return suf

def get_subblock_maps(fnames):

    d_params = {}

    for fname in fnames:
        df = pd.read_csv(fname, sep = ";")

        name = os.path.basename(fname)
        name = name.replace("_param.csv","")

        df["parameter"] = df["function"].apply(split_param_name)
        df["param_bit"] = df["function"].apply(split_param_bit)

        d_params[name] = df

    return d_params

def make_map(indir, outfname = "test.csv"):

    subblock_fnames = glob(indir + "/*param.csv")
    d_params = get_subblock_maps(subblock_fnames)

    #print d_params.keys()

    df_subAdr = pd.read_csv(indir + "/SubBlock_address.csv", sep = ";")

    df_subAdr["Block"] = df_subAdr["function"].apply(lambda x: x[:x.rfind("_")] if "_" in x else x)
    df_subAdr["BlockID"] = df_subAdr["function"].apply(lambda x: x[x.rfind("_")+1:] if "_" in x else 0)

    # dict for subblock names
    map_subblocks = {'cm':"channel-wise", 'ch':"channel-wise", 'calib':"channel-wise",
                     'ReferenceVoltage':'ReferenceVoltage', 'GlobalAnalog':"GlobalAnalog",
                     'MasterTdc':"MasterTdc", 'DigitalHalf':"DigitalHalf", 'Top':"Top"}

    # map corresponding type
    df_subAdr["Type"] = df_subAdr["Block"].map(map_subblocks)

    # set column names
    df_subAdr.columns = ['Name', 'SubAddress', 'SubBlock', 'BlockID', 'Type']

    ## Join sub-block and parameters
    dfs = []

    for i in range(len(df_subAdr)):
        df = d_params[df_subAdr.iloc[i].Type]
        df["index"] = i

        df = df.reset_index().set_index("index")
        dfs.append(df)

    # concat all subblock dfs
    df_all = pd.concat(dfs)

    # join subblock dfs by index
    df_join = df_subAdr.join(df_all)

    df_join.reset_index(inplace = True)
    df_join.sort_values(["SubBlock","BlockID","parameter"], inplace = True)
    df_join.set_index(["SubBlock","BlockID","parameter"], inplace = True)

    columns = ["param_bit",u'SubAddress', u'address', u'location',  u'Default']
    df_out = df_join[columns]

    df_out.columns = ["param_bit",u'SubAddress', u'Register', u'reg_bit',  u'Default']

    df_out.to_csv(outfname)
    return

def main():
    print sys.argv

    if len(sys.argv) > 1:
        indir = sys.argv[1]
    else:
        indir = "./"

    if len(sys.argv) > 2:
        outfname = sys.argv[2]
    else:
        outfname = "test.csv"

    make_map(indir, outfname)

if __name__ == "__main__":

    main()
